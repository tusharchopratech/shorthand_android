package com.shorthand.edata.pojoeventbus;

import android.os.Bundle;

/**
 * Created by tushar on 29/07/17.
 */

public class MessageEventData {
    String eventName;
    Bundle bundleData;

    public MessageEventData(String eventName, Bundle bundleData) {
        this.eventName = eventName;
        this.bundleData = bundleData;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Bundle getBundleData() {
        return bundleData;
    }

    public void setBundleData(Bundle bundleData) {
        this.bundleData = bundleData;
    }
}
