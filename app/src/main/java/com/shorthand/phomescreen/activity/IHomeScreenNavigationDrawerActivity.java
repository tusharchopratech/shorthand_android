package com.shorthand.phomescreen.activity;

/**
 * Created by tushar on 25/07/17.
 */

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

public class IHomeScreenNavigationDrawerActivity {

    CacheBoundary cacheBoundary;
    Context context;

    public IHomeScreenNavigationDrawerActivity(Context context) {
        cacheBoundary = new CacheBoundary();
        this.context = context;
    }

    public void shareApp(Context context) {
        General.shareApp(context);
    }

    public void rateApp(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public void setPaidUserData(String jsonResponseString) {
        if (jsonResponseString != null) {
            cacheBoundary.setUserPaymentStatus(context, Constants.USER_PAYMENT_STATUS.PAID_BUT_DATA_NOT_SENT);
            cacheBoundary.setPaymentJsonResponseString(context, jsonResponseString);
            new BackgroundWork(jsonResponseString).execute();
        }
    }

    class BackgroundWork extends AsyncTask<Void, Void, Void> {

        String status;
        String jsonResponseString;

        public BackgroundWork(String jsonResponseString) {
            status = null;
            this.jsonResponseString = jsonResponseString;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            status = new RestBoundary().setUserPaymentInfo(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                    General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)), jsonResponseString);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (status != null && status.equals("SUCCESS")) {
                cacheBoundary.setUserPaymentStatus(context, Constants.USER_PAYMENT_STATUS.PAID);
                if (!((AppCompatActivity) context).isFinishing()) {
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Thanks for purchasing Shorthand App. Please restart app and select offline Mode.");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ((AppCompatActivity) context).finish();
                        }
                    });
                    builder1.create().show();
                }
            } else {
                if (!((AppCompatActivity) context).isFinishing()) {
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("Oops !!!\nSomething went wrong with network.");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            new BackgroundWork(jsonResponseString).execute();
                        }
                    });
                    builder1.create().show();
                }
            }
        }
    }
}

