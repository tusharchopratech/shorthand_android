package com.shorthand.plogin;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.jaredrummler.android.device.DeviceName;
import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.HomeScreenNavigationDrawerActivity;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    final String TAG = "#" + getClass().getSimpleName();
    final int RC_SIGN_IN = 007;
    LinearLayout linearLayoutLoginPanel, linearLayoutNetworkIssue, linearLayoutPaidUserPanel;
    ImageView imageViewGPlus, imageViewFacebook;
    Button buttonRetry, buttonGoOnline, buttonGoOffline;

    GoogleApiClient mGoogleApiClient;

    CallbackManager callbackManager;

    ILoginActivity iLoginActivity;


    private void initializeVariables() {
        iLoginActivity = new ILoginActivity();
        linearLayoutLoginPanel = (LinearLayout) findViewById(R.id.linearLayoutLoginPanelId);
        linearLayoutNetworkIssue = (LinearLayout) findViewById(R.id.linearLayoutNetoworkIssueLoginActivityId);
        linearLayoutPaidUserPanel = (LinearLayout) findViewById(R.id.linearLayoutPaidUserPanelId);
        buttonRetry = (Button) findViewById(R.id.buttonNetoworkIssueLoginActivityId);
        buttonGoOnline = (Button) findViewById(R.id.buttonOnlinePaidUserPanelId);
        buttonGoOffline = (Button) findViewById(R.id.buttonOfflinePaidUserPanelId);
        imageViewFacebook = (ImageView) findViewById(R.id.imageViewFacebookId);
        imageViewGPlus = (ImageView) findViewById(R.id.imageViewGPlusId);

        if (Constants.IS_IN_PRODUCTION) {
            Fabric.with(this, new Crashlytics());
            Fabric.with(this, new Answers());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        initializeVariables();

        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (General.isNetworkAvailable(LoginActivity.this)) {
                    String uuid = new CacheBoundary().getMyUuid(LoginActivity.this);
                    String token = new CacheBoundary().getMyToken(LoginActivity.this);
                    String refreshToken = new CacheBoundary().getMyRefreshToken(LoginActivity.this);

                    if (uuid != null && uuid != "" && token != null && token != "" && refreshToken != null && refreshToken != "") {
                        new BackgroundGetInitialInfo().execute();
                    } else {
                        linearLayoutLoginPanel.setVisibility(View.VISIBLE);
                        linearLayoutNetworkIssue.setVisibility(View.INVISIBLE);
                        findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        imageViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (General.isNetworkAvailable(LoginActivity.this)) {
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email"));
                } else {
                    linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
                    linearLayoutNetworkIssue.setVisibility(View.VISIBLE);
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                }
            }
        });

        imageViewGPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (General.isNetworkAvailable(LoginActivity.this)) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
                    linearLayoutNetworkIssue.setVisibility(View.VISIBLE);
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                }
            }
        });

        buttonGoOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (General.isThisServiceRunning(LoginActivity.this, MusicPlayerService.class)) {
                    if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.ONLINE)) {
                        stopService(new Intent(LoginActivity.this, MusicPlayerService.class));
                    }
                }
                startActivity(new Intent(LoginActivity.this, OfflineDictationsActivity.class));
                finish();
            }
        });

        buttonGoOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BackgroundGetInitialInfo().execute();
            }
        });

        if (getIntent().getAction() != null && getIntent().getAction().equals(Constants.MUSIC_PLAYER_NOTIFICATION_ACTION.OPEN_OFFLINE_DICTATION)) {
            startActivity(new Intent(LoginActivity.this, OfflineDictationsActivity.class));
            finish();
        } else if (getIntent().getAction() != null && getIntent().getAction().equals(Constants.MUSIC_PLAYER_NOTIFICATION_ACTION.OPEN_ONLINE_DICTATION)) {
            new BackgroundGetInitialInfo().execute();
        } else {
            checkLoginStatusAndProcess();
        }
    }

    private void checkLoginStatusAndProcess() {

        String uuid = new CacheBoundary().getMyUuid(LoginActivity.this);
        String token = new CacheBoundary().getMyToken(LoginActivity.this);
        String refreshToken = new CacheBoundary().getMyRefreshToken(LoginActivity.this);
        String userPaymentStatus = new CacheBoundary().getUserPaymentStatus(LoginActivity.this);

        if (uuid != null && uuid != "" && token != null && token != "" && refreshToken != null && refreshToken != "") {
            if (userPaymentStatus.equals(Constants.USER_PAYMENT_STATUS.PAID)) {
                if (!General.isNetworkAvailable(LoginActivity.this)) {
                    Intent intent = new Intent(LoginActivity.this, OfflineDictationsActivity.class);
                    intent.putExtra("showNoInternetDialog", true);
                    startActivity(intent);
                    finish();
                } else {
                    linearLayoutPaidUserPanel.setVisibility(View.VISIBLE);
                    linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
                    linearLayoutNetworkIssue.setVisibility(View.INVISIBLE);
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.GONE);
                }
            } else {
                new BackgroundGetInitialInfo().execute();
            }
        } else {

            DeviceName.with(LoginActivity.this).request(new DeviceName.Callback() {

                @Override
                public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                    try {
                        String manufacturer = info.manufacturer;  // "Samsung"
                        String name = info.marketName;            // "Galaxy S7 Edge"
                        String model = info.model;                // "SAMSUNG-SM-G935A"
                        //String codename = info.codename;          // "hero2lte"
                        //String deviceName = info.getName();       // "Galaxy S7 Edge"
                        new CacheBoundary().setMyDeviceName(LoginActivity.this, manufacturer + ", " + name + ", " + model);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            linearLayoutLoginPanel.setVisibility(View.VISIBLE);
            linearLayoutNetworkIssue.setVisibility(View.INVISIBLE);
            findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            AppEventsLogger.activateApp(this);
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    String accessToken = loginResult.getAccessToken().getToken();
                    General.showLog("tag", accessToken);

                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Get facebook data from login
                            Bundle bFacebookData = getFacebookData(object);
                            String personName = bFacebookData.getString("first_name") + " " + bFacebookData.getString("last_name");
                            String email = bFacebookData.getString("email");
                            String id = bFacebookData.getString("idFacebook");
                            String gender = bFacebookData.getString("gender");
                            String dob = bFacebookData.getString("birthday");
                            String picUrl = bFacebookData.getString("profile_pic");
                            new BackgroundSendDataAndStore(personName, dob, gender, email, id, "facebook", picUrl).execute();
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {
                }
            });


            if (isGooglePlayServicesAvailable()) {
                General.showLog(TAG, "Google Play Services Found");
            } else {
                General.showLog(TAG, "Google Play Services Not Found");
            }
        }

    }


    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(LoginActivity.this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(LoginActivity.this, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN && resultCode == Activity.RESULT_OK) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result != null) {
            General.showLog(TAG, "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();

                if (acct != null) {
                    String personName = acct.getDisplayName();
                    String email = acct.getEmail();
                    String id = acct.getId();
                    String picUrl = "";
                    if (acct.getPhotoUrl() != null) {
                        picUrl = acct.getPhotoUrl().toString();
                    }
                    //new BackgroundGoogleSignInGetDobAndGender(personName, email, id, picUrl).execute();
                    new BackgroundSendDataAndStore(personName, null, null, email, id, "google", picUrl).execute();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(LoginActivity.this, "Connection failed ...", Toast.LENGTH_LONG).show();
        General.showLog(TAG, "onConnectionFailed:" + connectionResult);
    }


    private Bundle getFacebookData(JSONObject object) {

        Bundle bundle = new Bundle();
        try {
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                General.showLog("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bundle;
    }

    class BackgroundSendDataAndStore extends AsyncTask<Void, Void, Void> {

        String name, dob, gender, email, id, accountType, picUrl;
        String status;

        public BackgroundSendDataAndStore(String name, String dob, String gender, String email, String id, String accountType, String picUrl) {
            if (dob != null && dob.length() > 0) {
                this.dob = dob;
            } else {
                this.dob = "";
            }

            if (gender != null && gender.length() > 0) {
                this.gender = gender.toLowerCase();
            } else {
                this.gender = "";
            }

            if (name != null && name.length() > 0) {
                this.name = name.toLowerCase();
            } else {
                this.name = "";
            }

            if (picUrl != null && picUrl.length() > 0) {
                this.picUrl = picUrl;
            } else {
                this.picUrl = "";
            }

            this.email = email;
            this.id = id;
            this.accountType = accountType;

        }

        @Override
        protected void onPreExecute() {
            linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
            linearLayoutNetworkIssue.setVisibility(View.INVISIBLE);
            findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            status = iLoginActivity.sendDataAndStore(name, gender, dob, email, id, accountType.toLowerCase(), picUrl, LoginActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (status != null && status.equals("SUCCESS")) {
                new BackgroundGetInitialInfo().execute();
            } else {
                linearLayoutNetworkIssue.setVisibility(View.VISIBLE);
                linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
                findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
            }
        }
    }


    class BackgroundGetInitialInfo extends AsyncTask<Void, Void, Void> {
        InitialAppInfo initialAppInfo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
            linearLayoutNetworkIssue.setVisibility(View.INVISIBLE);
            findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            initialAppInfo = iLoginActivity.getInitialAppInfo(LoginActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (initialAppInfo != null) {

                Variables.ADS_PERCENTAGE_BANNER_HOMESCREEN_ACTIVITY = initialAppInfo.getAds_banner_percent_homescreen_activity();
                Variables.ADS_PERCENTAGE_BANNER_SLIDING_UP_PANEL = initialAppInfo.getAds_banner_percent_sliding_up_panel();
                Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_LIST = initialAppInfo.getAds_interstitial_dictation_list_percent();
                Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_TYPE = initialAppInfo.getAds_interstitial_dictation_type_percent();
                Variables.ADS_PERCENTAGE_REWARDED_VIDEO = initialAppInfo.getAds_rewarded_video_percent();
                Variables.SEND_ANALYTICS = initialAppInfo.getReceive_analytics();

                if (!new CacheBoundary().getUserPaymentStatus(LoginActivity.this).equals(Constants.USER_PAYMENT_STATUS.PAID_BUT_DATA_NOT_SENT)) {
                    new CacheBoundary().setUserPaymentStatus(LoginActivity.this, initialAppInfo.getUser_type());
                }

                if (initialAppInfo.getUpdate_app().toLowerCase().equals("yes")) {
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    builder1.setMessage("This version of your application not supported now. Please update your app.");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            General.goToPlayStore(LoginActivity.this);
                            finish();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (initialAppInfo.getUpdate_app().toLowerCase().equals("optional")) {
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    builder1.setMessage("The new version of this application is available, click Ok to upgrade now?");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            General.goToPlayStore(LoginActivity.this);
                            finish();
                        }
                    });
                    builder1.setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (General.isThisServiceRunning(LoginActivity.this, MusicPlayerService.class)) {
                                if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.OFFLINE)) {
                                    stopService(new Intent(LoginActivity.this, MusicPlayerService.class));
                                }
                            }
                            Intent intent = new Intent(LoginActivity.this, HomeScreenNavigationDrawerActivity.class);
                            intent.putExtra("initialAppInfo", initialAppInfo);
                            startActivity(intent);
                            finish();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (initialAppInfo.getUpdate_app().toLowerCase().equals("no")) {
                    if (General.isThisServiceRunning(LoginActivity.this, MusicPlayerService.class)) {
                        if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.OFFLINE)) {
                            stopService(new Intent(LoginActivity.this, MusicPlayerService.class));
                        }
                    }
                    Variables.INITIAL_APP_INFO = initialAppInfo;
                    startActivity(new Intent(LoginActivity.this, HomeScreenNavigationDrawerActivity.class));
                    findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
                    finish();
                }
            } else {
                linearLayoutNetworkIssue.setVisibility(View.VISIBLE);
                linearLayoutLoginPanel.setVisibility(View.INVISIBLE);
                findViewById(R.id.aVLoadingIndicatorViewLoginActivityId).setVisibility(View.INVISIBLE);
            }
        }
    }


}
