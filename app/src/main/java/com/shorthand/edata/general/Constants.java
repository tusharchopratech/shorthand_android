package com.shorthand.edata.general;

import android.os.Environment;

/**
 * Created by tushar on 26/03/17.
 */

public class Constants {

    public static final boolean IS_IN_PRODUCTION = true;
    public static final String BASE_URL = "https://www.shorthandpractice.com/api/";
    //public static final String BASE_URL = "http://3d14b0d8.ngrok.io/api/";
    public static final String AUDIO_URL = BASE_URL + "v1/dictation/v1/get_audio";
    public static final String IMAGE_URL = BASE_URL + "v1/dictation/v1/get_image";
    public static final String SHA_KEY_1 = "volsky";
    public static final String SHA_KEY_2 = "robots";
    public static final int SHORTHAND_ANALYTICS_RECEIVE_SIZE = 10;

    public static final String IN_APP_PURCHASE_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlGePQ+A894YuwOQHO8vzCmrlMkS6gn" +
            "1Y2/r2Bv726HM0qpNJ7X7fcwh3nOHe7+MIXqo+LgU89OM6iZNmHgZ9XWqxt5cp2srvMUsrajifSSCmgJdndPg4wy0WBlBzesCelgW9ltyJvG" +
            "0F298IZk8+0l0STp2z80V5pZXowLDco8TEm53VJdLdtmY+4QWMg8oXJmCY3lI/yF/I/oWRFpNeKw8LcbeHmtyNumLJy6KXXyOzmUm+Ek12Or4m" +
            "77vkVr/ygcrfXVwI7OvT0zaKsu9a91RvQxy+kaSrnOlPwDZfA2TlTyCbvFoCm9IlstpL6F2/eSAH/m5fijbgibGKSeOF5wIDAQAB";
    public static final String MERCHANT_ID = "01639774578933956027";


    public static final String INTERNAL_STORAGE_PATH = Environment.getExternalStorageDirectory() + "/Android/data/com.shorthand";
    public static final String OFFLINE_FILE_PATH = INTERNAL_STORAGE_PATH + "/data.data";

    public interface USER_PAYMENT_STATUS {
        String FREE = "free";
        String PAID = "paid";
        String PAID_BUT_DATA_NOT_SENT = "paid_but_data_not_sent";
    }

    public interface MUSIC_PLAYER_NOTIFICATION_ACTION{
        String OPEN_OFFLINE_DICTATION="music_player_notification_action_open_offline_dictation";
        String OPEN_ONLINE_DICTATION="music_player_notification_action_open_online_dictation";
    }

    public interface AUDIO_STATUS {
        String PLAYING = "audio_status_playing";
        String STOPPED = "audio_status_stopped";
        String PAUSED = "audio_status_paused";
        String COMPLETED = "audio_status_completed";
        String ERROR = "audio_status_error";
        String LOADING = "audio_status_loading";
    }

    public interface AUDIO_SOURCE {
        String ONLINE = "audio_source_online";
        String OFFLINE = "audio_source_offline";
    }

    public interface EVENT_BUS_EVENT {

        String OPEN_IN_APP_PURCHASE_DIALOG = "ebe_open_in_app_purchase_dialog";

        interface MUSIC_PLAYER_SERVICE_PERFORM {
            String PLAY = "ebe_media_player_service_method_play";
            String PAUSE = "ebe_media_player_service_method_pause";
            String SEEK_TO = "ebe_media_player_service_method_seek_to";
            String START_AUDIO = "ebe_media_player_service_method_start_audio";
        }

        interface AUDIO_PLAYER_GET_EVENTS {
            String MUSIC_PLAYER_SERVICE_STARTED = "event_bus_event_music_player_service_started";
            String PLAYER_PLAYED = "event_bus_event_media_player_play";
            String PLAYER_PAUSED = "event_bus_event_media_player_paused";
            String PLAYER_CLOSED = "event_bus_event_media_player_close";
            String PLAYER_COMPLETED = "event_bus_event_media_player_complete";
            String PLAYER_PROGRESS = "event_bus_event_media_player_progress";
            String PLAYER_ERROR = "event_bus_event_media_player_error";
            String PLAYER_SECONDARY_PROGRESS = "event_bus_event_media_player_secondary_progress";
            String PLAYER_PLAYED_AFTER_PAUSED = "event_bus_event_media_player_played_after_paused";
        }

        interface DOWNLOAD_OFFLINE_FILE {
            String DOWNLOAD_START = "download_start";
            String DOWNLOAD_RETRY = "download_retry";
            String DOWNLOAD_PROGRESS = "download_progress";
            String DOWNLOAD_SUCCESS = "download_success";
            String DOWNLOAD_FAILED = "download_failed";
            String GET_DOWNLOAD_FILE_DATA_FAILED = "get_download_file_data_failed";
        }

    }

    public interface FRAGMENT_ANIMATION {
        String SLIDE_LEFT = "slide_left";
        String SLIDE_RIGHT = "slide_right";
        String NO_SLIDE = "no_slide";
    }

    public interface ANALYTICS {

        interface SCREEN_TYPE {
            String SCREEN_ACTIVITY = "activity";
            String SCREEN_FRAGMENT = "fragment";
        }

        interface EVENT_TYPE {
            String BANNER_AD_CLICK = "ad_click_banner";
            String INTERSTITIAL_AD_CLICK = "ad_click_interstitial";
            String REWARDED_VIDEO_AD_CLICK = "ad_click_rewarded_video";
            String NAVIGATION_DRAWER_ITEM_CLICK = "navigation_drawer_item_click";
            String HARDWARE_BACK_PRESSED = "hardware_back_pressed";
            String TOP_LEFT_ICON_BACK_PRESSED = "top_left_icon_back_pressed";
            String SLIDING_UP_PANEL_VIEW_CLICK = "sliding_up_panel_view_click";
            String DICTATIONS_TYPE_CLICKED = "dictations_type_clicked";
            String DICTATIONS_LIST_CLICKED = "dictations_list_clicked";
            String FEEDBACK_BUTTON_SEND_CLICKED = "feedback_button_send_clicked";
            String FEEDBACK_BUTTON_GO_BACK_CLICKED = "feedback_button_go_back_clicked";
            String SETTINGS_RESET_PROGRESS_LAYOUT_CLICKED = "settings_reset_proress_layout_clicked";
            String SETTINGS_RESET_PROGRESS_DIALOG_YES_CLICKED = "settings_reset_progress_dialog_yes_clicked";
            String SETTINGS_RESET_PROGRESS_DIALOG_NO_CLICKED = "settings_reset_progress_dialog_no_clicked";
            String USER_PROFILE_BUTTON_GO_BACK_CLICKED = "user_profile_button_go_back_clicked";
            String USER_PROFILE_BUTTON_SAVE_CLICKED = "user_profile_button_save_clicked";
            String IN_APP_PURCHASE_CUSTOM_DIALOG_GET_FULL_VERSION_CLICKED = "in_app_purchase_custom_dialog_get_full_version_clicked";
            String IN_APP_PURCHASE_PAYMENT_SUCCESS = "in_app_purchase_payment_success";
            String IN_APP_PURCHASE_PAYMENT_ERROR = "in_app_purchase_payment_error";
            String NAVIGATION_DRAWER_ICON_CLICK = "navigation_drawer_icon_click";
            String PAYTM_IN_APP_PURCHASE_LINK_CLICK = "paytm_in_app_purchase_link_click";
        }
    }


}
