package com.shorthand.edata.storage.sharedpreference;

import android.content.Context;

/**
 * This class is boundary for Shared Prefrence.
 * Direct function passing is done.
 */

public class CacheBoundary {

    public CacheBoundary() {

    }

    public void setMyName(Context context, String name) {
        new SharedPreference(context).setMyName(name);
    }

    public String getMyName(Context context) {
        return new SharedPreference(context).getMyName();
    }

    public void setMyEmail(Context context, String email) {
        new SharedPreference(context).setMyEmail(email);
    }

    public String getMyEmail(Context context) {
        return new SharedPreference(context).getMyEmail();
    }

    public void setMyGender(Context context, String gender) {
        new SharedPreference(context).setMyGender(gender);
    }

    public String getMyGender(Context context) {
        return new SharedPreference(context).getMyGender();
    }

    public void setMyDob(Context context, String dob) {
        new SharedPreference(context).setMyDob(dob);
    }

    public String getMyDob(Context context) {
        return new SharedPreference(context).getMyDob();
    }

    public void setMyPhone(Context context, String phone) {
        new SharedPreference(context).setMyPhone(phone);
    }

    public String getMyPhone(Context context) {
        return new SharedPreference(context).getMyPhone();
    }

    public String getMyReferrer(Context context) {
        return new SharedPreference(context).getMyReferrer();
    }

    public void setMyReferrer(Context context, String myReferrer) {
        new SharedPreference(context).setMyReferrer(myReferrer);
    }

    public void setMyUuid(Context context, String uuid) {
        new SharedPreference(context).setMyUuid(uuid);
    }

    public String getMyUuid(Context context) {
        return new SharedPreference(context).getMyUuid();
    }

    public void setMyToken(Context context, String myToken) {
        new SharedPreference(context).setMyToken(myToken);
    }

    public String getMyToken(Context context) {
        return new SharedPreference(context).getMyToken();
    }

    public void setMyRefreshToken(Context context, String refreshToken) {
        new SharedPreference(context).setMyRefreshToken(refreshToken);
    }

    public String getMyRefreshToken(Context context) {
        return new SharedPreference(context).getMyRefreshToken();
    }

    public void setMyPicUrl(Context context, String picUrl) {
        new SharedPreference(context).setMyPicUrl(picUrl);
    }

    public String getMyPicUrl(Context context) {
        return new SharedPreference(context).getMyPicUrl();
    }

    public void setIsTokenExpired(Context context, boolean isTokenExpired) {
        new SharedPreference(context).setIsTokenExpired(isTokenExpired);
    }

    public boolean getIsTokenExpired(Context context) {
        return new SharedPreference(context).getIsTokenExpired();
    }

    public String getOfflinePracticedDictations(Context context) {
        return new SharedPreference(context).getOfflinePracticedDictations();
    }

    public void setOfflinePracticedDictations(Context context, String offlinePracticedDictations) {
        new SharedPreference(context).setOfflinePracticedDictations(offlinePracticedDictations);
    }

    public String getOfflineViewedDictations(Context context) {
        return new SharedPreference(context).getOfflineViewedDictations();
    }

    public void setOfflineViewedDictations(Context context, String offlineViewedDictations) {
        new SharedPreference(context).setOfflineViewedDictations(offlineViewedDictations);
    }

    public String getOfflineFileVersion(Context context) {
        return new SharedPreference(context).getOfflineFileVersion();
    }

    public void setOfflineFileVersion(Context context, String offlineFileVersion) {
        new SharedPreference(context).setOfflineFileVersion(offlineFileVersion);
    }

    public String getOfflineFilePassword(Context context) {
        return new SharedPreference(context).getOfflineFilePassword();
    }

    public void setOfflineFilePassword(Context context, String offlineFilePassword) {
        new SharedPreference(context).setOfflineFilePassword(offlineFilePassword);
    }

    public String getOfflineFileIv(Context context) {
        return new SharedPreference(context).getOfflineFileIv();
    }

    public void setOfflineFileIv(Context context, String offlineFileIv) {
        new SharedPreference(context).setOfflineFileIv(offlineFileIv);
    }

    public String getUserPaymentStatus(Context context) {
        return new SharedPreference(context).getUserPaymentStatus();
    }

    public void setUserPaymentStatus(Context context, String status) {
        new SharedPreference(context).setUserPaymentStatus(status);
    }

    public boolean getShowTapTargetViewDictationType(Context context) {
        return new SharedPreference(context).getShowTapTargetViewDictationType();
    }

    public void setShowTapTargetViewDictationType(Context context, boolean showTapTargetViewDictationType) {
        new SharedPreference(context).setShowTapTargetViewDictationType(showTapTargetViewDictationType);
    }

    public boolean getShowTapTargetViewDictationList(Context context) {
        return new SharedPreference(context).getShowTapTargetViewDictationList();
    }

    public void setShowTapTargetViewDictationList(Context context, boolean showTapTargetViewDictationList) {
        new SharedPreference(context).setShowTapTargetViewDictationList(showTapTargetViewDictationList);
    }

    public String getPaymentJsonResponseString(Context context) {
        return new SharedPreference(context).getPaymentJsonResponseString();
    }

    public void setPaymentJsonResponseString(Context context, String paymentJsonResponseString) {
        new SharedPreference(context).setPaymentJsonResponseString(paymentJsonResponseString);
    }

    public String getMyDeviceName(Context context) {
        return new SharedPreference(context).getMyDeviceName();
    }

    public void setMyDeviceName(Context context, String myDeviceName) {
        new SharedPreference(context).setMyDeviceName(myDeviceName);
    }

    public String getMyCountryName(Context context) {
       return new SharedPreference(context).getMyCountryName();
    }

    public void setMyCountryName(Context context,String myCountryName) {
        new SharedPreference(context).setMyCountryName(myCountryName);
    }

}
