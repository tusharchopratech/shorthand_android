package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.pojo.UserDetails;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by tushar on 10/07/17.
 */

class RetrofitGetUserDetails {
    public UserDetails getUserDetails(Context context, String baseUrl, String token, String key) {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<UserDetails> c = api.sendData(token, key);
        try {
            Response<UserDetails> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return resp.body();
            }else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    interface ApiInterface {
        @POST("v1/get_user_details")
        Call<UserDetails> sendData(@Header("authorization") String authorization, @Header("key") String key);
    }
}
