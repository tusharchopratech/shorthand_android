package com.shorthand.edata.general;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.popenlink.OpenLinkActivity;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Locale;


public class General {

    public static void goToPlayStore(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static String getMd5(String email, String userId) {
        String s = email + "betabet" + "snipclip" + userId;
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getShaKeyForRefreshToken(String uuid, String email) {
        return getSha(uuid + Constants.SHA_KEY_1 + email + Constants.SHA_KEY_2);
    }

    public static String getShaKey(String uuid, String email) {
        return getSha(uuid + Constants.SHA_KEY_1 + email);
    }

    public static String getSha(String text) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(text.getBytes("UTF-8"));
            byte[] hash = crypt.digest();

            Formatter formatter = new Formatter();
            for (byte b : hash) {
                formatter.format("%02x", b);
            }
            sha1 = formatter.toString();
            formatter.close();


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    public static String getArrayAsCommaSeparatedString(ArrayList<String> array) {

        StringBuffer ret = new StringBuffer("");
        for (int i = 0; array != null && i < array.size(); i++) {
            ret.append(array.get(i));
            if (i < array.size() - 1) {
                ret.append(',');
            }
        }
        return ret.toString();
    }

    public static ArrayList<String> getCommaSeparatedStringAsArray(String string) {
        return new ArrayList<String>(Arrays.asList(string.split(",")));
    }


    public static int getScreenWidth(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }

    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void shareApp(Context context) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Shorthand App");
            String sAux = context.getResources().getString(R.string.share_app);
            sAux = sAux + ": https://play.google.com/store/apps/details?id=com.shorthand&referrer=utm_source%3Dandroid_app" +
                    "%26utm_medium%3Dshare_app_navi_drawer%26utm_content%3D" + new CacheBoundary().getMyUuid(context);
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            context.startActivity(Intent.createChooser(i, context.getString(R.string.share_app_view_title)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getNumberString(int number) {
        return NumberFormat.getNumberInstance(Locale.US).format(number);
    }

    public static String getNumberStringWithSuffix(int count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f %c", count / Math.pow(1000, exp), "KMGTPE".charAt(exp - 1));
    }


    public static String capsFirst(String str) {
        try {
            if (str == null || str.trim().length() == 0) {
                return "";
            }
            String[] words = str.split(" ");
            StringBuilder ret = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                ret.append(Character.toUpperCase(words[i].charAt(0)));
                ret.append(words[i].substring(1));
                if (i < words.length - 1) {
                    ret.append(' ');
                }
            }
            return ret.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String getAudioUrl(String token, String key, String uuid) {
        return Constants.AUDIO_URL + "?token=" + token + "&key=" + key + "&dictation_uuid=" + uuid;
    }

    public static String getImageUrl(String token, String key, String uuid, int imageNumber) {
        return Constants.IMAGE_URL + "?token=" + token + "&key=" + key + "&dictation_uuid=" + uuid + "&image_number="
                + String.valueOf(imageNumber);
    }

    public static boolean isThisServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void showToast(Context context, String message) {
        if (!Constants.IS_IN_PRODUCTION) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static void showLog(String tag, String message) {
        if (!Constants.IS_IN_PRODUCTION) {
            Log.v(tag, message);
        }
    }

    public static void changeFragment(FragmentManager fragmentManager, Fragment fragment, int containerId, String fragmentAnimation) {
        try {
            if (fragmentAnimation.equals(Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT)) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(containerId, fragment)
                        .disallowAddToBackStack()
                        .commit();
            } else if (fragmentAnimation.equals(Constants.FRAGMENT_ANIMATION.SLIDE_LEFT)) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(containerId, fragment)
                        .disallowAddToBackStack()
                        .commit();
            } else {
                fragmentManager.beginTransaction()
                        .replace(containerId, fragment)
                        .disallowAddToBackStack()
                        .commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void removeAllFragment(FragmentManager fragmentManager, int containerId) {
        fragmentManager
                .beginTransaction()
                .remove(fragmentManager.findFragmentById(containerId))
                .commit();
    }

    public static int getAvailbleStorage() {
        if (Build.VERSION.SDK_INT >= 18) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
            long megAvailable = bytesAvailable / (1024 * 1024);
            return (int) megAvailable;
        } else {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
            long megAvailable = bytesAvailable / (1024 * 1024);
            return (int) megAvailable;
        }
    }

    public static void openPaymentDialog(final Context context) {

        ShorthandAnalytics.trackEvent(context, "download_dictation_icon", "clicked");
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.inflate_alert_dialog_for_offline_app);

        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");

        TextView textViewOfflineTitle = (TextView) dialog.findViewById(R.id.textViewOfflineDialogId);
        TextView textViewOpenPaytmLink = (TextView) dialog.findViewById(R.id.textViewOfflineDialogOpenLinkId);
        textViewOfflineTitle.setTypeface(typeFace);
        textViewOfflineTitle.setTextSize(40);
        if (new CacheBoundary().getMyCountryName(context).toLowerCase().contains("india")) {
            textViewOpenPaytmLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OpenLinkActivity.class);
                    intent.putExtra("url", "https://blog.paytm.com/now-recharge-your-google-play-account-with-paytm-1a927b44476a");
                    context.startActivity(intent);
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.PAYTM_IN_APP_PURCHASE_LINK_CLICK, "");

                }
            });
        }else{
            textViewOpenPaytmLink.setVisibility(View.GONE);
        }

        Button button = (Button) dialog.findViewById(R.id.buttonOpenInAppPurchaseId);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.OPEN_IN_APP_PURCHASE_DIALOG, null));
                ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.IN_APP_PURCHASE_CUSTOM_DIALOG_GET_FULL_VERSION_CLICKED, "");
            }
        });

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


}


