package com.shorthand.edata.pojoeventbus;

import android.os.Bundle;

/**
 * Created by tushar on 29/07/17.
 */

public class MessageEventServiceMethods {
    String eventName;
    Bundle bundleData;

    public MessageEventServiceMethods(String eventName, Bundle bundleData) {
        this.eventName = eventName;
        this.bundleData = bundleData;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Bundle getBundleData() {
        return bundleData;
    }

    public void setBundleData(Bundle bundleData) {
        this.bundleData = bundleData;
    }
}
