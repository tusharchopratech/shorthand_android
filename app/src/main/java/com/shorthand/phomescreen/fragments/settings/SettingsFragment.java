package com.shorthand.phomescreen.fragments.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

public class SettingsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String className = this.getClass().getSimpleName();

    private String mParam1;
    private String mParam2;

    Context context;
    LinearLayout linearLayoutReset;
    View view;

    private OnFragmentInteractionListener mListener;

    public SettingsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void initializeVariables(View view) {
        this.view = view;
        context = view.getContext();
        linearLayoutReset = (LinearLayout) view.findViewById(R.id.linearLayoutSettingsFragmentResetId);
        setToolbar();
        linearLayoutReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SETTINGS_RESET_PROGRESS_LAYOUT_CLICKED,"");
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Are you sure you want to Reset?");
                builder1.setCancelable(true);

                builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new BackgroundProcess().execute();
                        ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SETTINGS_RESET_PROGRESS_DIALOG_YES_CLICKED,"");
                        dialog.cancel();
                    }
                });

                builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SETTINGS_RESET_PROGRESS_DIALOG_NO_CLICKED,"");
                        dialog.cancel();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
        ShorthandAnalytics.trackScreen(context, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_FRAGMENT, className);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) ((Activity) view.getContext()).findViewById(R.id.toolbarHomeScreenId);
        LinearLayout linearLayout = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarHomeScreenId);
        linearLayout.removeAllViews();
        linearLayout.setGravity(Gravity.CENTER);
        TextView textView = new TextView(context);
        textView.setText("Settings");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        textView.setPadding(0, 0, General.dpToPx(72, context), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(45);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.addView(textView, layoutParams);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initializeVariables(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class BackgroundProcess extends AsyncTask<Void,Void,Void>{
        String status;

        public BackgroundProcess(){
            status = "";
        }

        @Override
        protected Void doInBackground(Void... params) {
            status = new RestBoundary().setProgressReset(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                                General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(status!=null && status.equals("SUCCESS")){
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("You progress is reset.");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }else{
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Something went wrong. Try After some time.");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
    }
}
