package com.shorthand.pofflinedictations.fragments;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;

import java.util.ArrayList;

import io.gresse.hugo.vumeterlibrary.VuMeterView;

/**
 * Created by tushar on 10/08/17.
 */

public class RecyclerViewAdapterOfflineDictationList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<DictationInfo> dictationInfoArrayList;
    ArrayList<String> viewedArrayList, practicedArrayList;
    RecyclerView recyclerView;

    int greenColor;


    public RecyclerViewAdapterOfflineDictationList(ArrayList<DictationInfo> dictationInfos, Context context, RecyclerView recyclerView) {
        this.context = context;
        this.dictationInfoArrayList = dictationInfos;
        this.recyclerView = recyclerView;
        greenColor = ResourcesCompat.getColor(context.getResources(), R.color.colorAccent, null);
        this.viewedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflineViewedDictations(context));
        this.practicedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflinePracticedDictations(context));

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_recycler_view_dictation_list_offline, parent, false);
        RecyclerView.ViewHolder holder = new DictationListOfflineViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final DictationListOfflineViewHolder dictationListOfflineViewHolder = (DictationListOfflineViewHolder) holder;
        String topic = dictationInfoArrayList.get(position).getTopic();
        String speakerName = dictationInfoArrayList.get(position).getSpeaker_name();

        if (topic != null) {
            dictationListOfflineViewHolder.textViewTopic.setText(General.capsFirst(topic));
        } else {

        }
        if (speakerName != null) {
            dictationListOfflineViewHolder.textViewSpeakerName.setText(General.capsFirst(speakerName));
        } else {

        }


        if (MusicPlayerService.dictationInfo != null && MusicPlayerService.dictationInfo.getUuid()
                .equals(dictationInfoArrayList.get(position).getUuid())) {
            if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.PLAYING)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "PLAY");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.PAUSED)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "PAUSED");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.COMPLETED)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.ERROR)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.STOPPED)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.LOADING)) {
                setItemUiStateWhileRendering(dictationListOfflineViewHolder, "LOADING");
            } else {

            }

        } else {
            dictationListOfflineViewHolder.view.setBackgroundColor(Color.GRAY);
            dictationListOfflineViewHolder.textViewTopic.setTextColor(Color.parseColor("#6F747C"));
            dictationListOfflineViewHolder.vuMeterView.setVisibility(View.GONE);
            dictationListOfflineViewHolder.progressBar.setVisibility(View.GONE);
            dictationListOfflineViewHolder.imageViewPlay.setVisibility(View.VISIBLE);
        }

        if (practicedArrayList.contains(dictationInfoArrayList.get(position).getUuid())) {
            dictationListOfflineViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_done);
        } else if (viewedArrayList.contains(dictationInfoArrayList.get(position).getUuid())) {
            dictationListOfflineViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_viewed);
        } else {
            dictationListOfflineViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_new);
        }

        dictationListOfflineViewHolder.linearLayoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(General.isThisServiceRunning(context, MusicPlayerService.class)
                        && MusicPlayerService.dictationInfo != null
                        && MusicPlayerService.dictationInfo.getUuid().equals(dictationInfoArrayList.get(position).getUuid()))) {

                    OfflineDictationsActivity.audioPlayer.startAudio(dictationInfoArrayList.get(position), Constants.AUDIO_SOURCE.OFFLINE);

                    if (MusicPlayerService.dictationInfo != null && MusicPlayerService.dictationInfo.getUuid().equals("")) {

                    } else {
                        int oldDictationIndex = 0;
                        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
                            if (MusicPlayerService.dictationInfo != null && dictationInfoArrayList.get(i).getUuid()
                                    .equals(MusicPlayerService.dictationInfo.getUuid())) {
                                oldDictationIndex = i;
                                break;
                            } else {

                            }
                        }
                        DictationListOfflineViewHolder obj = (DictationListOfflineViewHolder) recyclerView
                                .findViewHolderForAdapterPosition(oldDictationIndex);
                        if (obj != null) {
                            obj.view.setBackgroundColor(Color.GRAY);
                            obj.textViewTopic.setTextColor(Color.parseColor("#6F747C"));
                            obj.vuMeterView.setVisibility(View.GONE);
                            obj.progressBar.setVisibility(View.GONE);
                            obj.imageViewPlay.setVisibility(View.VISIBLE);
                        } else {

                        }
                    }

                    dictationListOfflineViewHolder.view.setBackgroundColor(greenColor);
                    dictationListOfflineViewHolder.textViewTopic.setTextColor(greenColor);
                    dictationListOfflineViewHolder.vuMeterView.setVisibility(View.GONE);
                    dictationListOfflineViewHolder.progressBar.setVisibility(View.VISIBLE);
                    dictationListOfflineViewHolder.imageViewPlay.setVisibility(View.GONE);


                    ArrayList<String> viewedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflineViewedDictations(context));
                    ArrayList<String> practicedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflinePracticedDictations(context));

                    if (!practicedArrayList.contains(dictationInfoArrayList.get(position).getUuid()) &&
                            !viewedArrayList.contains(dictationInfoArrayList.get(position).getUuid())) {
                        viewedArrayList.add(dictationInfoArrayList.get(position).getUuid());
                        new CacheBoundary().setOfflineViewedDictations(context, General.getArrayAsCommaSeparatedString(viewedArrayList));
                    } else {

                    }
                } else {

                }
            }
        });

    }

    public void onMessageEvent(MessageEventData event) {
        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED)) {
            setItemUiState("PLAY");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED_AFTER_PAUSED)) {
            setItemUiState("PLAY");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PAUSED)) {
            setItemUiState("PAUSED");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_CLOSED)) {
            stopAllItems();
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_COMPLETED)) {
            setItemUiState("STOP");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_ERROR)) {
            setItemUiState("STOP");
        } else {

        }
    }

    private void stopAllItems() {
        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
            DictationListOfflineViewHolder obj = (DictationListOfflineViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
            if (obj != null) {
                obj.view.setBackgroundColor(Color.GRAY);
                obj.textViewTopic.setTextColor(Color.parseColor("#6F747C"));
                obj.vuMeterView.setVisibility(View.GONE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.VISIBLE);
            } else {

            }
        }
    }

    public void setItemUiState(String itemUiState) {
        int index = 0;
        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
            if (MusicPlayerService.dictationInfo != null && dictationInfoArrayList.get(i).getUuid().equals(MusicPlayerService.dictationInfo.getUuid())) {
                index = i;
                break;
            } else {

            }
        }
        DictationListOfflineViewHolder obj = (DictationListOfflineViewHolder) recyclerView.findViewHolderForAdapterPosition(index);
        if (obj != null) {
            if (itemUiState.equals("PLAY")) {
                obj.view.setBackgroundColor(greenColor);
                obj.textViewTopic.setTextColor(greenColor);
                obj.vuMeterView.setColor(greenColor);
                obj.vuMeterView.resume(true);
                obj.vuMeterView.setVisibility(View.VISIBLE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.GONE);
            } else if (itemUiState.equals("PAUSED")) {
                obj.view.setBackgroundColor(greenColor);
                obj.textViewTopic.setTextColor(greenColor);
                obj.vuMeterView.pause();
                obj.vuMeterView.setColor(Color.LTGRAY);
                obj.vuMeterView.setVisibility(View.VISIBLE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.GONE);
            } else if (itemUiState.equals("STOP")) {
                obj.view.setBackgroundColor(Color.GRAY);
                obj.textViewTopic.setTextColor(Color.parseColor("#6F747C"));
                obj.vuMeterView.setVisibility(View.GONE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.VISIBLE);
            } else {

            }
        } else {

        }
    }


    public void setItemUiStateWhileRendering(DictationListOfflineViewHolder obj, String itemUiState) {
        if (itemUiState.equals("PLAY")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.setColor(greenColor);
            obj.vuMeterView.setVisibility(View.VISIBLE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else if (itemUiState.equals("PAUSED")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.pause();
            obj.vuMeterView.setColor(Color.LTGRAY);
            obj.vuMeterView.setVisibility(View.VISIBLE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else if (itemUiState.equals("STOP")) {
            obj.view.setBackgroundColor(Color.GRAY);
            obj.textViewTopic.setTextColor(Color.parseColor("#6F747C"));
            obj.vuMeterView.setVisibility(View.GONE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.VISIBLE);
        } else if (itemUiState.equals("LOADING")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.setVisibility(View.GONE);
            obj.progressBar.setVisibility(View.VISIBLE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else {

        }
    }


    @Override
    public int getItemCount() {
        return dictationInfoArrayList.size();
    }

    public class DictationListOfflineViewHolder extends RecyclerView.ViewHolder {

        TextView textViewSpeakerName, textViewTopic;
        View view;
        ImageView imageViewPlay;
        VuMeterView vuMeterView;
        ProgressBar progressBar;
        LinearLayout linearLayoutParent;
        ImageView imageViewStatus;


        public DictationListOfflineViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.viewLeftSideId);
            textViewSpeakerName = (TextView) itemView.findViewById(R.id.textViewSpeakerNameId);
            textViewTopic = (TextView) itemView.findViewById(R.id.textViewTopicNameId);
            linearLayoutParent = (LinearLayout) itemView.findViewById(R.id.linearLayoutInflateDictationListId);
            vuMeterView = (VuMeterView) itemView.findViewById(R.id.vuMeterViewInflateDictationListId);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressbarInflateDictationListId);
            imageViewPlay = (ImageView) itemView.findViewById(R.id.imageViewPlayInflateDictationListId);
            imageViewStatus = (ImageView) itemView.findViewById(R.id.imageViewStatusId);

        }
    }

}
