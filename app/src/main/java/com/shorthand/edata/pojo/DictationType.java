package com.shorthand.edata.pojo;

import java.io.Serializable;

/**
 * Created by tushar on 12/07/17.
 */

public class DictationType implements Serializable {
    private String duration;

    private String speed;

    private String language;

    private String times_played;

    private String number_of_recordings;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimes_played() {
        return times_played;
    }

    public void setTimes_played(String times_played) {
        this.times_played = times_played;
    }

    public String getNumber_of_recordings() {
        return number_of_recordings;
    }

    public void setNumber_of_recordings(String number_of_recordings) {
        this.number_of_recordings = number_of_recordings;
    }

    @Override
    public String toString() {
        return "ClassPojo [duration = " + duration + ", speed = " + speed + ", language = " + language + ", times_played = " + times_played + "]";
    }

}
