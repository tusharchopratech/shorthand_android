package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.pojo.OfflineFileData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by tushar on 13/07/17.
 */

public class RetrofitGetOfflineFileData {

    public OfflineFileData getOfflineFileData(Context context, String baseUrl, String token, String key) {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/dictation/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<OfflineFileData> c = api.sendData(token, key);
        try {
            Response<OfflineFileData> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return resp.body();
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    interface ApiInterface {
        @POST("v1/get_offline_file_data")
        Call<OfflineFileData> sendData(@Header("authorization") String authorization, @Header("key") String key);
    }
}
