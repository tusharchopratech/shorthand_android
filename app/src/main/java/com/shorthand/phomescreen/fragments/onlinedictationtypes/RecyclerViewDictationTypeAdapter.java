package com.shorthand.phomescreen.fragments.onlinedictationtypes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amulyakhare.textdrawable.TextDrawable;
import com.shorthand.R;
import com.shorthand.edata.ads.AdsProcessor;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.fragments.onlinedictationlist.DictationListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by tushar on 14/07/17.
 */

class RecyclerViewDictationTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<DictationType> dictationTypeArrayList;
    FragmentManager fragmentManagerMain;
    String className = this.getClass().getSimpleName();

    public RecyclerViewDictationTypeAdapter(ArrayList<DictationType> dictationTypes, Context context,
                                            FragmentManager fragmentManagerMain) {
        this.context = context;
        this.dictationTypeArrayList = dictationTypes;
        this.fragmentManagerMain = fragmentManagerMain;
        this.dictationTypeArrayList = sortDictationTypeArrayList(this.dictationTypeArrayList);
    }

    private ArrayList<DictationType> sortDictationTypeArrayList(ArrayList<DictationType> dictationTypeArrayList) {
        ArrayList<String> language = new ArrayList<>();
        ArrayList<DictationType> tmp = new ArrayList<>();

        for (int i = 0; i < dictationTypeArrayList.size(); i++) {
            if (!language.contains(dictationTypeArrayList.get(i).getLanguage())) {
                language.add(dictationTypeArrayList.get(i).getLanguage());
            }
        }

        Collections.sort(language, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        for (int i = 0; i < language.size(); i++) {
            for (int j = 0; j < dictationTypeArrayList.size(); j++) {
                if (dictationTypeArrayList.get(j).getLanguage().equals(language.get(i))) {
                    tmp.add(dictationTypeArrayList.get(j));
                }
            }
        }
        return tmp;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_recycler_view_dictation_types, parent, false);
        RecyclerView.ViewHolder holder = new DictationTypeViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        DictationTypeViewHolder dictationTypeViewHolder = (DictationTypeViewHolder) holder;


        int color;

        if (dictationTypeArrayList.get(position).getLanguage().toLowerCase().equals("english")) {
            color = Color.parseColor("#835583");
        } else if (dictationTypeArrayList.get(position).getLanguage().toLowerCase().equals("hindi")) {
            color = Color.parseColor("#E6C72E");
        } else {
            color = Color.parseColor("#62A5C0");
        }

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                //.fontSize(30) /* size in px */
                //.bold()
                .toUpperCase()
                .withBorder(4)
                .endConfig()
                .buildRoundRect(String.valueOf(dictationTypeArrayList.get(position).getLanguage()
                        .charAt(0)).toUpperCase(), color, 10);

        dictationTypeViewHolder.imageViewDictationType.setImageDrawable(drawable);
        dictationTypeViewHolder.textViewPracticed.setText(General.getNumberStringWithSuffix(Integer.parseInt(dictationTypeArrayList.get(position).getTimes_played())) + " Plays");
        dictationTypeViewHolder.textViewSpeed.setText(String.valueOf(dictationTypeArrayList.get(position).getSpeed()) + " wpm / " + String.valueOf(dictationTypeArrayList.get(position).getDuration()) + " minutes");
        dictationTypeViewHolder.textViewTime.setText(String.valueOf(dictationTypeArrayList.get(position).getNumber_of_recordings()) + "  passages");
        dictationTypeViewHolder.textViewLanguage.setText(General.capsFirst(dictationTypeArrayList.get(position).getLanguage()));


        dictationTypeViewHolder.linearLayoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(dictationTypeArrayList.get(position));

                ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.DICTATIONS_TYPE_CLICKED,
                        dictationTypeArrayList.get(position).getLanguage() + "/" + dictationTypeArrayList.get(position).getSpeed()
                                + "/" + dictationTypeArrayList.get(position).getDuration());

            }
        });

        if (new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            dictationTypeViewHolder.imageViewOffline.setVisibility(View.INVISIBLE);
        } else {
            dictationTypeViewHolder.imageViewOffline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    General.openPaymentDialog(context);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return dictationTypeArrayList.size();
    }


    public void setMainFragment(DictationType dictationType) {
        //DictationListFragment dictationListFragment = new DictationListFragment();
        //dictationListFragment.setDictationType(dictationType);
        try {
            Fragment fragment = DictationListFragment.newInstance(dictationType);
            fragmentManagerMain
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.main_container, fragment)
                    .disallowAddToBackStack()
                    .commit();
            new AdsProcessor(context).showInterstitial(className);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
