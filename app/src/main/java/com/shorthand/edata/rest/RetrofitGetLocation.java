package com.shorthand.edata.rest;

import android.content.Context;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class RetrofitGetLocation {

    public Bundle getMyLocation(final Context context) {


        String baseUrl = "https://www.googleapis.com/geolocation/";
        String api_key = "AIzaSyBeooKWNhWPuwYBXhtwwsSKGtyDA1ahn2Q";

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);

        Call<ApiResponse> c = api.sendData(api_key);
        try {
            Response<ApiResponse> resp = c.execute();
            if (resp != null && resp.body() != null && resp.code() == 200) {
                Bundle bundle = new Bundle();
                bundle.putString("lati", resp.body().getLocation().getLat());
                bundle.putString("longi", resp.body().getLocation().getLng());
                bundle.putString("accuracy", resp.body().getAccuracy());
                return bundle;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public class ApiResponse {

        private Location location;

        private String accuracy;

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public String getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(String accuracy) {
            this.accuracy = accuracy;
        }

        @Override
        public String toString() {
            return "ClassPojo [location = " + location + ", accuracy = " + accuracy + "]";
        }

        public class Location {
            private String lng;

            private String lat;

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            @Override
            public String toString() {
                return "ClassPojo [lng = " + lng + ", lat = " + lat + "]";
            }
        }

    }

    public interface ApiInterface {
        @POST("v1/geolocate")
        Call<ApiResponse> sendData(@Query("key") String userId);
    }

}
