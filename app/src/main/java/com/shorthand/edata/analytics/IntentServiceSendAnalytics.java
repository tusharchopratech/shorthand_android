package com.shorthand.edata.analytics;

import android.app.IntentService;
import android.content.Intent;

import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.AnalyticsPojo;
import com.shorthand.edata.pojo.AnalyticsPojoRealm;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class IntentServiceSendAnalytics extends IntentService {


    public IntentServiceSendAnalytics() {
        super("IntentServiceSendAnalytics");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<AnalyticsPojoRealm> result = realm.allObjects(AnalyticsPojoRealm.class);
            if (result.size() >= Constants.SHORTHAND_ANALYTICS_RECEIVE_SIZE) {
                AnalyticsPojo[] analyticsPojoArray = new AnalyticsPojo[result.size()];
                for (int i = 0; i < result.size(); i++) {
                    AnalyticsPojoRealm object = result.get(i);
                    analyticsPojoArray[i] = new AnalyticsPojo(object.getId()
                            , object.getCategory(), object.getAction(), object.getLabel(), object.getEpochTime());
                }
                final String[] ids = new RestBoundary().setShorthandAnalytics(this, Constants.BASE_URL, new CacheBoundary().getMyToken(this),
                        General.getShaKey(new CacheBoundary().getMyUuid(this), new CacheBoundary().getMyEmail(this)),
                        analyticsPojoArray);

                if (ids != null && ids.length > 0) {
                    for (int i = 0; i < ids.length; i++) {
                        final int finalI = i;
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                RealmResults<AnalyticsPojoRealm> rows = realm.where(AnalyticsPojoRealm.class)
                                        .equalTo("id", ids[finalI])
                                        .findAll();
                                rows.clear();
                            }
                        });
                    }
                }

            }
            realm.close();
        }
    }


}
