package com.shorthand.phomescreen.fragments.onlinedictationlist;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.HomeScreenNavigationDrawerActivity;
import com.shorthand.phomescreen.taptagetview.TapTargetViewProcessor;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class DictationListFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    String className = this.getClass().getSimpleName();

    DictationType dictationType;
    Context context;
    ArrayList<DictationInfo> arrayListDictationInfo;
    AVLoadingIndicatorView avLoadingIndicatorView;
    RecyclerView recyclerView;
    LinearLayout linearLayoutRetry;
    Button buttonRetry;
    View view;
    RecyclerViewDictationListAdapter recyclerViewDictationListAdapter;


    public DictationListFragment() {
        // Required empty public constructor
    }

    public static DictationListFragment newInstance(DictationType dictationType) {
        DictationListFragment fragment = new DictationListFragment();
        Bundle args = new Bundle();
        args.putSerializable("dictationType", dictationType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dictationType = (DictationType) getArguments().getSerializable("dictationType");
        }
    }

    public void initializeVariables(View view) {
        this.view = view;
        EventBus.getDefault().register(this);
        HomeScreenNavigationDrawerActivity.currentFragmentName = "DictationListFragment";
        context = view.getContext();
        avLoadingIndicatorView = (AVLoadingIndicatorView) view.findViewById(R.id.aVLoadingIndicatorViewDictationInfoId);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewDictationListId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        linearLayoutRetry = (LinearLayout) view.findViewById(R.id.linearLayoutFragmentDictationListRetryId);
        buttonRetry = (Button) view.findViewById(R.id.buttonDictationListNetworkProblemId);
        ShorthandAnalytics.trackScreen(context, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_FRAGMENT, className);
        setToolbar();
        //recyclerView.setHasFixedSize(true);
        //recyclerView.setItemViewCacheSize(100);
        //recyclerView.setDrawingCacheEnabled(true);
        //recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) ((Activity) view.getContext()).findViewById(R.id.toolbarHomeScreenId);
        LinearLayout linearLayoutParent = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarHomeScreenId);
        linearLayoutParent.removeAllViews();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(context);
        textView.setText("Dictations");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        //textView.setPadding(0, 0, General.dpToPx(72, context), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(25);

        TextView textViewSubtitle = new TextView(context);
        textViewSubtitle.setText(General.capsFirst(dictationType.getLanguage().toLowerCase()) + "/"
                + dictationType.getSpeed() + "/" + dictationType.getDuration());
        textViewSubtitle.setGravity(Gravity.CENTER);
        textViewSubtitle.setTextSize(10);
        textViewSubtitle.setTextColor(Color.WHITE);
        //textViewSubtitle.setPadding(0, 0, General.dpToPx(72, context), 0);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        linearLayout.addView(textView, layoutParams);
        linearLayout.addView(textViewSubtitle, layoutParams);
        linearLayoutParent.addView(linearLayout, layoutParams);

        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.vd_download_white, null));
        imageView.setPadding(General.dpToPx(16, context), General.dpToPx(16, context)
                , General.dpToPx(16, context), General.dpToPx(16, context));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            imageView.setVisibility(View.INVISIBLE);
        } else {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    General.openPaymentDialog(context);
                }
            });
        }
        layoutParams = new LinearLayout.LayoutParams(General.dpToPx(72, context), LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayoutParent.addView(imageView, layoutParams);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dictation_list, container, false);
        initializeVariables(view);
        new BackgroundWork().execute();

        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (General.isNetworkAvailable(context)) {
                    new BackgroundWork().execute();
                }
            }
        });

        return view;
    }


    public void setDictationType(DictationType dictationType) {
        this.dictationType = dictationType;
    }


    private void renderRecyclerView() {
        if (recyclerView != null && arrayListDictationInfo != null) {
            recyclerViewDictationListAdapter = new RecyclerViewDictationListAdapter(arrayListDictationInfo, context, recyclerView);
            recyclerView.setAdapter(recyclerViewDictationListAdapter);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventData event) {
        if (recyclerViewDictationListAdapter != null) {
            recyclerViewDictationListAdapter.onMessageEvent(event);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        EventBus.getDefault().unregister(this);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class BackgroundWork extends AsyncTask<Void, Void, Void> {

        public BackgroundWork() {
            avLoadingIndicatorView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            linearLayoutRetry.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            arrayListDictationInfo = new IDictationListFragment().getDictationList(context, dictationType);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (arrayListDictationInfo != null) {
                recyclerView.animate()
                        .alpha(1.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                        });
                avLoadingIndicatorView.animate()
                        .alpha(0.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                avLoadingIndicatorView.setVisibility(View.GONE);
                            }
                        });
                linearLayoutRetry.animate()
                        .alpha(0.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                linearLayoutRetry.setVisibility(View.GONE);
                            }
                        });
                renderRecyclerView();


                //new TapTargetViewProcessor(context).showDictationListFragmentTapTargetView(recyclerView);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (DictationListFragment.this.isVisible()) {
                            new TapTargetViewProcessor(context).showDictationListFragmentTapTargetView(recyclerView);
                        }
                    }
                }, 1000);

            } else {
                linearLayoutRetry.setVisibility(View.VISIBLE);
                avLoadingIndicatorView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }
        }
    }


}
