package com.shorthand.phomescreen.activity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationInfo;

/**
 * Created by tushar on 29/08/17.
 */

class ViewPagerAdapter extends PagerAdapter {
    Context context;
    DictationInfo dictationInfo;

    public ViewPagerAdapter(Context context, DictationInfo dictationInfo) {
        this.context = context;
        this.dictationInfo = dictationInfo;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (position == 0) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_item_ratings, container, false);

            TextView textViewRatingName = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingNameId);
            TextView textViewRating = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingId);
            TextView textViewRatingTotal = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingTotalId);
            textViewRatingName.setText("Modulation : ");
            if (!dictationInfo.getMeta().getModulation_total().equals("0")) {
                textViewRating.setText(dictationInfo.getMeta().getModulation());
                textViewRatingTotal.setText(General.getNumberString(Integer.parseInt(dictationInfo.getMeta().getModulation_total())));
            }
            ((ViewPager) container).addView(itemView);
            return itemView;
        } else if (position == 1) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_item_ratings, container, false);

            TextView textViewRatingName = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingNameId);
            TextView textViewRating = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingId);
            TextView textViewRatingTotal = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingTotalId);
            textViewRatingName.setText("Passage Difficulty : ");

            if (!dictationInfo.getMeta().getPassage_difficulty_total().equals("0")) {
                textViewRating.setText(dictationInfo.getMeta().getPassage_difficulty());
                textViewRatingTotal.setText(General.getNumberString(Integer.parseInt(dictationInfo.getMeta().getPassage_difficulty_total())));
            }
            ((ViewPager) container).addView(itemView);
            return itemView;
        } else if (position == 2) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_item_ratings, container, false);

            TextView textViewRatingName = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingNameId);
            TextView textViewRating = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingId);
            TextView textViewRatingTotal = (TextView) itemView.findViewById(R.id.viewPagerTextViewRatingTotalId);
            textViewRatingName.setText("Voice Clarity : ");
            if (!dictationInfo.getMeta().getVoice_clarity_total().equals("0")) {
                textViewRating.setText(dictationInfo.getMeta().getVoice_clarity());
                textViewRatingTotal.setText(General.getNumberString(Integer.parseInt(dictationInfo.getMeta().getVoice_clarity_total())));
            }
            ((ViewPager) container).addView(itemView);
            return itemView;
        } else {
            return null;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}
