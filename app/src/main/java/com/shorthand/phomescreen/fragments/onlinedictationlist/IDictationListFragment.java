package com.shorthand.phomescreen.fragments.onlinedictationlist;

import android.content.Context;

import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by tushar on 18/07/17.
 */

public class IDictationListFragment {
    public ArrayList<DictationInfo> getDictationList(Context context, DictationType dictationType) {

        DictationInfo[] dictationInfos = new RestBoundary().getDictationList(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)),
                dictationType.getLanguage(), Integer.parseInt(dictationType.getDuration()), Integer.parseInt(dictationType.getSpeed()));
        if (dictationInfos == null) {
            return null;
        }
        return new ArrayList<DictationInfo>(Arrays.asList(dictationInfos));
    }

}
