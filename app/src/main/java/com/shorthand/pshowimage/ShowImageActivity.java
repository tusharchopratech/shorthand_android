package com.shorthand.pshowimage;

/**
 * Created by tushar on 25/07/17.
 */

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.TouchImageView;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class ShowImageActivity extends AppCompatActivity {

    String className = this.getClass().getSimpleName();
    TouchImageView touchImageView;
    String thumbnailUrl = "";
    Toolbar toolbar;


    private void initializeVariables() {
        touchImageView = (TouchImageView) findViewById(R.id.touchImageViewShowImageId);
        ShorthandAnalytics.trackScreen(ShowImageActivity.this, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_ACTIVITY, className);
        setToolbar();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbarShowImageId);
        toolbar.setNavigationIcon(R.drawable.vd_back_white);
        LinearLayout linearLayout = (LinearLayout) toolbar.findViewById(R.id.linearLayoutShowImageId);
        linearLayout.removeAllViews();
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(ShowImageActivity.this);
        textView.setText("Transcription");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        textView.setPadding(0, 0, General.dpToPx(80, ShowImageActivity.this), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(25);

        TextView textViewSubtitle = new TextView(ShowImageActivity.this);
        textViewSubtitle.setText(General.capsFirst(getIntent().getStringExtra("topic")));
        textViewSubtitle.setGravity(Gravity.CENTER);
        textViewSubtitle.setTextSize(10);
        textViewSubtitle.setTextColor(Color.WHITE);
        textViewSubtitle.setPadding(0, 0, General.dpToPx(80, ShowImageActivity.this), 0);

        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.addView(textView, layoutParams);
        linearLayout.addView(textViewSubtitle, layoutParams);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        initializeVariables();

        setSupportActionBar(toolbar);

        if (getIntent().getStringExtra("imageSource").equals(Constants.AUDIO_SOURCE.ONLINE)) {
            thumbnailUrl = getIntent().getStringExtra("imageUrl");
            if (thumbnailUrl != null && !thumbnailUrl.equals("")) {
                Picasso
                        .with(ShowImageActivity.this)
                        .load(thumbnailUrl)
                        .resize(General.getScreenWidth(ShowImageActivity.this), General.getScreenWidth(ShowImageActivity.this))
                        .centerInside()
                        .placeholder(R.drawable.place_hoslder_2)
                        .error(R.drawable.place_hoslder_2)
                        .into(this.touchImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                finish();
                            }
                        });
            } else {
                onBackPressed();
            }
        } else if (getIntent().getStringExtra("imageSource").equals(Constants.AUDIO_SOURCE.OFFLINE)) {
            touchImageView.setEnabled(false);
            new BackgroundProcess(getIntent().getIntExtra("position", 0), getIntent().getStringExtra("recordNumber")).execute();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            ShorthandAnalytics.trackEvent(ShowImageActivity.this, Constants.ANALYTICS.EVENT_TYPE.TOP_LEFT_ICON_BACK_PRESSED + "/" + className, "activity_closed");
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class BackgroundProcess extends AsyncTask<Void, Void, Void> {

        String recordNumber;
        int position;
        Bitmap bitmap;

        public BackgroundProcess(int position, String recordNumber) {
            this.position = position;
            this.recordNumber = recordNumber;
            findViewById(R.id.aVLoadingIndicatorViewShowImageId).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            bitmap = new OfflineDictationsBoundary().getImages(recordNumber
                    , new CacheBoundary().getOfflineFilePassword(ShowImageActivity.this)
                    , new CacheBoundary().getOfflineFileIv(ShowImageActivity.this)).get(position);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            findViewById(R.id.aVLoadingIndicatorViewShowImageId).setVisibility(View.GONE);
            if (bitmap != null) {
                touchImageView.setEnabled(true);
                touchImageView.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(touchImageView.isZoomed()){
            touchImageView.resetZoom();
        }else {
            ShorthandAnalytics.trackEvent(ShowImageActivity.this, Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED + "/" + className, "activity_closed");
            super.onBackPressed();
        }
    }
}

