package com.shorthand.edata.analytics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.pojo.AnalyticsPojoRealm;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * This is general class for analytics used.
 */

public class ShorthandAnalytics {

    /**
     * This method is used to track screen in Google Analytics.
     *
     * @param context
     * @param screenType
     * @param screenName
     */
    public static void trackScreen(Context context, String screenType, String screenName) {
        if (Variables.IS_ONLINE_MODE) {
            AnalyticsApplicationGA application = (AnalyticsApplicationGA) ((Activity) context).getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName(screenType + " : " + screenName);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            trackShorthandAnalytics(context, "screen_track", screenType, screenName);
        }
    }

    /**
     * This method is used to track event in Google Analytics.
     *
     * @param context
     * @param action
     * @param label
     */
    public static void trackEvent(Context context, String action, String label) {
        if (Variables.IS_ONLINE_MODE) {
            trackGoogleAnalytics(context, action, label);
            trackFirebaseAnalytics(context, action, label);
            trackAnswersAnalytics(context, action, label);
            trackShorthandAnalytics(context, "event_track", action, label);
        }
    }

    /**
     * This method is used to send events in Answers.
     */
    private static void trackAnswersAnalytics(Context context, String action, String label) {
        Answers.getInstance().logCustom(new CustomEvent(new CacheBoundary().getMyUuid(context)).putCustomAttribute(action, label));
    }

    private static void trackFirebaseAnalytics(Context context, String action, String label) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle bundle = new Bundle();
        bundle.putString(action, label);
        firebaseAnalytics.logEvent(new CacheBoundary().getMyUuid(context), bundle);
    }

    private static void trackGoogleAnalytics(Context context, String action, String label) {
        AnalyticsApplicationGA application = (AnalyticsApplicationGA) ((Activity) context).getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder()
                .setCampaignParamsFromUrl(new CacheBoundary().getMyReferrer(context))
                .setCategory(new CacheBoundary().getMyUuid(context))
                .setAction(action)
                .setLabel(label)
                .build());
    }


    private static void trackShorthandAnalytics(final Context context, final String category, final String action, final String label) {
        if (Variables.SEND_ANALYTICS) {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            AnalyticsPojoRealm shorthandAnalyticsPojo = new AnalyticsPojoRealm();
            shorthandAnalyticsPojo.setId(UUID.randomUUID().toString());
            shorthandAnalyticsPojo.setAction(action);
            shorthandAnalyticsPojo.setLabel(label);
            shorthandAnalyticsPojo.setCategory(category);
            shorthandAnalyticsPojo.setEpochTime(System.currentTimeMillis());
            realm.copyToRealmOrUpdate(shorthandAnalyticsPojo);
            realm.commitTransaction();
            RealmResults<AnalyticsPojoRealm> result = realm.allObjects(AnalyticsPojoRealm.class);
            if (result.size() >= Constants.SHORTHAND_ANALYTICS_RECEIVE_SIZE) {
                if (!General.isThisServiceRunning(context, IntentServiceSendAnalytics.class)) {
                    context.startService(new Intent(context, IntentServiceSendAnalytics.class));
                }
            }
            realm.close();
        }
    }
}
