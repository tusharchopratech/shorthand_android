package com.shorthand.plogin;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.shorthand.BuildConfig;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.AddUserData;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.pojo.UserDetails;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import java.util.List;
import java.util.Locale;

/**
 * Created by tushar on 25/03/17.
 */

class ILoginActivity {

    public String sendDataAndStore(String name, String sex, String dob, String email, String id, String accountType, String picUrl, Context context) {
        int versionCode = 1;
        String location = "", locationAccuracy = "", locationCity = "", locationCountry = "", referrer = "";
        if (picUrl == null) {
            picUrl = "";
        }
        referrer = new CacheBoundary().getMyReferrer(context);
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            versionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Bundle bundle = new RestBoundary().getMyLocation(context);
            if (bundle != null) {
                location = bundle.getString("lati") + ", " + bundle.getString("longi");
                locationAccuracy = bundle.getString("accuracy");
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(bundle.getString("lati")), Double.parseDouble(bundle.getString("longi")), 1);
                if (addresses != null && addresses.size() > 0) {
                    // String cityName = addresses.get(0).getAddressLine(0);
                    String stateName = addresses.get(0).getLocality();
                    String countryName = addresses.get(0).getCountryName() + ", " + addresses.get(0).getCountryCode();
                    if (stateName != null && stateName.length() > 0) {
                        locationCity = stateName;
                    }
                    if (countryName != null && countryName.length() > 0) {
                        locationCountry = countryName;
                    }
                }
            }

        } catch (Exception e) {
            locationCity = "";
            locationCountry = "";
            e.printStackTrace();
        }
        AddUserData addUserData = new AddUserData(name, email, accountType, id, picUrl,
                "android", String.valueOf(versionCode), location, locationCity, locationCountry,
                referrer, locationAccuracy, new CacheBoundary().getMyDeviceName(context));

        Bundle bundle = new RestBoundary().addUserAndGetUserId(context, Constants.BASE_URL, addUserData);
        if (bundle == null) {
            return "FAILED";
        } else {
            try {
                String token = bundle.getString("token");
                String refreshToken = bundle.getString("refresh_token");
                String uuid = bundle.getString("uuid");
                String type = bundle.getString("type");

                CacheBoundary cacheBoundary = new CacheBoundary();
                cacheBoundary.setMyName(context, name);
                cacheBoundary.setMyDob(context, dob);
                cacheBoundary.setMyEmail(context, email);
                cacheBoundary.setMyGender(context, sex);
                cacheBoundary.setMyPicUrl(context, picUrl);
                cacheBoundary.setMyRefreshToken(context, refreshToken);
                cacheBoundary.setMyToken(context, token);
                cacheBoundary.setMyUuid(context, uuid);
                cacheBoundary.setMyCountryName(context, locationCountry);


                if (type != null && type.equals("old")) {
                    UserDetails userDetails = new RestBoundary().getUserDetails(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context)
                            , General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)));
                    if (userDetails != null) {
                        cacheBoundary.setMyPhone(context, userDetails.getPhone_no());
                        cacheBoundary.setMyGender(context, userDetails.getGender());
                        cacheBoundary.setMyDob(context, userDetails.getDob());
                    }
                }

                return "SUCCESS";
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "FAILED";
        }
    }

    public InitialAppInfo getInitialAppInfo(Context context) {
        int versionCode = BuildConfig.VERSION_CODE;
        InitialAppInfo initialAppInfo = new RestBoundary().getInitialAppInfo(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context)
                , General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)), String.valueOf(versionCode), "android");
        return initialAppInfo;
    }


}
