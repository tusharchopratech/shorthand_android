package com.shorthand.phomescreen.fragments.onlinedictationtypes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.AnalyticsPojo;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.pojo.AnalyticsPojoRealm;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.HomeScreenNavigationDrawerActivity;
import com.shorthand.phomescreen.taptagetview.TapTargetViewProcessor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

import io.realm.Realm;
import io.realm.RealmResults;

public class DictationTypeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String className = this.getClass().getSimpleName();
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerViewDictationTypes;
    Context context;
    View view;
    Toolbar toolbar;

    public DictationTypeFragment() {

    }

    private void initializeVariables(View view) {
        this.view = view;
        this.context = view.getContext();
        recyclerViewDictationTypes = (RecyclerView) view.findViewById(R.id.recyclerViewDictationTypesId);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                //TODO if the items are filtered, considered hiding the fast scroller here
            }
        };
        recyclerViewDictationTypes.setLayoutManager(layoutManager);
        recyclerViewDictationTypes.setHasFixedSize(true);
        recyclerViewDictationTypes.setItemViewCacheSize(100);
        recyclerViewDictationTypes.setDrawingCacheEnabled(true);
        recyclerViewDictationTypes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        ShorthandAnalytics.trackScreen(context, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_FRAGMENT, className);

        //new AdsProcessor(context).showInterstitial(className);
        setToolbar();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Field field = Toolbar.class.getDeclaredField("mNavButtonView");
                    field.setAccessible(true);
                    View navigationView = (View) field.get(toolbar);
                    new TapTargetViewProcessor(context).showDictationTypeFragmentTapTargetView(
                            recyclerViewDictationTypes, navigationView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    private void setToolbar() {
        toolbar = (Toolbar) ((Activity) view.getContext()).findViewById(R.id.toolbarHomeScreenId);
        LinearLayout linearLayoutParent = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarHomeScreenId);
        linearLayoutParent.removeAllViews();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(context);
        textView.setText("Shorthand");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        //textView.setPadding(0, 0, General.dpToPx(72, context), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(25);

        TextView textViewSubtitle = new TextView(context);
        textViewSubtitle.setText("Types");
        textViewSubtitle.setGravity(Gravity.CENTER);
        textViewSubtitle.setTextSize(10);
        textViewSubtitle.setTextColor(Color.WHITE);
        //textViewSubtitle.setPadding(0, 0, General.dpToPx(72, context), 0);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        linearLayout.addView(textView, layoutParams);
        linearLayout.addView(textViewSubtitle, layoutParams);
        linearLayoutParent.addView(linearLayout, layoutParams);

        ImageView imageViewDownload = new ImageView(context);
        imageViewDownload.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.vd_download_white, null));
        imageViewDownload.setPadding(General.dpToPx(16, context), General.dpToPx(16, context)
                , General.dpToPx(16, context), General.dpToPx(16, context));
        imageViewDownload.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            imageViewDownload.setVisibility(View.INVISIBLE);
        } else {
            imageViewDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    General.openPaymentDialog(context);
                }
            });
        }
        layoutParams = new LinearLayout.LayoutParams(General.dpToPx(72, context), LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayoutParent.addView(imageViewDownload, layoutParams);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dictation_type, container, false);
        initializeVariables(view);
        renderRecyclerView();
        return view;
    }

    public void renderRecyclerView() {
        if (recyclerViewDictationTypes != null && HomeScreenNavigationDrawerActivity.initialAppInfo != null
                && HomeScreenNavigationDrawerActivity.initialAppInfo.getDictation_types() != null) {
            RecyclerViewDictationTypeAdapter recyclerViewDictationTypeAdapter = new RecyclerViewDictationTypeAdapter(
                    new ArrayList<DictationType>(Arrays.asList(HomeScreenNavigationDrawerActivity.initialAppInfo.getDictation_types()))
                    , context, getFragmentManager());
            recyclerViewDictationTypes.setAdapter(recyclerViewDictationTypeAdapter);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
