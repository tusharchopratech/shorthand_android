package com.shorthand.edata.offline;

import android.content.Context;
import android.os.Bundle;

import com.shorthand.edata.general.Constants;
import com.shorthand.edata.pojoeventbus.MessageEventData;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A utility class that encrypts or decrypts a file.
 *
 * @author www.codejava.net
 */
public class CryptoUtils {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CFB8/NoPadding";
    //private static final String TRANSFORMATION = "AES";

    public static Context context;

   /* public static byte[] decrypt(String key, File inputFile) {
        return doCrypto(Cipher.DECRYPT_MODE, key, inputFile);
    }

    private static byte[] doCrypto(int cipherMode, String key, File inputFile) {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            //IvParameterSpec ivSpec = new IvParameterSpec(IV);
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            //FileOutputStream outputStream = new FileOutputStream(outputFile);
            // outputStream.write(outputBytes);

            inputStream.close();

            return outputBytes;
            // outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/


    public static File getDecryptedDataFile(String key,String ivString, File inputFile) {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKey,new IvParameterSpec(ivString.getBytes("UTF-8")));

            File tempFile = File.createTempFile("data", "data2");
            FileInputStream fis = new FileInputStream(inputFile);
            FileOutputStream fos = new FileOutputStream(tempFile);

            byte[] in = new byte[1024 * 1024];
            int read;
            while ((read = fis.read(in)) != -1) {
                byte[] output = cipher.update(in, 0, read);
                if (output != null)
                    fos.write(output);
            }

            byte[] output = cipher.doFinal();
            if (output != null)
                fos.write(output);
            fis.close();
            fos.flush();
            fos.close();
            return tempFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
