package com.shorthand.edata.pojo;

/**
 * Created by tushar on 23/08/17.
 */

public class AddUserData {
    String name, email, login_type, login_unique_id, pic_url,
            platform, platform_version, location, location_city, location_country, referrer, location_accuracy, device_name;

    public AddUserData(String name, String email, String login_type, String login_unique_id,
                       String pic_url, String platform, String platform_version, String location,
                       String location_city, String location_country, String referrer, String location_accuracy, String device_name) {
        this.name = name;
        this.email = email;
        this.login_type = login_type;
        this.login_unique_id = login_unique_id;
        this.pic_url = pic_url;
        this.platform = platform;
        this.platform_version = platform_version;
        this.location = location;
        this.location_city = location_city;
        this.location_country = location_country;
        this.referrer = referrer;
        this.location_accuracy = location_accuracy;
        this.device_name = device_name;
    }

}