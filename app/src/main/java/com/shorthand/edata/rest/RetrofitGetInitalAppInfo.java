package com.shorthand.edata.rest;

import android.content.Context;
import android.util.Log;

import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by tushar on 13/07/17.
 */

public class RetrofitGetInitalAppInfo {

    public InitialAppInfo getInitialAppInfo(Context context, String baseUrl, String token, String key, String appVersion, String platform) {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<InitialAppInfo> c = api.sendData(token, key, new ApiRequest(appVersion, platform));
        try {
            Response<InitialAppInfo> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return resp.body();
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    class ApiRequest {
        String app_version, platform;

        public ApiRequest(String app_version, String platform) {
            this.app_version = app_version;
            this.platform = platform;
        }
    }

    interface ApiInterface {
        @POST("v1/get_initial_info")
        Call<InitialAppInfo> sendData(@Header("authorization") String authorization, @Header("key") String key, @Body ApiRequest apiRequest);
    }
}
