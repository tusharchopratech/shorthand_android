package com.shorthand.edata.pojo;

import java.io.Serializable;

/**
 * Created by tushar on 12/07/17.
 */

public class DictationInfo implements Serializable {

    private String speaker_name;

    private String speaker_gender;

    private String topic;

    private String duration;

    private String speed;

    private int images;

    private String language;

    private String uuid;

    private Meta meta;

    public DictationInfo(String speaker_name, String speaker_gender, String topic,
                         String duration, String speed, int images, String language, String uuid, Meta meta) {
        this.speaker_name = speaker_name;
        this.speaker_gender = speaker_gender;
        this.topic = topic;
        this.duration = duration;
        this.speed = speed;
        this.images = images;
        this.language = language;
        this.uuid = uuid;
        this.meta = meta;
    }

    public String getSpeaker_gender() {
        return speaker_gender;
    }

    public void setSpeaker_gender(String speaker_gender) {
        this.speaker_gender = speaker_gender;
    }

    public String getSpeaker_name() {
        return speaker_name;
    }

    public void setSpeaker_name(String speaker_name) {
        this.speaker_name = speaker_name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "ClassPojo [speaker_name = " + speaker_name + ", topic = " + topic + ", duration = " + duration + ", speed = " + speed + ", images = " + images + ", language = " + language + ", uuid = " + uuid + ", meta = " + meta + "]";
    }

    public class Meta implements Serializable {
        private String tags;

        private String modulation, modulation_total;

        private String voice_clarity, voice_clarity_total;

        private String passage_difficulty, passage_difficulty_total;

        private String times_played;

        private String practiced_status;

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getModulation() {
            return modulation;
        }

        public void setModulation(String modulation) {
            this.modulation = modulation;
        }

        public String getVoice_clarity() {
            return voice_clarity;
        }

        public void setVoice_clarity(String voice_clarity) {
            this.voice_clarity = voice_clarity;
        }

        public String getPassage_difficulty() {
            return passage_difficulty;
        }

        public void setPassage_difficulty(String passage_difficulty) {
            this.passage_difficulty = passage_difficulty;
        }

        public String getTimes_played() {
            return times_played;
        }

        public String getPracticed_status() {
            return practiced_status;
        }

        public void setPracticed_status(String practiced_status) {
            this.practiced_status = practiced_status;
        }

        public void setTimes_played(String times_played) {
            this.times_played = times_played;
        }

        public String getModulation_total() {
            return modulation_total;
        }

        public void setModulation_total(String modulation_total) {
            this.modulation_total = modulation_total;
        }

        public String getVoice_clarity_total() {
            return voice_clarity_total;
        }

        public void setVoice_clarity_total(String voice_clarity_total) {
            this.voice_clarity_total = voice_clarity_total;
        }

        public String getPassage_difficulty_total() {
            return passage_difficulty_total;
        }

        public void setPassage_difficulty_total(String passage_difficulty_total) {
            this.passage_difficulty_total = passage_difficulty_total;
        }

        @Override
        public String toString() {
            return "Meta{" +
                    "tags='" + tags + '\'' +
                    ", modulation='" + modulation + '\'' +
                    ", voice_clarity='" + voice_clarity + '\'' +
                    ", passage_difficulty='" + passage_difficulty + '\'' +
                    ", times_played='" + times_played + '\'' +
                    ", practiced_status='" + practiced_status + '\'' +
                    '}';
        }
    }

}
