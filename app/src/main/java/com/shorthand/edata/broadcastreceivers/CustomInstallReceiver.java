package com.shorthand.edata.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.tagmanager.InstallReferrerReceiver;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

/**
 * Created by tushar on 23/08/17.
 */

public class CustomInstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        InstallReferrerReceiver googleReferrerTracking = new InstallReferrerReceiver();
        googleReferrerTracking.onReceive(context, intent);
        new CampaignTrackingReceiver().onReceive(context, intent);
        String referrer = intent.getStringExtra("referrer");
        new CacheBoundary().setMyReferrer(context,referrer);
    }
}
