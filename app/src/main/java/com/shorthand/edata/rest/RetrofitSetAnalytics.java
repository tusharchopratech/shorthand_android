package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.pojo.AnalyticsPojo;
import com.shorthand.edata.pojo.AnalyticsPojoRealm;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by tushar on 10/07/17.
 */

class RetrofitSetAnalytics {
    public String[] setAnalytics(Context context, String baseUrl, String token, String key, AnalyticsPojo[] analyticsPojoArray) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<ApiResponse> c = api.sendData(token, key, new ApiRequest(analyticsPojoArray));
        try {
            Response<ApiResponse> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return resp.body().getIds();
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    class ApiRequest {
        AnalyticsPojo[] analytics;
        public ApiRequest(AnalyticsPojo[] analytics) {
            this.analytics = analytics;
       }
}

    class ApiResponse {
        String[] ids;

        public String[] getIds() {
            return ids;
        }
    }


    interface ApiInterface {
        @POST("shorthand_analytics")
        Call<ApiResponse> sendData(@Header("authorization") String authorization, @Header("key") String key, @Body ApiRequest apiRequest);
    }
}
