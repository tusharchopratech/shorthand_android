package com.shorthand.edata.pojo;

/**
 * Created by tushar on 09/08/17.
 */

public class OfflineDictationType {
    String text, language,speed,duration;

    public OfflineDictationType(String text, String language, String speed, String duration) {
        this.text = text;
        this.language = language;
        this.speed = speed;
        this.duration = duration;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
