package com.shorthand.phomescreen.fragments.onlinedictationlist;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.ads.AdsProcessor;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.HomeScreenNavigationDrawerActivity;
import com.shorthand.phomescreen.service.MusicPlayerService;

import java.util.ArrayList;

import io.gresse.hugo.vumeterlibrary.VuMeterView;

/**
 * Created by tushar on 14/07/17.
 */

class RecyclerViewDictationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<DictationInfo> dictationInfoArrayList;
    RecyclerView recyclerView;
    int greenColor;
    String className = this.getClass().getSimpleName();

    public RecyclerViewDictationListAdapter(ArrayList<DictationInfo> dictationInfos, Context context, RecyclerView recyclerView) {
        this.context = context;
        this.dictationInfoArrayList = dictationInfos;
        this.recyclerView = recyclerView;
        greenColor = ResourcesCompat.getColor(context.getResources(), R.color.colorAccent, null);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_recycler_view_dictation_list, parent, false);
        RecyclerView.ViewHolder holder = new DictationListViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final DictationListViewHolder dictationListViewHolder = (DictationListViewHolder) holder;

        String timesPlayed = dictationInfoArrayList.get(position).getMeta().getTimes_played();
        String practicedStatus = dictationInfoArrayList.get(position).getMeta().getPracticed_status();
        String topic = dictationInfoArrayList.get(position).getTopic();
        String speakerName = dictationInfoArrayList.get(position).getSpeaker_name();

        if (timesPlayed != null) {
            dictationListViewHolder.textViewTimesPlayed.setText(General.getNumberString(Integer.parseInt(timesPlayed)) + " Plays");
        } else {

        }

        if (topic != null) {
            dictationListViewHolder.textViewTopic.setText(General.capsFirst(topic.toLowerCase()));
        } else {

        }
        if (speakerName != null) {
            dictationListViewHolder.textViewSpeakerName.setText(General.capsFirst(speakerName.toLowerCase()));
        } else {

        }
        if (practicedStatus != null) {
            if (practicedStatus.equals("done")) {
                dictationListViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_done);
            } else if (practicedStatus.equals("viewed")) {
                dictationListViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_viewed);
            } else {
                dictationListViewHolder.imageViewStatus.setImageResource(R.drawable.vd_circle_indicator_new);
            }
        } else {

        }


        if (MusicPlayerService.dictationInfo != null && MusicPlayerService.dictationInfo.getUuid().equals(dictationInfoArrayList.get(position).getUuid())) {
            if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.PLAYING)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "PLAY");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.PAUSED)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "PAUSED");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.COMPLETED)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.ERROR)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.STOPPED)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "STOP");
            } else if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.LOADING)) {
                setItemUiStateWhileRendering(dictationListViewHolder, "LOADING");
            } else {

            }

        } else {
            dictationListViewHolder.view.setBackgroundColor(Color.GRAY);
            dictationListViewHolder.textViewTopic.setTextColor(Color.BLACK);
            dictationListViewHolder.vuMeterView.setVisibility(View.GONE);
            dictationListViewHolder.progressBar.setVisibility(View.GONE);
            dictationListViewHolder.imageViewPlay.setVisibility(View.VISIBLE);
        }


        dictationListViewHolder.linearLayoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(General.isThisServiceRunning(context, MusicPlayerService.class)
                        && MusicPlayerService.dictationInfo != null
                        && MusicPlayerService.dictationInfo.getUuid().equals(dictationInfoArrayList.get(position).getUuid()))) {
                    if (HomeScreenNavigationDrawerActivity.audioPlayer != null) {

                        HomeScreenNavigationDrawerActivity.audioPlayer.startAudio(dictationInfoArrayList.get(position), Constants.AUDIO_SOURCE.ONLINE);

                        if (MusicPlayerService.dictationInfo != null) {
                            int oldDictationIndex = 0;
                            for (int i = 0; i < dictationInfoArrayList.size(); i++) {
                                if (dictationInfoArrayList.get(i).getUuid().equals(MusicPlayerService.dictationInfo.getUuid())) {
                                    oldDictationIndex = i;
                                    break;
                                } else {

                                }
                            }
                            DictationListViewHolder obj = (DictationListViewHolder) recyclerView.findViewHolderForAdapterPosition(oldDictationIndex);
                            if (obj != null) {
                                obj.view.setBackgroundColor(Color.GRAY);
                                obj.textViewTopic.setTextColor(Color.BLACK);
                                obj.vuMeterView.setVisibility(View.GONE);
                                obj.progressBar.setVisibility(View.GONE);
                                obj.imageViewPlay.setVisibility(View.VISIBLE);
                            } else {

                            }
                        } else {

                        }

                        dictationListViewHolder.view.setBackgroundColor(greenColor);
                        dictationListViewHolder.textViewTopic.setTextColor(greenColor);
                        dictationListViewHolder.vuMeterView.setVisibility(View.GONE);
                        dictationListViewHolder.progressBar.setVisibility(View.VISIBLE);
                        dictationListViewHolder.imageViewPlay.setVisibility(View.GONE);

                        new AdsProcessor(context).showInterstitial(className);
                        ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.DICTATIONS_LIST_CLICKED,
                                dictationInfoArrayList.get(position).getLanguage() + "/" + dictationInfoArrayList.get(position).getSpeed()
                                        + "/" + dictationInfoArrayList.get(position).getDuration() + "/"
                                        + dictationInfoArrayList.get(position).getUuid());
                    } else {

                    }
                } else {

                }
            }
        });

        if (new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            dictationListViewHolder.imageViewOffline.setVisibility(View.INVISIBLE);
        } else {
            dictationListViewHolder.imageViewOffline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    General.openPaymentDialog(context);
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return dictationInfoArrayList.size();
    }


    public void onMessageEvent(MessageEventData event) {
        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.MUSIC_PLAYER_SERVICE_STARTED)) {
            setItemUiState("START_LOADING");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED)) {
            setItemUiState("PLAY");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED_AFTER_PAUSED)) {
            setItemUiState("PLAY");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PAUSED)) {
            setItemUiState("PAUSED");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_CLOSED)) {
            stopAllItems();
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_COMPLETED)) {
            setItemUiState("STOP");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_ERROR)) {
            setItemUiState("STOP");
        } else {

        }
    }

    private void stopAllItems() {
        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
            DictationListViewHolder obj = (DictationListViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
            if (obj != null) {
                obj.view.setBackgroundColor(Color.GRAY);
                obj.textViewTopic.setTextColor(Color.BLACK);
                obj.vuMeterView.setVisibility(View.GONE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.VISIBLE);
            } else {

            }
        }
    }

    public void setItemUiState(String itemUiState) {
        int index = 0;
        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
            if (MusicPlayerService.dictationInfo != null && dictationInfoArrayList.get(i).getUuid().equals(MusicPlayerService.dictationInfo.getUuid())) {
                index = i;
                break;
            } else {

            }
        }
        DictationListViewHolder obj = (DictationListViewHolder) recyclerView.findViewHolderForAdapterPosition(index);
        if (obj != null) {
            if (itemUiState.equals("PLAY")) {
                obj.view.setBackgroundColor(greenColor);
                obj.textViewTopic.setTextColor(greenColor);
                obj.vuMeterView.setColor(greenColor);
                obj.vuMeterView.resume(true);
                obj.vuMeterView.setVisibility(View.VISIBLE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.GONE);
            } else if (itemUiState.equals("PAUSED")) {
                obj.view.setBackgroundColor(greenColor);
                obj.textViewTopic.setTextColor(greenColor);
                obj.vuMeterView.pause();
                obj.vuMeterView.setColor(Color.LTGRAY);
                obj.vuMeterView.setVisibility(View.VISIBLE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.GONE);
            } else if (itemUiState.equals("STOP")) {
                obj.view.setBackgroundColor(Color.GRAY);
                obj.textViewTopic.setTextColor(Color.BLACK);
                obj.vuMeterView.setVisibility(View.GONE);
                obj.progressBar.setVisibility(View.GONE);
                obj.imageViewPlay.setVisibility(View.VISIBLE);
            } else if (itemUiState.equals("START_LOADING")) {
                obj.view.setBackgroundColor(greenColor);
                obj.textViewTopic.setTextColor(greenColor);
                obj.vuMeterView.setVisibility(View.GONE);
                obj.progressBar.setVisibility(View.VISIBLE);
                obj.imageViewPlay.setVisibility(View.GONE);
            } else {

            }

        } else {

        }

    }

    public void setItemUiStateWhileRendering(DictationListViewHolder obj, String itemUiState) {
        if (itemUiState.equals("PLAY")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.setColor(greenColor);
            obj.vuMeterView.setVisibility(View.VISIBLE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else if (itemUiState.equals("PAUSED")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.pause();
            obj.vuMeterView.setColor(Color.LTGRAY);
            obj.vuMeterView.setVisibility(View.VISIBLE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else if (itemUiState.equals("STOP")) {
            obj.view.setBackgroundColor(Color.GRAY);
            obj.textViewTopic.setTextColor(Color.BLACK);
            obj.vuMeterView.setVisibility(View.GONE);
            obj.progressBar.setVisibility(View.GONE);
            obj.imageViewPlay.setVisibility(View.VISIBLE);
        } else if (itemUiState.equals("LOADING")) {
            obj.view.setBackgroundColor(greenColor);
            obj.textViewTopic.setTextColor(greenColor);
            obj.vuMeterView.setVisibility(View.GONE);
            obj.progressBar.setVisibility(View.VISIBLE);
            obj.imageViewPlay.setVisibility(View.GONE);
        } else {

        }
    }


    public class DictationListViewHolder extends RecyclerView.ViewHolder {

        TextView textViewSpeakerName, textViewTopic, textViewTimesPlayed;
        View view;
        ProgressBar progressBar;
        VuMeterView vuMeterView;
        ImageView imageViewPlay, imageViewStatus, imageViewOffline;
        LinearLayout linearLayoutParent;


        public DictationListViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.viewLeftSideId);
            textViewSpeakerName = (TextView) itemView.findViewById(R.id.textViewSpeakerNameId);
            textViewTopic = (TextView) itemView.findViewById(R.id.textViewTopicNameId);
            textViewTimesPlayed = (TextView) itemView.findViewById(R.id.textViewTimesPlayedId);
            imageViewPlay = (ImageView) itemView.findViewById(R.id.imageViewPlayInflateDictationListId);
            imageViewStatus = (ImageView) itemView.findViewById(R.id.imageViewStatusId);
            imageViewOffline = (ImageView) itemView.findViewById(R.id.imageViewOfflineDictationListId);
            linearLayoutParent = (LinearLayout) itemView.findViewById(R.id.linearLayoutInflateDictationListId);
            vuMeterView = (VuMeterView) itemView.findViewById(R.id.vuMeterViewInflateDictationListId);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressbarInflateDictationListId);
        }
    }

}
