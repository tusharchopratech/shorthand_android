package com.shorthand.edata.rest;

import android.content.Context;
import android.os.Bundle;

import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.AddUserData;
import com.shorthand.edata.pojo.AnalyticsPojo;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.pojo.OfflineFileData;
import com.shorthand.edata.pojo.UserDetails;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

/**
 * Created by tushar on 13/07/17.
 */

class RestInteractor {

    public RestInteractor() {

    }

    public Bundle addUserAndGetUserIdInteractor(Context context, String baseUrl, AddUserData addUserData) {
        return new RetrofitAddUser().addUserAndGetUserId(context, baseUrl, addUserData);
    }

    public Bundle getMyLocation(Context context) {
        return new RetrofitGetLocation().getMyLocation(context);
    }

    public DictationInfo[] getDictationListInteractor(Context context, String baseUrl, String token, String key, String language, int duration, int speed) {

        DictationInfo[] dictationInfos = new RetrofitGetDictationList().getDictationList(context, baseUrl, token, key, language, duration, speed);
        if (dictationInfos == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                dictationInfos = new RetrofitGetDictationList().getDictationList(context, baseUrl, newToken, key, language, duration, speed);
            }
        }
        return dictationInfos;
    }


    public UserDetails getUserDetailsInteractor(Context context, String baseUrl, String token, String key) {
        UserDetails userDetails = new RetrofitGetUserDetails().getUserDetails(context, baseUrl, token, key);
        if (userDetails == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                userDetails = new RetrofitGetUserDetails().getUserDetails(context, baseUrl, newToken, key);
            }
        }
        return userDetails;
    }

    public String setBookmarkInteractor(Context context, String baseUrl, String token, String key, String dictationId, boolean isBookmarked) {
        String status = new RetrofitSetBookmark().setBookmark(context, baseUrl, token, key, dictationId, isBookmarked);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetBookmark().setBookmark(context, baseUrl, newToken, key, dictationId, isBookmarked);
            }
        }
        return status;
    }

    public String setFeedbackInteractor(Context context, String baseUrl, String token, String key, String feedBack, String type) {
        String status = new RetrofitSetFeedBack().setFeedback(context, baseUrl, token, key, feedBack, type);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetFeedBack().setFeedback(context, baseUrl, newToken, key, feedBack, type);
            }
        }
        return status;
    }

    public String setPracticedInteractor(Context context, String baseUrl, String token, String key, String dictationId) {
        String status = new RetrofitSetPracticed().setPracticed(context, baseUrl, token, key, dictationId);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetPracticed().setPracticed(context, baseUrl, newToken, key, dictationId);
            }
        }
        return status;
    }

    public String setUserDetailsInteractor(Context context, String baseUrl, String token, String key, String dob, String gender, String phoneNo, String name) {
        String status = new RetrofitSetUserDetails().setUserDetails(context, baseUrl, token, key, dob, gender, phoneNo, name);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetUserDetails().setUserDetails(context, baseUrl, newToken, key, dob, gender, phoneNo, name);
            }
        }
        return status;
    }

    public String setUserDictationRatingInteractor(Context context, String baseUrl, String token, String key, String dictationId, int passageDifficulty, int modulation, int voiceClarity, String message) {
        String status = new RetrofitSetUserDictationRating().setUserDictationRating(context, baseUrl, token, key, dictationId, passageDifficulty, modulation, voiceClarity, message);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetUserDictationRating().setUserDictationRating(context, baseUrl, newToken, key, dictationId, passageDifficulty, modulation, voiceClarity, message);
            }
        }
        return status;
    }

    public InitialAppInfo getInitialAppInfoInteractor(Context context, String baseUrl, String token, String key, String appVersion, String platform) {
        InitialAppInfo initialAppInfo = new RetrofitGetInitalAppInfo().getInitialAppInfo(context, baseUrl, token, key, appVersion, platform);
        if (initialAppInfo == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                initialAppInfo = new RetrofitGetInitalAppInfo().getInitialAppInfo(context, baseUrl, newToken, key, appVersion, platform);
            }
        }
        return initialAppInfo;
    }

    public String getNewAccessTokenInteractor(Context context, String baseUrl, String refreshToken, String key) {
        return new RetrofitGetNewAccessToken().getNewAccessToken(context, baseUrl, refreshToken, key);
    }


    public String setViewedInteractor(Context context, String baseUrl, String token, String key, String dictationId) {
        String status = new RetrofitSetViewed().setViewed(context, baseUrl, token, key, dictationId);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetViewed().setViewed(context, baseUrl, newToken, key, dictationId);
            }
        }
        return status;
    }

    public String setProgressReset(Context context, String baseUrl, String token, String key) {
        String status = new RetrofitSetProgressReset().setProgressReset(context, baseUrl, token, key);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetProgressReset().setProgressReset(context, baseUrl, newToken, key);
            }
        }
        return status;
    }

    public String[] setShorthandAnalytics(Context context, String baseUrl, String token, String key, AnalyticsPojo[] analyticsPojoArray) {
        String ids[] = new RetrofitSetAnalytics().setAnalytics(context, baseUrl, token, key, analyticsPojoArray);
        if (ids == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                ids = new RetrofitSetAnalytics().setAnalytics(context, baseUrl, newToken, key, analyticsPojoArray);
            }
        }
        return ids;
    }

    public OfflineFileData getOfflineFileData(Context context, String baseUrl, String token, String key) {
        OfflineFileData offlineFileData = new RetrofitGetOfflineFileData().getOfflineFileData(context, baseUrl, token, key);
        if (offlineFileData == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                offlineFileData = new RetrofitGetOfflineFileData().getOfflineFileData(context, baseUrl, newToken, key);
            }
        }
        return offlineFileData;
    }

    public String setUserPaymentInfo(Context context, String baseUrl, String token, String key, String jsonResponseString) {
        String status = new RetrofitSetUserPaymentInfo().setUserPaymentInfo(context, baseUrl, token, key, jsonResponseString);
        if (status == null && new CacheBoundary().getIsTokenExpired(context)) {
            String newToken = new RetrofitGetNewAccessToken().getNewAccessToken(context, Constants.BASE_URL,
                    new CacheBoundary().getMyRefreshToken(context), General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)));
            if (newToken != null) {
                new CacheBoundary().setMyToken(context, newToken);
                status = new RetrofitSetUserPaymentInfo().setUserPaymentInfo(context, baseUrl, newToken, key,jsonResponseString);
            }
        }
        return status;
    }
}
