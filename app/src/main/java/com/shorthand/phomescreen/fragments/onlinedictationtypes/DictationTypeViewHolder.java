package com.shorthand.phomescreen.fragments.onlinedictationtypes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shorthand.R;

/**
 * Created by tushar on 15/07/17.
 */

public class DictationTypeViewHolder extends RecyclerView.ViewHolder{

    ImageView imageViewDictationType, imageViewOffline;
    TextView textViewSpeed,textViewPracticed, textViewLanguage,textViewTime;
    LinearLayout linearLayoutParent;

    public DictationTypeViewHolder(View itemView) {
        super(itemView);
        imageViewDictationType = (ImageView) itemView.findViewById(R.id.imageViewDictationTypeId);
        imageViewOffline = (ImageView) itemView.findViewById(R.id.imageViewOfflineId);
        textViewSpeed = (TextView) itemView.findViewById(R.id.textViewSpeedId);
        textViewPracticed = (TextView) itemView.findViewById(R.id.textviewPracticedId);
        textViewLanguage = (TextView) itemView.findViewById(R.id.textviewLanguageId);
        textViewTime = (TextView) itemView.findViewById(R.id.textviewTimeId);
        linearLayoutParent = (LinearLayout) itemView.findViewById(R.id.linearLayoutInflateDictationTypeID);

    }
}
