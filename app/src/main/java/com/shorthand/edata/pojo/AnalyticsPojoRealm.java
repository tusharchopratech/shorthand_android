package com.shorthand.edata.pojo;

import com.shorthand.edata.analytics.ShorthandAnalytics;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by tushar on 12/09/17.
 */

public class AnalyticsPojoRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String category, action, label;
    private long epochTime;

    public AnalyticsPojoRealm(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(long epochTime) {
        this.epochTime = epochTime;
    }

}
