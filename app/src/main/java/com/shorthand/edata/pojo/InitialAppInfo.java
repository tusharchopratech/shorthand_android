package com.shorthand.edata.pojo;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by tushar on 13/07/17.
 */

public class InitialAppInfo implements Serializable {
    String update_app;
    int ads_banner_percent_homescreen_activity, ads_interstitial_dictation_list_percent,
            ads_interstitial_dictation_type_percent, ads_rewarded_video_percent, ads_banner_percent_sliding_up_panel;
    DictationType[] dictation_types;
    boolean receive_analytics;
    String subscription_id;
    String user_type;

    public int getAds_banner_percent_sliding_up_panel() {
        return ads_banner_percent_sliding_up_panel;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public boolean getReceive_analytics() {
        return receive_analytics;
    }

    public void setReceive_analytics(boolean receive_analytics) {
        this.receive_analytics = receive_analytics;
    }

    public String getUpdate_app() {
        return update_app;
    }

    public void setUpdate_app(String update_app) {
        this.update_app = update_app;
    }

    public DictationType[] getDictation_types() {
        return dictation_types;
    }

    public void setDictation_types(DictationType[] dictation_types) {
        this.dictation_types = dictation_types;
    }

    public int getAds_banner_percent_homescreen_activity() {
        return ads_banner_percent_homescreen_activity;
    }

    public void setAds_banner_percent_homescreen_activity(int ads_banner_percent_homescreen_activity) {
        this.ads_banner_percent_homescreen_activity = ads_banner_percent_homescreen_activity;
    }

    public int getAds_interstitial_dictation_list_percent() {
        return ads_interstitial_dictation_list_percent;
    }

    public void setAds_interstitial_dictation_list_percent(int ads_interstitial_dictation_list_percent) {
        this.ads_interstitial_dictation_list_percent = ads_interstitial_dictation_list_percent;
    }

    public int getAds_interstitial_dictation_type_percent() {
        return ads_interstitial_dictation_type_percent;
    }

    public void setAds_interstitial_dictation_type_percent(int ads_interstitial_dictation_type_percent) {
        this.ads_interstitial_dictation_type_percent = ads_interstitial_dictation_type_percent;
    }

    public int getAds_rewarded_video_percent() {
        return ads_rewarded_video_percent;
    }

    public void setAds_rewarded_video_percent(int ads_rewarded_video_percent) {
        this.ads_rewarded_video_percent = ads_rewarded_video_percent;
    }

    @Override
    public String toString() {
        return "InitialAppInfo{" +
                "update_app='" + update_app + '\'' +
                ", dictation_types=" + Arrays.toString(dictation_types) +
                '}';
    }
}
