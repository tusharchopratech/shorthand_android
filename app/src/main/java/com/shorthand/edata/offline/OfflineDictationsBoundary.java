package com.shorthand.edata.offline;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.api.client.util.IOUtils;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.pojo.DictationInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by tushar on 05/08/17.
 */

public class OfflineDictationsBoundary {


    public boolean isOfflineFileExist() {
        return new File(Constants.OFFLINE_FILE_PATH).exists();
    }


    public ArrayList<DictationInfo> getDictationList(String password, String ivString) {
        ArrayList<DictationInfo> arrayList = new ArrayList<>();
        try {

            File file = new File(Constants.OFFLINE_FILE_PATH);
            final ZipFile zipFile = new ZipFile(file);
            for (Enumeration e = zipFile.entries(); e.hasMoreElements(); ) {
                final ZipEntry entry = (ZipEntry) e.nextElement();
                if (entry.getName().equals("temp/info.data")) {

                    InputStream inputStream = zipFile.getInputStream(entry);
                    File tempFile = File.createTempFile("temp", "temp");
                    tempFile.deleteOnExit();
                    FileOutputStream out = new FileOutputStream(tempFile);
                    IOUtils.copy(inputStream, out);
                    File jsonDataFile = CryptoUtils.getDecryptedDataFile(password, ivString, tempFile);

                    StringBuilder text = new StringBuilder();
                    BufferedReader br = new BufferedReader(new FileReader(jsonDataFile));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    br.close();

                    JSONObject mainObject = new JSONObject(text.toString());
                    JSONArray jsonArray = mainObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                        String images[] = new String[jsonObject.getJSONArray("images").length()];
                        for (int j = 0; j < jsonObject.getJSONArray("images").length(); j++) {
                            images[j] = (String) jsonObject.getJSONArray("images").get(j);
                        }

                        arrayList.add(new DictationInfo(
                                jsonObject.getString("speaker_name"),
                                jsonObject.getString("gender"),
                                jsonObject.getString("topic"),
                                jsonObject.getString("time"),
                                jsonObject.getString("speed"),
                                jsonObject.getJSONArray("images").length(),
                                jsonObject.getString("language"),
                                jsonObject.getString("record_name"),
                                null
                        ));
                    }

                    return arrayList;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public FileInputStream getAudioFile(String recordName, String password, String ivString) {
        try {


            File file = new File(Constants.OFFLINE_FILE_PATH);
            final ZipFile zipFile = new ZipFile(file);
            for (Enumeration e = zipFile.entries(); e.hasMoreElements(); ) {
                final ZipEntry entry = (ZipEntry) e.nextElement();
                if (entry.getName().equals("temp/" + recordName.substring(recordName.indexOf("_") + 1) + ".dat")) {

                    InputStream inputStream = zipFile.getInputStream(entry);
                    File tempFile = File.createTempFile("temp", "temp");
                    tempFile.deleteOnExit();
                    FileOutputStream out = new FileOutputStream(tempFile);
                    IOUtils.copy(inputStream, out);
                    File recordFolderZip = CryptoUtils.getDecryptedDataFile(password, ivString, tempFile);

                    ZipFile recordZipFIle = new ZipFile(recordFolderZip);
                    for (Enumeration e1 = recordZipFIle.entries(); e1.hasMoreElements(); ) {
                        final ZipEntry entry1 = (ZipEntry) e1.nextElement();
                        if (entry1.getName().contains("mp3")) {

                            InputStream inputStream2 = recordZipFIle.getInputStream(entry1);
                            File tempFile2 = File.createTempFile("temp2", "temp");
                            tempFile2.deleteOnExit();
                            FileOutputStream out2 = new FileOutputStream(tempFile2);
                            IOUtils.copy(inputStream2, out2);
                            return new FileInputStream(tempFile2);
                        }
                    }

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<Bitmap> getImages(String recordName, String password, String ivString) {
        ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
        try {


            File file = new File(Constants.OFFLINE_FILE_PATH);
            final ZipFile zipFile = new ZipFile(file);
            for (Enumeration e = zipFile.entries(); e.hasMoreElements(); ) {
                final ZipEntry entry = (ZipEntry) e.nextElement();
                if (entry.getName().equals("temp/" + recordName.substring(recordName.indexOf("_") + 1) + ".dat")) {

                    InputStream inputStream = zipFile.getInputStream(entry);
                    File tempFile = File.createTempFile("temp", "temp");
                    tempFile.deleteOnExit();
                    FileOutputStream out = new FileOutputStream(tempFile);
                    IOUtils.copy(inputStream, out);
                    File recordFolderZip = CryptoUtils.getDecryptedDataFile(password, ivString, tempFile);

                    ZipFile recordZipFIle = new ZipFile(recordFolderZip);
                    for (Enumeration e1 = recordZipFIle.entries(); e1.hasMoreElements(); ) {
                        final ZipEntry entry1 = (ZipEntry) e1.nextElement();
                        if (entry1.getName().endsWith("jpg")) {
                            InputStream inputStream2 = recordZipFIle.getInputStream(entry1);
                            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);
                            Bitmap bitmap = BitmapFactory.decodeStream(bufferedInputStream);
                            bitmapArrayList.add(bitmap);
                        }
                    }

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bitmapArrayList;
    }

}
