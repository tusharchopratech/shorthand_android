package com.shorthand.pofflinedictations.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;

public class OfflineDictationsNetworkProblemFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public OfflineDictationsNetworkProblemFragment() {
        // Required empty public constructor
    }

    Context context;
    Button buttonRetry;

    // TODO: Rename and change types and number of parameters
    public static OfflineDictationsNetworkProblemFragment newInstance(String param1, String param2) {
        OfflineDictationsNetworkProblemFragment fragment = new OfflineDictationsNetworkProblemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private void initalizeVariables(View view) {
        context = view.getContext();
        buttonRetry = (Button) view.findViewById(R.id.buttonOfflineNetworkProblemId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offline_dictations_network_problem, container, false);
        initalizeVariables(view);

        buttonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (General.isNetworkAvailable(context)) {
                    General.changeFragment(getFragmentManager(), new OfflineDictationDownloadFragment(),
                            R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_LEFT);
                    OfflineDictationsActivity.currentFragmentName = "OfflineDictationDownloadFragment";
                }
            }
        });

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
