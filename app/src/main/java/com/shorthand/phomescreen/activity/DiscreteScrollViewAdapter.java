package com.shorthand.phomescreen.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.shorthand.pshowimage.ShowImageActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by tushar on 25/07/17.
 */

public class DiscreteScrollViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    int imageCount;
    String uuid, topic;


    public DiscreteScrollViewAdapter(Context context, String uuid, int imageCount, String topic) {
        this.context = context;
        this.imageCount = imageCount;
        this.uuid = uuid;
        this.topic = topic;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.inflate_discrete_scroll_view_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder h = (ViewHolder) holder;

        if (uuid != null && !uuid.equals("")) {

            if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.ONLINE)) {

                final String imageUrl = General.getImageUrl(new CacheBoundary().getMyToken(context),
                        General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context))
                        , uuid, position + 1);
                if (imageUrl != null && !imageUrl.equals("")) {

                    Picasso.with(context)
                            .load(imageUrl)
                            .resize(General.getScreenWidth(context), General.getScreenWidth(context))
                            .centerInside()
                            .placeholder(R.drawable.place_hoslder_2)
                            .error(R.drawable.place_hoslder_2)
                            .into(h.image, new Callback() {
                                @Override
                                public void onSuccess() {

                                    h.image.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "transcription_image_" + String.valueOf(position) + "/" + uuid);
                                            Intent intent = new Intent(context, ShowImageActivity.class);
                                            intent.putExtra("imageUrl", imageUrl);
                                            intent.putExtra("imageSource", Constants.AUDIO_SOURCE.ONLINE);
                                            intent.putExtra("topic", topic);
                                            context.startActivity(intent);
                                        }
                                    });
                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            } else if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.OFFLINE)) {
                new BackgroundProcess(h.image, position).execute();
            }
        }

    }

    @Override
    public int getItemCount() {
        return imageCount;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.imageViewInflateDiscreteScrollViewId);
        }
    }


    class BackgroundProcess extends AsyncTask<Void, Void, Void> {
        ImageView imageView;
        int position;
        ArrayList<Bitmap> bitmapArrayList;

        public BackgroundProcess(ImageView imageView, int position) {
            this.imageView = imageView;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            bitmapArrayList = new OfflineDictationsBoundary().getImages(uuid
                    , new CacheBoundary().getOfflineFilePassword(context)
                    , new CacheBoundary().getOfflineFileIv(context));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (bitmapArrayList != null && bitmapArrayList.size() > 0) {
                imageView.setImageBitmap(getResizedBitmap(bitmapArrayList.get(position),
                        General.dpToPx(270, context), General.dpToPx(270, context)));
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ShowImageActivity.class);
                        intent.putExtra("imageSource", Constants.AUDIO_SOURCE.OFFLINE);
                        intent.putExtra("topic", topic);
                        intent.putExtra("position", position);
                        intent.putExtra("recordNumber", uuid);
                        context.startActivity(intent);
                    }
                });
            }
        }
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

}
