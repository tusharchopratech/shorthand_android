package com.shorthand.edata.rest;

import android.content.Context;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by tushar on 10/07/17.
 */

public class RetrofitSetUserDictationRating {
    public String setUserDictationRating(Context context,String baseUrl, String token, String key, String dictationId, int passageDifficulty, int modulation, int voiceClarity, String message) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<Void> c = api.sendData(token, key, new ApiRequest(dictationId, passageDifficulty, modulation, voiceClarity, message));
        try {
            Response<Void> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return "SUCCESS";
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    class ApiRequest {
        String dictation_id, message;
        int passage_difficulty, modulation, voice_clarity;

        public ApiRequest(String dictation_id, int passage_difficulty, int modulation, int voice_clarity, String message) {
            this.dictation_id = dictation_id;
            this.passage_difficulty = passage_difficulty;
            this.modulation = modulation;
            this.voice_clarity = voice_clarity;
            this.message = message;
        }
    }


    public interface ApiInterface {
        @POST("v1/set_user_dictation_rating")
        Call<Void> sendData(@Header("authorization") String authorization, @Header("key") String key, @Body ApiRequest apiRequest);
    }
}
