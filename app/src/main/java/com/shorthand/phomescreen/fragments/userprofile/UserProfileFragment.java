package com.shorthand.phomescreen.fragments.userprofile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.datetimepicker.date.DatePickerDialog;
import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.HomeScreenNavigationDrawerActivity;
import com.shorthand.phomescreen.fragments.onlinedictationtypes.DictationTypeFragment;
import com.squareup.picasso.Picasso;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;


public class UserProfileFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    String className = this.getClass().getSimpleName();
    Calendar calendar;

    CircleImageView circleImageView;
    MaterialBetterSpinner materialSpinnerGender;
    EditText editTextDob, editTextPhoneNo, editTextName, editTextEmail;
    Button buttonSave, buttonBack;
    View view;
    Context context;
    LinearLayout linearLayoutGoBackParent, linearLayoutUserProfileParent;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment newInstance(String param1, String param2) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private void initializeVariables(View view) {
        this.view = view;
        this.context = view.getContext();
        calendar = Calendar.getInstance();

        linearLayoutGoBackParent = (LinearLayout) view.findViewById(R.id.linearLayoutGoBackParentId);
        linearLayoutUserProfileParent = (LinearLayout) view.findViewById(R.id.linearLayoutUserProfileParentId);
        buttonBack = (Button) view.findViewById(R.id.buttonUserDetailsGoBackId);
        linearLayoutGoBackParent = (LinearLayout) view.findViewById(R.id.linearLayoutGoBackParentId);
        editTextDob = (EditText) view.findViewById(R.id.editTextDobId);
        editTextName = (EditText) view.findViewById(R.id.editTextNameId);
        editTextEmail = (EditText) view.findViewById(R.id.editTextEmailId);
        editTextPhoneNo = (EditText) view.findViewById(R.id.editTextPhoneNumberId);
        buttonSave = (Button) view.findViewById(R.id.buttonUserDetailsSaveId);
        materialSpinnerGender = (MaterialBetterSpinner) view.findViewById(R.id.materialBetterSpinnerGenderId);
        circleImageView = (CircleImageView) view.findViewById(R.id.circleImageViewUserDetailsId);
        ShorthandAnalytics.trackScreen(context, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_FRAGMENT, className);
        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) ((Activity) view.getContext()).findViewById(R.id.toolbarHomeScreenId);
        LinearLayout linearLayout = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarHomeScreenId);
        linearLayout.removeAllViews();
        linearLayout.setGravity(Gravity.CENTER);
        TextView textView = new TextView(context);
        textView.setText("Profile");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        textView.setPadding(0, 0, General.dpToPx(72, context), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(45);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.addView(textView, layoutParams);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        initializeVariables(view);

        editTextName.setText(General.capsFirst(new CacheBoundary().getMyName(context)));
        editTextEmail.setText(new CacheBoundary().getMyEmail(context));
        editTextPhoneNo.setText(new CacheBoundary().getMyPhone(context));
        editTextDob.setText(new CacheBoundary().getMyDob(context));

        editTextName.setCompoundDrawablesWithIntrinsicBounds(VectorDrawableCompat.create(context.getResources()
                , R.drawable.vd_profile, null), null, null, null);
        editTextName.setCompoundDrawablePadding(General.dpToPx(10,context));

        editTextEmail.setCompoundDrawablesWithIntrinsicBounds(VectorDrawableCompat.create(context.getResources()
                , R.drawable.vd_email, null), null, null, null);
        editTextEmail.setCompoundDrawablePadding(General.dpToPx(10,context));

        editTextPhoneNo.setCompoundDrawablesWithIntrinsicBounds(VectorDrawableCompat.create(context.getResources()
                , R.drawable.vd_phone, null), null, null, null);
        editTextPhoneNo.setCompoundDrawablePadding(General.dpToPx(10,context));

        editTextDob.setCompoundDrawablesWithIntrinsicBounds(VectorDrawableCompat.create(context.getResources()
                , R.drawable.vd_calender, null), null, null, null);
        editTextDob.setCompoundDrawablePadding(General.dpToPx(10,context));

        materialSpinnerGender.setCompoundDrawablesWithIntrinsicBounds(VectorDrawableCompat.create(context.getResources()
                , R.drawable.vd_gender, null), null, null, null);
        materialSpinnerGender.setCompoundDrawablePadding(General.dpToPx(10,context));

        String gender = new CacheBoundary().getMyGender(context);

        String picUrl = new CacheBoundary().getMyPicUrl(context);
        if (picUrl != null && !picUrl.equals("")) {
            Picasso.with(context)
                    .load(picUrl)
                    .resize(240, 280)
                    .centerInside()
                    .placeholder(R.drawable.ic_profile_placeholder)
                    .error(R.drawable.ic_profile_placeholder)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(circleImageView);
        }


        final String[] ITEMS = {"Male", "Female", "Other"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.inflate_spinner_textview_user_details, ITEMS);
        materialSpinnerGender.setAdapter(arrayAdapter);

        editTextDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.newInstance(UserProfileFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show(((Activity) context).getFragmentManager(), "datePicker");
            }
        });
        editTextDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DatePickerDialog.newInstance(UserProfileFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)).show(((Activity) context).getFragmentManager(), "datePicker");

                }
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gender = materialSpinnerGender.getText().toString().toLowerCase();
                String dob = editTextDob.getText().toString();
                String phoneNo = editTextPhoneNo.getText().toString();
                String name = editTextName.getText().toString().toLowerCase();
                if (gender.equals("") || dob.equals("") || phoneNo.equals("") || name.equals("")) {
                    Toast.makeText(context, "Please fill all fields.", Toast.LENGTH_LONG).show();
                } else {
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.USER_PROFILE_BUTTON_SAVE_CLICKED,"");
                    new BackgroundProcess(dob, gender, phoneNo, name).execute();
                }
            }
        });

        if (gender.toLowerCase().equals("male")) {
            materialSpinnerGender.setText("Male");
        } else if (gender.toLowerCase().equals("female")) {
            materialSpinnerGender.setText("Female");
        } else if (gender.toLowerCase().equals("other")) {
            materialSpinnerGender.setText("Other");
        }

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HomeScreenNavigationDrawerActivity.currentFragmentName = "DictationTypeFragment";
                General.changeFragment(getFragmentManager(), new DictationTypeFragment(),
                        R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_LEFT);
                ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.USER_PROFILE_BUTTON_GO_BACK_CLICKED,"");
            }
        });


        return view;

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {

        String dob = String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
        editTextDob.setText(dob);
    }

    class BackgroundProcess extends AsyncTask<Void, Void, Void> {
        String status = "";
        String dob, gender, phoneNo, name;

        public BackgroundProcess(String dob, String gender, String phoneNo, String name) {
            this.phoneNo = phoneNo;
            this.gender = gender;
            this.dob = dob;
            this.name = name;
            status = "FAILED";
        }

        @Override
        protected Void doInBackground(Void... params) {
            status = new RestBoundary().setUserDetails(context, Constants.BASE_URL,
                    new CacheBoundary().getMyToken(context),
                    General.getShaKey(new CacheBoundary().getMyUuid(context),
                            new CacheBoundary().getMyEmail(context)), dob, gender, phoneNo, name);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (status != null && status.equals("SUCCESS")) {
                linearLayoutGoBackParent.setVisibility(View.VISIBLE);
                linearLayoutUserProfileParent.setVisibility(View.GONE);
                CacheBoundary cacheBoundary = new CacheBoundary();
                cacheBoundary.setMyName(context, name);
                cacheBoundary.setMyDob(context, dob);
                cacheBoundary.setMyGender(context, gender);
                cacheBoundary.setMyPhone(context, phoneNo);
            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Oops !!!\nSomething went wrong with network.");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
    }


}
