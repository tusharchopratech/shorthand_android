package com.shorthand.phomescreen.fragments.feedback;

/**
 * Created by tushar on 25/07/17.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.fragments.onlinedictationtypes.DictationTypeFragment;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


public class FeedbackFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String className = this.getClass().getSimpleName();
    Context context;
    EditText editTextFeedback;
    Button buttonSend, buttonGoBack;
    LinearLayout linearLayoutForm;
    LinearLayout linearLayoutFormSubmitted;
    View view;
    MaterialBetterSpinner materialBetterSpinnerSelectFeedbackType;

    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    private void initializeVariables(View view) {
        this.context = view.getContext();
        this.view = view;
        editTextFeedback = (EditText) view.findViewById(R.id.editTextFeedbackId);
        linearLayoutForm = (LinearLayout) view.findViewById(R.id.linearLayoutFormId);
        linearLayoutFormSubmitted = (LinearLayout) view.findViewById(R.id.linearLayoutFormSubmittedId);
        buttonSend = (Button) view.findViewById(R.id.buttonSendId);
        buttonGoBack = (Button) view.findViewById(R.id.buttonGoBackId);
        materialBetterSpinnerSelectFeedbackType = (MaterialBetterSpinner) view.findViewById(R.id.materialBetterSpinnerSelectFeedbackTypeId);
        ShorthandAnalytics.trackScreen(context, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_FRAGMENT, className);
        setToolbar();

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) ((Activity) view.getContext()).findViewById(R.id.toolbarHomeScreenId);
        LinearLayout linearLayout = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarHomeScreenId);
        linearLayout.removeAllViews();
        linearLayout.setGravity(Gravity.CENTER);
        TextView textView = new TextView(context);
        textView.setText("Feedback");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        textView.setPadding(0, 0, General.dpToPx(72, context), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(45);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.addView(textView, layoutParams);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        initializeVariables(view);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context
                , R.layout.inflate_spinner_textview_user_details, new String[]{"General", "Suggestion", "Complaint"});
        materialBetterSpinnerSelectFeedbackType.setAdapter(arrayAdapter);


        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editTextFeedback.getText().toString().trim().equals("") && !materialBetterSpinnerSelectFeedbackType.getText().toString().equals("")) {
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.FEEDBACK_BUTTON_SEND_CLICKED, "");
                    new BackgroundSendFeedback(editTextFeedback.getText().toString(),
                            materialBetterSpinnerSelectFeedbackType.getText().toString()).execute();
                } else {
                    Toast.makeText(context, "Please select feedback type and write feedback.", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                General.changeFragment(getFragmentManager(), new DictationTypeFragment(), R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_LEFT);
                OfflineDictationsActivity.currentFragmentName = "DictationTypeFragment";
                 ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.FEEDBACK_BUTTON_GO_BACK_CLICKED,"");
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    class BackgroundSendFeedback extends AsyncTask<Integer, Integer, Integer> {
        String feedback, type;
        String status = null;

        BackgroundSendFeedback(String feedback, String type) {
            this.feedback = feedback;
            this.type = type;
        }

        @Override
        protected Integer doInBackground(Integer... integers) {
            status = new RestBoundary().setFeedback(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                    General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context))
                    , feedback, type);

            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (status != null && status.equals("SUCCESS")) {
                linearLayoutForm.setVisibility(View.GONE);
                linearLayoutFormSubmitted.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(context, "Error. Please try again.", Toast.LENGTH_LONG).show();
            }

        }
    }

}

