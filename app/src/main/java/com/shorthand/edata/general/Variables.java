package com.shorthand.edata.general;

import com.shorthand.edata.pojo.InitialAppInfo;

/**
 * Created by tushar on 29/08/17.
 */

public class Variables {
    public static InitialAppInfo INITIAL_APP_INFO = null;
    public static boolean IS_ONLINE_MODE = false;
    public static boolean SEND_ANALYTICS = false;
    public static int ADS_PERCENTAGE_BANNER_HOMESCREEN_ACTIVITY = 0;
    public static int ADS_PERCENTAGE_BANNER_SLIDING_UP_PANEL = 0;
    public static int ADS_PERCENTAGE_REWARDED_VIDEO = 0;
    public static int ADS_PERCENTAGE_INTERSTITIAL_DICTATION_TYPE = 0;
    public static int ADS_PERCENTAGE_INTERSTITIAL_DICTATION_LIST = 0;
}
