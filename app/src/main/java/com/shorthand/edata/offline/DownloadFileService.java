package com.shorthand.edata.offline;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Logger;
import com.coolerfall.download.Priority;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.pojo.OfflineFileData;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class DownloadFileService extends Service {

    public DownloadFileService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        General.showToast(this, "Service Started");
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
        } else {
            new BacokgroundWork().execute();
        }
        return START_STICKY;
    }

    private void startDownloadFile(String url) {

        new File(Constants.INTERNAL_STORAGE_PATH).mkdirs();

        DownloadManager manager = new DownloadManager.Builder().context(this)
                //.downloader(OkHttpDownloader.create())
                .threadPoolSize(2)
                .logger(Logger.EMPTY)
                .build();
        DownloadRequest request = new DownloadRequest.Builder()
                .url(url)
                .retryTime(5)
                .retryInterval(4, TimeUnit.SECONDS)
                .progressInterval(1, TimeUnit.SECONDS)
                .priority(Priority.HIGH)
                .destinationFilePath(Constants.OFFLINE_FILE_PATH)
                .downloadCallback(new DownloadCallback() {
                    @Override
                    public void onStart(int downloadId, long totalBytes) {
                        General.showLog("##DOWNLOAD", "Started : " + String.valueOf((totalBytes / 1024) / 1024));
                        Bundle bundle = new Bundle();
                        bundle.putString("total_size", String.valueOf((totalBytes / 1024) / 1024));
                        EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                                DOWNLOAD_OFFLINE_FILE.DOWNLOAD_START, bundle));
                    }

                    @Override
                    public void onRetry(int downloadId) {
                        General.showLog("##DOWNLOAD", "Retry");
                        EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                                DOWNLOAD_OFFLINE_FILE.DOWNLOAD_RETRY, null));
                    }

                    @Override
                    public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                        int progress = (int) ((bytesWritten * 100) / totalBytes);
                        General.showLog("##DOWNLOAD", "Progress " + String.valueOf(progress));
                        Bundle bundle = new Bundle();
                        bundle.putInt("progress", progress);
                        bundle.putString("total_size", String.valueOf((totalBytes / 1024) / 1024));
                        EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                                DOWNLOAD_OFFLINE_FILE.DOWNLOAD_PROGRESS, bundle));
                    }

                    @Override
                    public void onSuccess(int downloadId, String filePath) {
                        General.showLog("##DOWNLOAD", "Success path : " + filePath);
                        Bundle bundle = new Bundle();
                        bundle.putString("file_path", filePath);
                        EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                                DOWNLOAD_OFFLINE_FILE.DOWNLOAD_SUCCESS, bundle));
                        stopSelf();
                    }

                    @Override
                    public void onFailure(int downloadId, int statusCode, String errMsg) {
                        General.showLog("##DOWNLOAD", "Error : " + errMsg);
                        Bundle bundle = new Bundle();
                        bundle.putInt("status_code", statusCode);
                        bundle.putString("error_message", errMsg);
                        EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                                DOWNLOAD_OFFLINE_FILE.DOWNLOAD_FAILED, bundle));
                        stopSelf();
                    }


                })
                .build();

        int downloadId = manager.add(request);

    }

    @Override
    public void onDestroy() {
        General.showToast(DownloadFileService.this, "service ends");
        super.onDestroy();
    }


    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    class BacokgroundWork extends AsyncTask<Void, Void, Void> {

        OfflineFileData offlineFileData;
        //int flag;

        @Override
        protected void onPreExecute() {
            offlineFileData = null;
            //flag = 1;
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            /*if (!new CacheBoundary().getIsPaymentDataSentToServer(DownloadFileService.this)) {
                String status = new RestBoundary().setUserPaymentInfo(DownloadFileService.this,
                        Constants.BASE_URL, new CacheBoundary().getMyToken(DownloadFileService.this),
                        General.getShaKeyForRefreshToken(new CacheBoundary().getMyUuid(DownloadFileService.this),
                                new CacheBoundary().getMyEmail(DownloadFileService.this)),
                        new CacheBoundary().getPaymentJsonResponseString(DownloadFileService.this));
                if (status == null) {
                    flag = 0;
                }
            }*/
            //if (flag == 1) {
                offlineFileData = new RestBoundary().getOfflineFileData(DownloadFileService.this, Constants.BASE_URL,
                        new CacheBoundary().getMyToken(DownloadFileService.this),
                        General.getShaKey(new CacheBoundary().getMyUuid(DownloadFileService.this),
                                new CacheBoundary().getMyEmail(DownloadFileService.this)));
            //}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (offlineFileData != null) {

                if (!new CacheBoundary().getOfflineFileVersion(DownloadFileService.this).equals(offlineFileData.getOffline_file_version())) {
                    File mainFile = new File(Constants.OFFLINE_FILE_PATH);
                    if (mainFile.exists()) {
                        mainFile.delete();
                    }
                    File tempFile = new File(Constants.OFFLINE_FILE_PATH + ".tmp");
                    if (tempFile.exists()) {
                        tempFile.delete();
                    }
                }

                new CacheBoundary().setOfflineFileVersion(DownloadFileService.this, offlineFileData.getOffline_file_version());
                new CacheBoundary().setOfflineFileIv(DownloadFileService.this, offlineFileData.getOffline_file_iv());
                new CacheBoundary().setOfflineFilePassword(DownloadFileService.this, offlineFileData.getOffline_file_password());
                startDownloadFile(offlineFileData.getOffline_file_url());

            } else {
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.
                        DOWNLOAD_OFFLINE_FILE.GET_DOWNLOAD_FILE_DATA_FAILED, null));
                stopSelf();
            }
        }
    }

}
