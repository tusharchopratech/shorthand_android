package com.shorthand.edata.pojo;

/**
 * Created by tushar on 10/08/17.
 */

public class OfflineFileData {

    String offline_file_version, offline_file_password, offline_file_iv;
    String offline_file_url;


    public String getOffline_file_version() {
        return offline_file_version;
    }

    public void setOffline_file_version(String offline_file_version) {
        this.offline_file_version = offline_file_version;
    }

    public String getOffline_file_url() {
        return offline_file_url;
    }

    public void setOffline_file_url(String offline_file_url) {
        this.offline_file_url = offline_file_url;
    }

    public String getOffline_file_password() {
        return offline_file_password;
    }

    public void setOffline_file_password(String offline_file_password) {
        this.offline_file_password = offline_file_password;
    }

    public String getOffline_file_iv() {
        return offline_file_iv;
    }

    public void setOffline_file_iv(String offline_file_iv) {
        this.offline_file_iv = offline_file_iv;
    }
}
