package com.shorthand.edata.pojo;

import java.util.Arrays;

/**
 * Created by tushar on 27/08/17.
 */

public class OfflineDictationInfo {

    private String speaker_name;

    private String topic;

    private String record_name;

    private String[] audio;

    private String speed;

    private String time;

    private String gender;

    private String[] images;

    private String language;


    public OfflineDictationInfo(String speaker_name, String topic, String record_name,
                                String[] audio, String speed, String time, String gender,
                                String[] images, String language) {
        this.speaker_name = speaker_name;
        this.topic = topic;
        this.record_name = record_name;
        this.audio = audio;
        this.speed = speed;
        this.time = time;
        this.gender = gender;
        this.images = images;
        this.language = language;
    }

    public String getSpeaker_name() {
        return speaker_name;
    }

    public void setSpeaker_name(String speaker_name) {
        this.speaker_name = speaker_name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getRecord_name() {
        return record_name;
    }

    public void setRecord_name(String record_name) {
        this.record_name = record_name;
    }

    public String[] getAudio() {
        return audio;
    }

    public void setAudio(String[] audio) {
        this.audio = audio;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "OfflineDictationInfo{" +
                "speaker_name='" + speaker_name + '\'' +
                ", topic='" + topic + '\'' +
                ", record_name='" + record_name + '\'' +
                ", audio=" + Arrays.toString(audio) +
                ", speed='" + speed + '\'' +
                ", time='" + time + '\'' +
                ", gender='" + gender + '\'' +
                ", images=" + Arrays.toString(images) +
                ", language='" + language + '\'' +
                '}';
    }

}

