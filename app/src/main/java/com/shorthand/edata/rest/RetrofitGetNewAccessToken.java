package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by tushar on 13/07/17.
 */

public class RetrofitGetNewAccessToken {

    public String getNewAccessToken(Context context, String baseUrl, String refreshToken, String key) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<ApiResponse> c = api.sendData(refreshToken, key);
        try {
            Response<ApiResponse> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                new CacheBoundary().setIsTokenExpired(context, false);
                return resp.body().getToken();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    class ApiResponse {
        String token;

        public ApiResponse(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }
    }

    interface ApiInterface {
        @POST("v1/get_new_access_token")
        Call<ApiResponse> sendData(@Header("authorization") String authorization, @Header("key") String key);
    }
}
