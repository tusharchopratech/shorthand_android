package com.shorthand.phomescreen.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;

import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.pojoeventbus.MessageEventServiceMethods;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.plogin.LoginActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.FileInputStream;

public class MusicPlayerService extends Service implements AudioManager.OnAudioFocusChangeListener {


    String className = this.getClass().getSimpleName();
    String TAG = "#" + className;
    private static final int NOTIFICATION_ID = 001;
    private Bitmap notificationIconBitmap;
    private int smallImage = R.mipmap.ic_launcher;
    private static NotificationManager mNotificationManager;

    public static String PLAY_PAUSE_ACTION = "play_pause_action";
    public static String CLOSE_ACTION = "close_action";
    public static DictationInfo dictationInfo = null;
    public static String audioSource = "";

    public static int TOTAL_AUDIO_DURATION = 0;
    public static String audioStatus = "";

    MediaPlayer mediaPlayer;
    PhoneStateListener phoneStateListener;
    AudioManager audioManager;
    private final Handler handler = new Handler();
    private final Runnable r = new Runnable() {
        @Override
        public void run() {
            updateSeekProgress();
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            if (intent.hasExtra("dictationInfo") && intent.hasExtra("audioSource")) {
                audioStatus = Constants.AUDIO_STATUS.LOADING;
                dictationInfo = (DictationInfo) intent.getSerializableExtra("dictationInfo");
                audioSource = intent.getStringExtra("audioSource");
                startAudio();
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.MUSIC_PLAYER_SERVICE_STARTED, null));

                audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                setupPhoneStateListener();
                setupAudioFocusListener();
            } else {

                if (intent.getAction() != null && intent.getAction().equals(PLAY_PAUSE_ACTION)) {
                    if (mediaPlayer.isPlaying()) {
                        pauseAudio();
                    } else {
                        playAudio();
                    }
                } else if (intent.getAction() != null && intent.getAction().equals(CLOSE_ACTION)) {
                    EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_CLOSED, null));
                    onDestroy();
                }
            }
        }

        if (dictationInfo != null) {
            General.showLog(TAG, "onStartCommand called : " + dictationInfo.getLanguage()
                    + "/" + dictationInfo.getSpeed() + "/" + dictationInfo.getDuration()
                    + "/" + General.capsFirst(dictationInfo.getTopic()));
        } else {
            General.showLog(TAG, "onStartCommand called : dictationInfo is null");
        }

        return START_NOT_STICKY;
    }

    private void setupAudioFocusListener() {
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
        }
    }

    private void setupPhoneStateListener() {
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    pauseAudio();
                } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mNotificationManager != null) {
            mNotificationManager.cancelAll();
            this.stopSelf();
        }
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (dictationInfo != null) {
            General.showLog(TAG, "onDestroy called : " + dictationInfo.getLanguage()
                    + "/" + dictationInfo.getSpeed() + "/" + dictationInfo.getDuration()
                    + "/" + dictationInfo.getTopic());
        } else {
            General.showLog(TAG, "onDestroy called : dictationInfo is null");
        }
        dictationInfo = null;
        audioStatus = Constants.AUDIO_STATUS.STOPPED;
        EventBus.getDefault().unregister(this);
        removePhoneStateListener();

    }

    private void removePhoneStateListener() {
        if (phoneStateListener != null) {
            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (mgr != null) {
                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventServiceMethods event) {
        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.SEEK_TO)) {
            seekTo(event.getBundleData().getInt("SEEK_TO"));
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PLAY)) {
            playAudio();
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PAUSE)) {
            pauseAudio();
        }
    }


    public void startAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        setUpMediaPlayer();
        if (audioSource.equals(Constants.AUDIO_SOURCE.ONLINE)) {
            if (dictationInfo != null) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if(dictationInfo!=null && dictationInfo.getUuid()!=null) {
                            new RestBoundary().setViewed(MusicPlayerService.this, Constants.BASE_URL, new CacheBoundary().getMyToken(MusicPlayerService.this),
                                    General.getShaKey(new CacheBoundary().getMyUuid(MusicPlayerService.this), new CacheBoundary().getMyEmail(MusicPlayerService.this)),
                                    dictationInfo.getUuid());
                        }
                    }
                });
                String audioUrl = General.getAudioUrl(new CacheBoundary().getMyToken(MusicPlayerService.this),
                        General.getShaKey(new CacheBoundary().getMyUuid(MusicPlayerService.this),
                                new CacheBoundary().getMyEmail(MusicPlayerService.this)), dictationInfo.getUuid());
                try {
                    mediaPlayer.setDataSource(audioUrl);
                    mediaPlayer.prepareAsync();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (audioSource.equals(Constants.AUDIO_SOURCE.OFFLINE)) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    FileInputStream mp3FileInputStream = new OfflineDictationsBoundary().getAudioFile(dictationInfo.getUuid()
                            , new CacheBoundary().getOfflineFilePassword(MusicPlayerService.this)
                            , new CacheBoundary().getOfflineFileIv(MusicPlayerService.this));
                    try {
                        mediaPlayer.setDataSource(mp3FileInputStream.getFD());
                        mediaPlayer.prepareAsync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    private void setUpMediaPlayer() {
        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                Bundle bundle = new Bundle();
                bundle.putInt("SECONDARY_PROGRESS_PERCENT", percent);
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_SECONDARY_PROGRESS, bundle));
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer.start();
                //setUpMediaPlayer();
                Bundle bundle = new Bundle();
                bundle.putString("STATUS", "SUCCESS");
                TOTAL_AUDIO_DURATION = mediaPlayer.getDuration();
                General.showLog("####MPDU", String.valueOf(mediaPlayer.getDuration()));
                bundle.putInt("AUDIO_DURATION", mediaPlayer.getDuration());
                audioStatus = Constants.AUDIO_STATUS.PLAYING;
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED, bundle));
                updateSeekProgress();
                buildNotification();
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (dictationInfo != null) {
                    General.showLog(TAG, "MediaPlayer.OnCompletionListener called : " + dictationInfo.getLanguage()
                            + "/" + dictationInfo.getSpeed() + "/" + dictationInfo.getDuration()
                            + "/" + General.capsFirst(dictationInfo.getTopic()));
                } else {
                    General.showLog(TAG, "MediaPlayer.OnCompletionListener called : dictationInfo is null");
                }
                audioStatus = Constants.AUDIO_STATUS.COMPLETED;
                buildNotification();
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_COMPLETED, null));
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                audioStatus = Constants.AUDIO_STATUS.ERROR;
                EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_ERROR, null));
                MusicPlayerService.this.stopSelf();
                return false;
            }
        });


    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void pauseAudio() {
        if (mediaPlayer != null) {
            audioStatus = Constants.AUDIO_STATUS.PAUSED;
            mediaPlayer.pause();
            buildNotification();
            EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PAUSED, null));
        }
    }

    public void playAudio() {
        if (mediaPlayer != null) {
            //mediaPlayer.seekTo((lengthOfAudio / 100) * seekBar.getProgress());
            audioStatus = Constants.AUDIO_STATUS.PLAYING;
            mediaPlayer.start();
            updateSeekProgress();
            buildNotification();
            EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED_AFTER_PAUSED, null));
            audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }
    }

    private void updateSeekProgress() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            Bundle bundle = new Bundle();
            bundle.putInt("CURRENT_POSITION", mediaPlayer.getCurrentPosition());
            bundle.putInt("TOTAL_DURATION", mediaPlayer.getDuration());
            EventBus.getDefault().post(new MessageEventData(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PROGRESS, bundle));
            handler.postDelayed(r, 1000);
        }
    }

    public void seekTo(int seekValue) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(seekValue);
        }
    }


    /**
     * Build notification
     */
    private void buildNotification() {

        if (dictationInfo != null) {
            RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.layout.notification);
            Notification.Builder notificationBuilder = new Notification.Builder(this);

            if (notificationIconBitmap == null) {
                notificationIconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            }

            remoteViews.setTextViewText(R.id.notification_line_one, General.capsFirst(dictationInfo.getTopic().toLowerCase()));
            remoteViews.setTextViewText(R.id.notification_line_two, General.capsFirst(dictationInfo.getLanguage().toLowerCase())
                    + ", " + dictationInfo.getSpeed() + "/" + dictationInfo.getDuration());
            remoteViews.setImageViewResource(R.id.notification_play, isPlaying() ? R.drawable.btn_playback_pause : R.drawable.btn_playback_play);
            remoteViews.setImageViewBitmap(R.id.notification_image, notificationIconBitmap);


            Intent playIntent = new Intent(this, MusicPlayerService.class);
            playIntent.setAction(PLAY_PAUSE_ACTION);
            PendingIntent pPlayIntent = PendingIntent.getService(this, 0, playIntent, 0);

            Intent closeIntent = new Intent(this, MusicPlayerService.class);
            closeIntent.setAction(CLOSE_ACTION);
            PendingIntent pCloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);

            Intent notificationIntent = new Intent(this, LoginActivity.class);
            if (audioSource != null && audioSource.equals(Constants.AUDIO_SOURCE.ONLINE)) {
                notificationIntent.setAction(Constants.MUSIC_PLAYER_NOTIFICATION_ACTION.OPEN_ONLINE_DICTATION);
            } else if (audioSource != null && audioSource.equals(Constants.AUDIO_SOURCE.OFFLINE)) {
                notificationIntent.setAction(Constants.MUSIC_PLAYER_NOTIFICATION_ACTION.OPEN_OFFLINE_DICTATION);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


            remoteViews.setOnClickPendingIntent(R.id.notification_collapse, pCloseIntent);
            remoteViews.setOnClickPendingIntent(R.id.notification_play, pPlayIntent);


            Notification notification = notificationBuilder
                    .setSmallIcon(smallImage)
                    .setContentIntent(pendingIntent)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setContent(remoteViews)
                    .setUsesChronometer(true)
                    //.setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setOngoing(true)
                    .build();
            notification.contentView = remoteViews;
            notification.flags = Notification.FLAG_ONGOING_EVENT;
            notification.icon = R.mipmap.ic_launcher;
            startForeground(NOTIFICATION_ID, notification);

            /**
             * Expanded notification
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                RemoteViews mExpandedView = new RemoteViews(this.getPackageName(), R.layout.notification_expanded);

                mExpandedView.setTextViewText(R.id.notification_line_one, General.capsFirst(dictationInfo.getTopic().toLowerCase()));
                mExpandedView.setTextViewText(R.id.notification_line_two, General.capsFirst(dictationInfo.getLanguage().toLowerCase())
                        + ", " + dictationInfo.getSpeed() + "/" + dictationInfo.getDuration());
                mExpandedView.setImageViewResource(R.id.notification_expanded_play, isPlaying() ? R.drawable.btn_playback_pause : R.drawable.btn_playback_play);
                mExpandedView.setImageViewBitmap(R.id.notification_image, notificationIconBitmap);

                mExpandedView.setOnClickPendingIntent(R.id.notification_collapse, pCloseIntent);
                mExpandedView.setOnClickPendingIntent(R.id.notification_expanded_play, pPlayIntent);

                notification.bigContentView = mExpandedView;
            }

            if (mNotificationManager != null) {
                mNotificationManager.notify(NOTIFICATION_ID, notification);
            }
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {

            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                break;
            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                pauseAudio();
                break;

            case (AudioManager.AUDIOFOCUS_LOSS):
                pauseAudio();
                break;

            case (AudioManager.AUDIOFOCUS_GAIN):
                break;
            default:
                break;
        }
    }
}
