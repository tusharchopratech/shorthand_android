package com.shorthand.phomescreen.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.kobakei.ratethisapp.RateThisApp;
import com.shorthand.R;
import com.shorthand.edata.ads.AdsProcessor;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.fragments.feedback.FeedbackFragment;
import com.shorthand.phomescreen.fragments.onlinedictationlist.DictationListFragment;
import com.shorthand.phomescreen.fragments.onlinedictationtypes.DictationTypeFragment;
import com.shorthand.phomescreen.fragments.settings.SettingsFragment;
import com.shorthand.phomescreen.fragments.userprofile.UserProfileFragment;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.shorthand.plogin.LoginActivity;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreenNavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DictationTypeFragment.OnFragmentInteractionListener,
        DictationListFragment.OnFragmentInteractionListener, FeedbackFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener, UserProfileFragment.OnFragmentInteractionListener {

    String className = this.getClass().getSimpleName();
    String TAG = "#" + className;

    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    CircleImageView circleImageViewUserPic;
    SlidingUpPanelLayout slidingUpPanelLayout;
    ImageView imageViewPlayPauseBottom, imageViewCrossTopRight;
    ProgressBar progressBarBottom;
    IHomeScreenNavigationDrawerActivity iHomeScreenNavigationDrawerActivity;
    Button buttonGoOffline;


    public static AudioPlayer audioPlayer;
    public static String currentFragmentName = "";
    public static InitialAppInfo initialAppInfo;
    LinearLayout linearLayoutSlidingPaneLayoutParent, linearLayoutBottomPanel;

    private RewardedVideoAd rewardedVideoAd;


    private final String LOG_TAG = "###IN-APP-PURCHASE-LOGS###";
    private BillingProcessor bp;


    private void initializeVariables() {
        Variables.IS_ONLINE_MODE = true;
        EventBus.getDefault().register(this);
        iHomeScreenNavigationDrawerActivity = new IHomeScreenNavigationDrawerActivity(HomeScreenNavigationDrawerActivity.this);
        ShorthandAnalytics.trackScreen(HomeScreenNavigationDrawerActivity.this, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_ACTIVITY, this.getClass().getSimpleName());
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        toolbar = (Toolbar) findViewById(R.id.toolbarHomeScreenId);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ICON_CLICK, "");
            }
        });
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.slidingLayoutMainId);
        slidingUpPanelLayout.setDragView(this.findViewById(R.id.linearLayoutSlidingPaneLayoutParentId));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        TextView textViewName = (TextView) hView.findViewById(R.id.textViewNameId);
        textViewName.setText(General.capsFirst(new CacheBoundary().getMyName(HomeScreenNavigationDrawerActivity.this)));
        circleImageViewUserPic = (CircleImageView) hView.findViewById(R.id.imageViewProfilePicId);

        imageViewPlayPauseBottom = (ImageView) findViewById(R.id.imageViewPlayPauseBottomId);
        progressBarBottom = (ProgressBar) findViewById(R.id.progressBarBottomId);
        imageViewCrossTopRight = (ImageView) findViewById(R.id.imageViewCrossTopRightId);
        linearLayoutSlidingPaneLayoutParent = (LinearLayout) findViewById(R.id.linearLayoutSlidingPaneLayoutParentId);
        linearLayoutBottomPanel = (LinearLayout) findViewById(R.id.linearLayoutDictationPlayBottomId);
        buttonGoOffline = (Button) findViewById(R.id.buttonGoOfflineSlidingPaneLayoutId);

        TextView textViewPlayedDuration = (TextView) findViewById(R.id.textViewPlayedDurationId);
        TextView textViewTotalDuration = (TextView) findViewById(R.id.textViewTotalDurationId);
        TextView textViewBottomIcon = (TextView) findViewById(R.id.textViewBottomIconId);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBarDictationListId);
        ImageView imageViewPlayPauseMain = (ImageView) findViewById(R.id.imageViewPlayPauseMainId);
        TextView textViewBottomTopic = (TextView) findViewById(R.id.textViewBottomTopicId);
        TextView textViewBottomSpeaker = (TextView) findViewById(R.id.textViewBottomSpeakerId);
        ProgressBar progressBarMain = (ProgressBar) findViewById(R.id.progressBarMainId);
        DiscreteScrollView discreteScrollView = (DiscreteScrollView) findViewById(R.id.discreteScrollViewId);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPagerSlidingUpPanelId);
        String picUrl = new CacheBoundary().getMyPicUrl(HomeScreenNavigationDrawerActivity.this);
        if (picUrl != null && !picUrl.equals("")) {
            Picasso.with(this).load(picUrl).resize(General.getScreenWidth(this), General.getScreenWidth(this))
                    .centerInside().placeholder(R.drawable.ic_profile_placeholder).error(R.drawable.ic_profile_placeholder)
                    .into(circleImageViewUserPic);
        }


        new AdsProcessor(HomeScreenNavigationDrawerActivity.this).showBanner((AdView) findViewById(R.id.adViewAudioPlayerId), "SlidingUpPanelLayout");
        new AdsProcessor(HomeScreenNavigationDrawerActivity.this).showBanner((AdView) findViewById(R.id.adViewHomeScreenNavigationDrawerActivityId), className);

        findViewById(R.id.imageViewViewPagerLeftIconId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);
                }
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "view_pager_left_arrow");
            }
        });

        findViewById(R.id.imageViewViewPagerRightIconId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "view_pager_right_arrow");
            }
        });


        audioPlayer = new AudioPlayer(HomeScreenNavigationDrawerActivity.this);
        audioPlayer.setElements(seekBar, imageViewPlayPauseMain, imageViewPlayPauseBottom, textViewBottomTopic,
                textViewBottomIcon, textViewPlayedDuration, progressBarBottom, textViewTotalDuration,
                progressBarMain, discreteScrollView, textViewBottomSpeaker, slidingUpPanelLayout, viewPager);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen_navigation_drawer);

        initialAppInfo = Variables.INITIAL_APP_INFO;

        initializeVariables();

        General.changeFragment(getSupportFragmentManager(), new DictationTypeFragment()
                , R.id.main_container, Constants.FRAGMENT_ANIMATION.NO_SLIDE);
        currentFragmentName = "DictationTypeFragment";

        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                imageViewPlayPauseBottom.setAlpha(1f - slideOffset);
                progressBarBottom.setAlpha(1f - slideOffset);
                imageViewCrossTopRight.setAlpha(slideOffset);

                if (slideOffset == 1f) {
                    imageViewPlayPauseBottom.setVisibility(View.GONE);
                    progressBarBottom.setVisibility(View.GONE);
                    imageViewCrossTopRight.setVisibility(View.VISIBLE);
                } else if (slideOffset == 0f) {
                    if (MusicPlayerService.audioStatus.equals(Constants.AUDIO_STATUS.LOADING)) {
                        progressBarBottom.setVisibility(View.VISIBLE);
                        imageViewPlayPauseBottom.setVisibility(View.GONE);
                    } else {
                        progressBarBottom.setVisibility(View.GONE);
                        imageViewPlayPauseBottom.setVisibility(View.VISIBLE);
                    }
                    imageViewCrossTopRight.setVisibility(View.GONE);
                }
               /* else {
                    imageViewPlayPauseBottom.setVisibility(View.VISIBLE);
                    imageViewCrossTopRight.setVisibility(View.VISIBLE);
                }*/
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState,
                                            SlidingUpPanelLayout.PanelState newState) {

            }
        });


        imageViewCrossTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "top_right_cross");
            }
        });

        linearLayoutBottomPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                            Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "bottom_panel_click_to_expand");
                }
            }
        });

        if (new CacheBoundary().getUserPaymentStatus(HomeScreenNavigationDrawerActivity.this).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            buttonGoOffline.setVisibility(View.GONE);
        } else {
            buttonGoOffline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                            Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK, "go_offline_button_in_center");
                    General.openPaymentDialog(HomeScreenNavigationDrawerActivity.this);
                }
            });
        }

        String paymentStatus = new CacheBoundary().getUserPaymentStatus(HomeScreenNavigationDrawerActivity.this);
        if (paymentStatus.equals(Constants.USER_PAYMENT_STATUS.PAID_BUT_DATA_NOT_SENT)) {
            iHomeScreenNavigationDrawerActivity.setPaidUserData(
                    new CacheBoundary().getPaymentJsonResponseString(HomeScreenNavigationDrawerActivity.this));
        }

        if (General.isThisServiceRunning(HomeScreenNavigationDrawerActivity.this, MusicPlayerService.class)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            audioPlayer.syncWithService();
        } else {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }

        RateThisApp.onCreate(this);
        RateThisApp.Config config = new RateThisApp.Config();
        config.setNoButtonText(R.string.my_own_thanks);
        RateThisApp.init(config);
        RateThisApp.showRateDialogIfNeeded(this);

        setupBilling();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        rewardedVideoAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        rewardedVideoAd.pause(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
        rewardedVideoAd.destroy(this);
        audioPlayer = null;
        currentFragmentName = "";
        initialAppInfo = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED, "drawer_closed");
        } else if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED, "sliding_up_panel_collapsed");
        } else {
            if (currentFragmentName.equals("DictationTypeFragment")) {
                super.onBackPressed();
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED, "app_closed");
            } else {
                General.changeFragment(getSupportFragmentManager(), new DictationTypeFragment()
                        , R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_LEFT);
                currentFragmentName = "DictationTypeFragment";
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED, "set_dictation_type_fragment");
            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_online_dictation) {
            if (!currentFragmentName.equals("DictationTypeFragment")) {
                currentFragmentName = "DictationTypeFragment";
                General.changeFragment(getSupportFragmentManager(), new DictationTypeFragment()
                        , R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "online_dictations");
            }
        } else if (id == R.id.nav_feedback) {
            if (!currentFragmentName.equals("FeedbackFragment")) {
                currentFragmentName = "FeedbackFragment";
                General.changeFragment(getSupportFragmentManager(), new FeedbackFragment()
                        , R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "feedback");
            }
        } else if (id == R.id.nav_settings) {
            if (!currentFragmentName.equals("SettingsFragment")) {
                currentFragmentName = "SettingsFragment";
                General.changeFragment(getSupportFragmentManager(), new SettingsFragment()
                        , R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "settings");
            }
        } else if (id == R.id.nav_my_details) {
            if (!currentFragmentName.equals("UserDetailsFragment")) {
                currentFragmentName = "UserDetailsFragment";
                General.changeFragment(getSupportFragmentManager(), new UserProfileFragment()
                        , R.id.main_container, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
            }
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "my_profile");
        } else if (id == R.id.nav_offline) {
            if (new CacheBoundary().getUserPaymentStatus(HomeScreenNavigationDrawerActivity.this).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
                if (General.isThisServiceRunning(HomeScreenNavigationDrawerActivity.this, MusicPlayerService.class)) {
                    if (MusicPlayerService.audioSource.equals(Constants.AUDIO_SOURCE.ONLINE)) {
                        stopService(new Intent(HomeScreenNavigationDrawerActivity.this, MusicPlayerService.class));
                    }
                }
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                startActivity(new Intent(HomeScreenNavigationDrawerActivity.this, OfflineDictationsActivity.class));
                finish();
            } else {
                General.openPaymentDialog(HomeScreenNavigationDrawerActivity.this);
            }
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "offline_dictations");
        } else if (id == R.id.nav_rate) {
            iHomeScreenNavigationDrawerActivity.rateApp(HomeScreenNavigationDrawerActivity.this);
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "rate_shorthand");
            RateThisApp.stopRateDialog(this);
        } else if (id == R.id.nav_share) {
            iHomeScreenNavigationDrawerActivity.shareApp(HomeScreenNavigationDrawerActivity.this);
            ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                    Constants.ANALYTICS.EVENT_TYPE.NAVIGATION_DRAWER_ITEM_CLICK, "invite_friends");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventData event) {
        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.OPEN_IN_APP_PURCHASE_DIALOG)) {
            startBilling();
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_COMPLETED)) {
            new AdsProcessor(HomeScreenNavigationDrawerActivity.this).showRewardedVideo(rewardedVideoAd, className);
        }
        audioPlayer.onMessageEvent(event);

    }


    public void setupBilling() {

        bp = new BillingProcessor(this, Constants.IN_APP_PURCHASE_LICENSE_KEY, Constants.MERCHANT_ID, new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(String productId, TransactionDetails details) {
                General.showToast(HomeScreenNavigationDrawerActivity.this, "onProductPurchased: " + productId);
                updateTextViews();
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.IN_APP_PURCHASE_PAYMENT_SUCCESS,
                        new CacheBoundary().getMyEmail(HomeScreenNavigationDrawerActivity.this) + "/" + details.purchaseInfo.responseData);
                iHomeScreenNavigationDrawerActivity.setPaidUserData(details.purchaseInfo.responseData);

            }

            @Override
            public void onBillingError(int errorCode, Throwable error) {
                General.showToast(HomeScreenNavigationDrawerActivity.this, "onBillingError: " + Integer.toString(errorCode));
                ShorthandAnalytics.trackEvent(HomeScreenNavigationDrawerActivity.this,
                        Constants.ANALYTICS.EVENT_TYPE.IN_APP_PURCHASE_PAYMENT_ERROR, "on_billing_error: " + Integer.toString(errorCode));
                if(errorCode == 7){
                    TransactionDetails transactionDetails = bp.getSubscriptionTransactionDetails(initialAppInfo.getSubscription_id());
                    General.showLog(LOG_TAG,transactionDetails.purchaseInfo.responseData);
                    iHomeScreenNavigationDrawerActivity.setPaidUserData(transactionDetails.purchaseInfo.responseData);
                }
            }

            @Override
            public void onBillingInitialized() {
                General.showToast(HomeScreenNavigationDrawerActivity.this, "onBillingInitialized");
                updateTextViews();
            }

            @Override
            public void onPurchaseHistoryRestored() {
                General.showToast(HomeScreenNavigationDrawerActivity.this, "onPurchaseHistoryRestored");
                for (String sku : bp.listOwnedProducts())
                    General.showLog(LOG_TAG, "Owned Managed Product: " + sku);
                for (String sku : bp.listOwnedSubscriptions())
                    General.showLog(LOG_TAG, "Owned Subscription: " + sku);
                updateTextViews();
            }
        });

    }


    private void startBilling() {
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            General.showToast(HomeScreenNavigationDrawerActivity.this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        } else {
            bp.subscribe(this, initialAppInfo.getSubscription_id(), new CacheBoundary().getMyUuid(HomeScreenNavigationDrawerActivity.this), null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }


    private void updateTextViews() {
        try {
            String two = String.format("%s is%s subscribed", initialAppInfo.getSubscription_id(), bp.isSubscribed(initialAppInfo.getSubscription_id()) ? "" : " not");
            General.showLog(LOG_TAG, two);
            General.showToast(HomeScreenNavigationDrawerActivity.this, two);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
