package com.shorthand.edata.pojo;

/**
 * Created by tushar on 12/07/17.
 */

public class UserDetails {
    private String[] practiced;

    private String[] bookmarks;

    private String dob;

    private String name;

    private String gender;

    private String phone_no;

    private String user_type;

    private Rating[] rating;

    public String[] getPracticed() {
        return practiced;
    }

    public void setPracticed(String[] practiced) {
        this.practiced = practiced;
    }

    public String[] getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(String[] bookmarks) {
        this.bookmarks = bookmarks;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Rating[] getRating() {
        return rating;
    }

    public void setRating(Rating[] rating) {
        this.rating = rating;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [practiced = " + practiced + ", bookmarks = " + bookmarks + ", dob = " + dob + ", name = " + name + ", gender = " + gender + ", rating = " + rating + "]";
    }

    public class Rating {
        private String dictation_id;

        private String modulation;

        private String voice_clarity;

        private String passage_difficulty;

        public String getDictation_id() {
            return dictation_id;
        }

        public void setDictation_id(String dictation_id) {
            this.dictation_id = dictation_id;
        }

        public String getModulation() {
            return modulation;
        }

        public void setModulation(String modulation) {
            this.modulation = modulation;
        }

        public String getVoice_clarity() {
            return voice_clarity;
        }

        public void setVoice_clarity(String voice_clarity) {
            this.voice_clarity = voice_clarity;
        }

        public String getPassage_difficulty() {
            return passage_difficulty;
        }

        public void setPassage_difficulty(String passage_difficulty) {
            this.passage_difficulty = passage_difficulty;
        }

        @Override
        public String toString() {
            return "ClassPojo [dictation_id = " + dictation_id + ", modulation = " + modulation + ", voice_clarity = " + voice_clarity + ", passage_difficulty = " + passage_difficulty + "]";
        }
    }

}
