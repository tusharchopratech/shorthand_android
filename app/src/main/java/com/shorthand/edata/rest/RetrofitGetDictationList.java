package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by tushar on 10/07/17.
 */

class RetrofitGetDictationList {

    public DictationInfo[] getDictationList(Context context, String baseUrl, String token, String key, String language, int duration, int speed) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/dictation/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<DictationInfo[]> c = api.sendData(token, key, new ApiRequest(language, duration, speed));
        try {
            Response<DictationInfo[]> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return resp.body();
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    class ApiRequest {
        String language;
        int duration, speed;

        public ApiRequest(String language, int duration, int speed) {
            this.language = language;
            this.duration = duration;
            this.speed = speed;
        }
    }

    private interface ApiInterface {
        @POST("v1/get_dictation_list")
        Call<DictationInfo[]> sendData(@Header("authorization") String authorization, @Header("key") String key, @Body ApiRequest apiRequest);
    }
}
