package com.shorthand.edata.rest;

import android.content.Context;

import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by tushar on 10/07/17.
 */

class RetrofitSetUserDetails {
    public String setUserDetails(Context context, String baseUrl, String token, String key, String dob, String gender, String phoneNo, String name) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        Call<Void> c = api.sendData(token, key, new ApiRequest(dob, gender, phoneNo, name));
        try {
            Response<Void> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                return "SUCCESS";
            } else if (resp != null && resp.code() == 403) {
                new CacheBoundary().setIsTokenExpired(context, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    class ApiRequest {
        String dob, gender, phone_no, name;

        public ApiRequest(String dob, String gender, String phone_no, String name) {
            this.dob = dob;
            this.gender = gender;
            this.phone_no = phone_no;
            this.name = name;
        }
    }


    interface ApiInterface {
        @POST("v1/set_user_details")
        Call<Void> sendData(@Header("authorization") String authorization, @Header("key") String key, @Body ApiRequest apiRequest);
    }
}
