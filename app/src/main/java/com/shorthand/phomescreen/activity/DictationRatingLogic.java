package com.shorthand.phomescreen.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

/**
 * Created by tushar on 02/08/17.
 */

public class DictationRatingLogic {

    boolean autoSubmit;
    Dialog dialog;
    Context context;

    ProgressBar progressBar;
    RatingBar ratingBarModulation, ratingBarVoiceClarity, ratingBarPassageDifficulty;
    EditText editTextMessage;
    Button buttonSubmit;
    TextView textViewRatingDialogTitle;


    public DictationRatingLogic(Context context) {
        this.context = context;
        dialog=new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.inflate_alert_dialog_for_rating);

        buttonSubmit = (Button) dialog.findViewById(R.id.buttonDictationRatingDialogId);
        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBarDiatationRatingDialogId);
        ratingBarModulation = (RatingBar) dialog.findViewById(R.id.ratingBarModulationId);
        ratingBarPassageDifficulty = (RatingBar) dialog.findViewById(R.id.ratingBarPassageDifficultyId);
        ratingBarVoiceClarity = (RatingBar) dialog.findViewById(R.id.ratingBarVoiceClarityId);
        editTextMessage = (EditText) dialog.findViewById(R.id.editTextDictationCommentId);
        textViewRatingDialogTitle = (TextView) dialog.findViewById(R.id.textViewRatingDialogTitleId);

        textViewRatingDialogTitle.setText("Dictation Feedback");
        textViewRatingDialogTitle.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/big_noodle_titling.ttf");
        textViewRatingDialogTitle.setTypeface(typeFace);
        textViewRatingDialogTitle.setTextSize(40);



        ratingBarModulation.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                autoSubmit = false;
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        ratingBarVoiceClarity.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                autoSubmit = false;
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        ratingBarPassageDifficulty.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                autoSubmit = false;
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        editTextMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoSubmit = false;
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    public boolean isDialogShowing(){
        return dialog.isShowing();
    }


    public void openRatingDialog(final String dictationId) {

        autoSubmit = true;

         buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new Background(dictationId, (int) ratingBarPassageDifficulty.getRating(),
                        (int) ratingBarVoiceClarity.getRating(), (int) ratingBarModulation.getRating(), editTextMessage.getText().toString()).execute();
            }
        });

        new CountDownTimer(8 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress((int) ((8000 - millisUntilFinished) * 100) / 8000);
            }

            public void onFinish() {
                if (autoSubmit) {
                    progressBar.setProgress(100);
                    dialog.dismiss();
                    new Background(dictationId, (int) ratingBarPassageDifficulty.getRating(),
                            (int) ratingBarVoiceClarity.getRating(), (int) ratingBarModulation.getRating(), editTextMessage.getText().toString()).execute();
                }
            }
        }.start();

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    class Background extends AsyncTask<Void, Void, Void> {

        String status;
        String dictationId, message;
        int modulation, passageDifficulty, voiceClarity;

        public Background(String dictationId, int passageDifficulty, int voiceClarity, int modulation, String message) {
            status = "";
            this.modulation = modulation;
            this.voiceClarity = voiceClarity;
            this.passageDifficulty = passageDifficulty;
            this.dictationId = dictationId;
            this.message = message;
        }

        @Override
        protected Void doInBackground(Void... params) {
            status = new RestBoundary().setUserDictationRating(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                    General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)),
                    dictationId, passageDifficulty, modulation, voiceClarity, message);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (status != null && status.equals("SUCCESS")) {
            }
        }
    }


}
