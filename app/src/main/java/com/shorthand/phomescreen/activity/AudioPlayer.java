package com.shorthand.phomescreen.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.pojoeventbus.MessageEventServiceMethods;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by tushar on 19/07/17.
 */

public class AudioPlayer {

    Context context;
    int lengthOfAudio;
    SeekBar seekBar;
    ImageView imageViewPlayPauseMain, imageViewPlayPauseBottom;
    TextView textViewTopic, textViewBottomIcon, textViewPlayedDuration, textViewTotalDuration,
            textViewBottomSpeaker;
    ProgressBar progressBarBottom, progressBarMain;
    DiscreteScrollView discreteScrollView;
    SlidingUpPanelLayout slidingUpPanelLayout;
    ViewPager viewPager;
    boolean isAudioPlaying = false;
    DictationRatingLogic dictationRatingLogic;
    static DictationInfo dictationInfo;
    private static final int MINUTES_IN_AN_HOUR = 60;
    private static final int SECONDS_IN_A_MINUTE = 60;

    public AudioPlayer(Context context) {
        this.context = context;
        dictationRatingLogic = new DictationRatingLogic(context);

    }

    public void setElements(SeekBar seekBar, final ImageView imageViewPlayPauseMain,
                            final ImageView imageViewPlayPauseBottom, TextView textViewTopic, TextView textViewBottomIcon,
                            TextView textViewPlayedDuration, ProgressBar progressBarBottom, TextView textViewTotalDuration,
                            ProgressBar progressBarMain, DiscreteScrollView discreteScrollView,
                            TextView textViewBottomSpeaker, SlidingUpPanelLayout slidingUpPanelLayout, ViewPager viewPager) {
        this.seekBar = seekBar;
        this.imageViewPlayPauseBottom = imageViewPlayPauseBottom;
        this.imageViewPlayPauseMain = imageViewPlayPauseMain;
        this.textViewBottomIcon = textViewBottomIcon;
        this.textViewTopic = textViewTopic;
        this.textViewPlayedDuration = textViewPlayedDuration;
        this.progressBarBottom = progressBarBottom;
        this.textViewTotalDuration = textViewTotalDuration;
        this.progressBarMain = progressBarMain;
        this.discreteScrollView = discreteScrollView;
        this.textViewBottomSpeaker = textViewBottomSpeaker;
        this.slidingUpPanelLayout = slidingUpPanelLayout;
        this.viewPager = viewPager;

        this.seekBar.setPadding(25, 0, 25, 0);

        this.discreteScrollView.setHasFixedSize(true);
        this.discreteScrollView.setItemViewCacheSize(100);
        this.discreteScrollView.setDrawingCacheEnabled(true);
        this.discreteScrollView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        this.discreteScrollView.setItemTransitionTimeMillis(150);
        this.discreteScrollView.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());


        if (isAudioPlaying) {
            this.imageViewPlayPauseBottom.setImageResource(R.drawable.vd_pause);
            this.imageViewPlayPauseMain.setImageResource(R.drawable.vd_pause);
        } else {
            this.imageViewPlayPauseBottom.setImageResource(R.drawable.vd_play);
            this.imageViewPlayPauseMain.setImageResource(R.drawable.vd_play);
        }

        this.imageViewPlayPauseBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAudioPlaying) {
                    EventBus.getDefault().post(new MessageEventServiceMethods(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PAUSE, null));
                    setIsAudioPlaying(false);
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK
                            , "pause_from_bottom_panel" + "/" + dictationInfo.getUuid());
                } else {
                    EventBus.getDefault().post(new MessageEventServiceMethods(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PLAY, null));
                    setIsAudioPlaying(true);
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK
                            , "play_from_bottom_panel" + "/" + dictationInfo.getUuid());
                }
            }
        });

        this.imageViewPlayPauseMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAudioPlaying) {
                    EventBus.getDefault().post(new MessageEventServiceMethods(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PAUSE, null));
                    setIsAudioPlaying(false);
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK
                            , "pause_from_main_panel" + "/" + dictationInfo.getUuid());
                } else {
                    EventBus.getDefault().post(new MessageEventServiceMethods(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.PLAY, null));
                    setIsAudioPlaying(true);
                    ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.SLIDING_UP_PANEL_VIEW_CLICK
                            , "play_from_main_panel" + "/" + dictationInfo.getUuid());
                }
            }
        });

        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Bundle bundle = new Bundle();
                int seekValue = (lengthOfAudio * seekBar.getProgress()) / 100;
                bundle.putInt("SEEK_TO", seekValue);
                EventBus.getDefault().post(new MessageEventServiceMethods(Constants.EVENT_BUS_EVENT.MUSIC_PLAYER_SERVICE_PERFORM.SEEK_TO, bundle));
            }
        });

        this.discreteScrollView.setAdapter(new DiscreteScrollViewAdapter(context, "", 1, ""));
    }

    public void onMessageEvent(MessageEventData event) {

        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.MUSIC_PLAYER_SERVICE_STARTED)) {
            if (slidingUpPanelLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }

        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED)) {
            if (event.getBundleData().getString("STATUS").equals("SUCCESS")) {
                lengthOfAudio = event.getBundleData().getInt("AUDIO_DURATION");
                setIsAudioPlaying(true);
                textViewTotalDuration.setText(timeConversion(lengthOfAudio / 1000));
                discreteScrollView.setAdapter(new DiscreteScrollViewAdapter(context, dictationInfo.getUuid(),
                        dictationInfo.getImages(), dictationInfo.getTopic()));
            }
            progressBarBottom.setVisibility(View.GONE);
            progressBarMain.setVisibility(View.GONE);
            imageViewPlayPauseBottom.setVisibility(View.VISIBLE);
            imageViewPlayPauseMain.setVisibility(View.VISIBLE);
            if (slidingUpPanelLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }

        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PROGRESS)) {
            int currentPosition = event.getBundleData().getInt("CURRENT_POSITION");
            int totalLength = event.getBundleData().getInt("TOTAL_DURATION");
            int progress = (int) (((float) currentPosition / totalLength) * 100);
            seekBar.setEnabled(true);
            setIsAudioPlaying(true);
            seekBar.getProgressDrawable().setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
            seekBar.getThumb().setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
            seekBar.setProgress(progress);
            textViewPlayedDuration.setText(timeConversion(currentPosition / 1000));
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_SECONDARY_PROGRESS)) {
            int percent = event.getBundleData().getInt("SECONDARY_PROGRESS_PERCENT");
            seekBar.setSecondaryProgress(percent);
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_CLOSED)) {
            setIsAudioPlaying(false);
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_COMPLETED)) {
            setIsAudioPlaying(false);
            if (Variables.IS_ONLINE_MODE) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (dictationInfo != null && dictationInfo.getUuid() != null) {
                            new RestBoundary().setPracticed(context, Constants.BASE_URL, new CacheBoundary().getMyToken(context),
                                    General.getShaKey(new CacheBoundary().getMyUuid(context), new CacheBoundary().getMyEmail(context)),
                                    dictationInfo.getUuid());
                        }
                    }
                });

                if (!dictationRatingLogic.isDialogShowing()) {
                    dictationRatingLogic.openRatingDialog(dictationInfo.getUuid());
                }

            } else if (!Variables.IS_ONLINE_MODE) {

                ArrayList<String> viewedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflineViewedDictations(context));
                ArrayList<String> practicedArrayList = General.getCommaSeparatedStringAsArray(new CacheBoundary().getOfflinePracticedDictations(context));
                if (viewedArrayList.contains(dictationInfo.getUuid())) {
                    viewedArrayList.remove(dictationInfo.getUuid());
                    new CacheBoundary().setOfflineViewedDictations(context, General.getArrayAsCommaSeparatedString(viewedArrayList));
                }
                if (!practicedArrayList.contains(dictationInfo.getUuid())) {
                    practicedArrayList.add(dictationInfo.getUuid());
                    new CacheBoundary().setOfflinePracticedDictations(context, General.getArrayAsCommaSeparatedString(practicedArrayList));
                }
            }

        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_ERROR)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PAUSED)) {
            setIsAudioPlaying(false);
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.AUDIO_PLAYER_GET_EVENTS.PLAYER_PLAYED_AFTER_PAUSED)) {
            setIsAudioPlaying(true);
        }

    }


    public void startAudio(DictationInfo dictationInfo, String audioSource) {

        seekBar.setProgress(0);
        seekBar.setSecondaryProgress(0);
        textViewPlayedDuration.setText(timeConversion(0));
        textViewTotalDuration.setText(timeConversion(0));
        seekBar.setEnabled(false);
        this.dictationInfo = dictationInfo;
        textViewTopic.setText(General.capsFirst(dictationInfo.getTopic().toLowerCase()));
        textViewBottomSpeaker.setText(General.capsFirst(dictationInfo.getSpeaker_name().toLowerCase()));
        textViewBottomIcon.setText(String.valueOf(dictationInfo.getSpeed()) + "/" + String.valueOf(dictationInfo.getDuration()));

        progressBarBottom.setVisibility(View.VISIBLE);
        progressBarMain.setVisibility(View.VISIBLE);
        imageViewPlayPauseBottom.setVisibility(View.GONE);
        imageViewPlayPauseMain.setVisibility(View.GONE);
        setIsAudioPlaying(false);

        if (General.isThisServiceRunning(context, MusicPlayerService.class)) {
            context.stopService(new Intent(context, MusicPlayerService.class));
        }

        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if (Variables.IS_ONLINE_MODE) {
            viewPager.setAdapter(new ViewPagerAdapter(context, dictationInfo));
        }

        Intent intent = new Intent(context, MusicPlayerService.class);
        intent.putExtra("dictationInfo", dictationInfo);
        intent.putExtra("audioSource", audioSource);
        context.startService(intent);


    }


    private void setIsAudioPlaying(boolean isAudioPlaying) {
        this.isAudioPlaying = isAudioPlaying;
        if (isAudioPlaying) {
            imageViewPlayPauseBottom.setImageResource(R.drawable.vd_pause);
            imageViewPlayPauseMain.setImageResource(R.drawable.vd_pause);
        } else {
            imageViewPlayPauseBottom.setImageResource(R.drawable.vd_play);
            imageViewPlayPauseMain.setImageResource(R.drawable.vd_play);
        }
    }


    private static String timeConversion(int totalSeconds) {
        int hours = totalSeconds / MINUTES_IN_AN_HOUR / SECONDS_IN_A_MINUTE;
        int minutes = (totalSeconds - (hoursToSeconds(hours))) / SECONDS_IN_A_MINUTE;
        int seconds = totalSeconds - ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));
        return String.format("%d:%02d", minutes, seconds);
    }

    private static int hoursToSeconds(int hours) {
        return hours * MINUTES_IN_AN_HOUR * SECONDS_IN_A_MINUTE;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * SECONDS_IN_A_MINUTE;
    }

    public void syncWithService() {
        if (MusicPlayerService.dictationInfo != null) {
            this.dictationInfo = MusicPlayerService.dictationInfo;
            lengthOfAudio = MusicPlayerService.TOTAL_AUDIO_DURATION;
            discreteScrollView.setAdapter(new DiscreteScrollViewAdapter(context, dictationInfo.getUuid(),
                    dictationInfo.getImages(), dictationInfo.getTopic()));
            textViewTopic.setText(General.capsFirst(dictationInfo.getTopic().toLowerCase()));
            textViewBottomSpeaker.setText(General.capsFirst(dictationInfo.getSpeaker_name().toLowerCase()));
            textViewBottomIcon.setText(String.valueOf(dictationInfo.getSpeed()) + "/" + String.valueOf(dictationInfo.getDuration()));
            textViewTotalDuration.setText(timeConversion(lengthOfAudio / 1000));
            if (Variables.IS_ONLINE_MODE) {
                viewPager.setAdapter(new ViewPagerAdapter(context, dictationInfo));
            }
        }
    }
}
