package com.shorthand.edata.storage.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.shorthand.edata.general.Constants;

/**
 * This is the main class of shared preference.
 * Direct setter/getter are used to save and retrieve values.
 */

class SharedPreference {

    private SharedPreferences sharedPreferences;
    private String myName = "myName";
    private String myEmail = "myEmail";
    private String myGender = "myGender";
    private String myDob = "myDob";
    private String myPhone = "myPhone";
    private String myUuid = "myUuid";
    private String myToken = "myToken";
    private String myPicUrl = "myPicUrl";
    private String myRefreshToken = "myRefreshToken";
    private String myReferrer = "myReferrer";
    private String myDeviceName = "myDeviceName";
    private String myCountryName = "myCountryName";
    private String isTokenExpired = "isTokenExpired";
    private String offlinePracticedDictations = "offlinePracticedDictations";
    private String offlineViewedDictations = "offlineViewedDictations";

    private String offlineFileVersion = "offlineFileVersion";
    private String offlineFilePassword = "offlineFilePassword";
    private String offlineFileIv = "offlineFileIv";
    private String userPaymentStatus = "userPaymentStatus";
    private String paymentJsonResponseString = "paymentJsonResponseString";
    private String showTapTargetViewDictationType = "showTapTargetViewDictationType";
    private String showTapTargetViewDictationList = "showTapTargetViewDictationList";


    public SharedPreference(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences("GlobalPrefs", Context.MODE_PRIVATE);
        }
    }

    public String getMyName() {
        return sharedPreferences.getString(this.myName, "");
    }

    public void setMyName(String myName) {
        sharedPreferences.edit().putString(this.myName, myName).apply();
    }

    public String getMyEmail() {
        return sharedPreferences.getString(this.myEmail, "");
    }

    public void setMyEmail(String myEmail) {
        sharedPreferences.edit().putString(this.myEmail, myEmail).apply();
    }

    public String getMyGender() {
        return sharedPreferences.getString(this.myGender, "");
    }

    public void setMyGender(String myGender) {
        sharedPreferences.edit().putString(this.myGender, myGender).apply();
    }

    public String getMyDob() {
        return sharedPreferences.getString(this.myDob, "");
    }

    public void setMyDob(String myDob) {
        sharedPreferences.edit().putString(this.myDob, myDob).apply();
    }

    public String getMyPhone() {
        return sharedPreferences.getString(this.myPhone, "");
    }

    public void setMyPhone(String myPhone) {
        sharedPreferences.edit().putString(this.myPhone, myPhone).apply();
    }

    public String getMyUuid() {
        return sharedPreferences.getString(this.myUuid, "");
    }

    public void setMyUuid(String myUuid) {
        sharedPreferences.edit().putString(this.myUuid, myUuid).apply();
    }

    public String getMyToken() {
        return sharedPreferences.getString(this.myToken, "");
    }

    public void setMyToken(String myToken) {
        sharedPreferences.edit().putString(this.myToken, myToken).apply();
    }

    public String getMyRefreshToken() {
        return sharedPreferences.getString(this.myRefreshToken, "");
    }

    public void setMyRefreshToken(String myRefreshToken) {
        sharedPreferences.edit().putString(this.myRefreshToken, myRefreshToken).apply();
    }

    public String getMyPicUrl() {
        return sharedPreferences.getString(this.myPicUrl, "");
    }

    public void setMyPicUrl(String myPicUrl) {
        sharedPreferences.edit().putString(this.myPicUrl, myPicUrl).apply();
    }

    public boolean getIsTokenExpired() {
        return sharedPreferences.getBoolean(this.isTokenExpired, false);
    }

    public void setIsTokenExpired(boolean isTokenExpired) {
        sharedPreferences.edit().putBoolean(this.isTokenExpired, isTokenExpired).apply();
    }

    public String getOfflinePracticedDictations() {
        return sharedPreferences.getString(this.offlinePracticedDictations, "");
    }

    public void setOfflinePracticedDictations(String offlinePracticedDictations) {
        sharedPreferences.edit().putString(this.offlinePracticedDictations, offlinePracticedDictations).apply();
    }

    public String getOfflineViewedDictations() {
        return sharedPreferences.getString(this.offlineViewedDictations, "");
    }

    public void setOfflineViewedDictations(String offlineViewedDictations) {
        sharedPreferences.edit().putString(this.offlineViewedDictations, offlineViewedDictations).apply();
    }

    public String getOfflineFileVersion() {
        return sharedPreferences.getString(this.offlineFileVersion, "");
    }

    public void setOfflineFileVersion(String offlineFileVersion) {
        sharedPreferences.edit().putString(this.offlineFileVersion, offlineFileVersion).apply();
    }

    public String getOfflineFilePassword() {
        return sharedPreferences.getString(this.offlineFilePassword, "");
    }

    public void setOfflineFilePassword(String offlineFilePassword) {
        sharedPreferences.edit().putString(this.offlineFilePassword, offlineFilePassword).apply();
    }

    public String getOfflineFileIv() {
        return sharedPreferences.getString(this.offlineFileIv, "");
    }

    public void setOfflineFileIv(String offlineFileIv) {
        sharedPreferences.edit().putString(this.offlineFileIv, offlineFileIv).apply();
    }

    public String getMyReferrer() {
        return sharedPreferences.getString(this.myReferrer, "");
    }

    public void setMyReferrer(String myReferrer) {
        sharedPreferences.edit().putString(this.myReferrer, myReferrer).apply();
    }

    public String getUserPaymentStatus() {
        return sharedPreferences.getString(this.userPaymentStatus, Constants.USER_PAYMENT_STATUS.FREE);
    }

    public void setUserPaymentStatus(String status) {
        sharedPreferences.edit().putString(this.userPaymentStatus, status).apply();
    }


    public boolean getShowTapTargetViewDictationType() {
        return sharedPreferences.getBoolean(this.showTapTargetViewDictationType, true);
    }

    public void setShowTapTargetViewDictationType(boolean showTapTargetViewDictationType) {
        sharedPreferences.edit().putBoolean(this.showTapTargetViewDictationType, showTapTargetViewDictationType).apply();
    }

    public boolean getShowTapTargetViewDictationList() {
        return sharedPreferences.getBoolean(this.showTapTargetViewDictationList, true);
    }

    public void setShowTapTargetViewDictationList(boolean showTapTargetViewDictationList) {
        sharedPreferences.edit().putBoolean(this.showTapTargetViewDictationList, showTapTargetViewDictationList).apply();
    }

    public String getPaymentJsonResponseString() {
        return sharedPreferences.getString(this.paymentJsonResponseString, "");
    }

    public void setPaymentJsonResponseString(String paymentJsonResponseString) {
        sharedPreferences.edit().putString(this.paymentJsonResponseString, paymentJsonResponseString).apply();
    }

    public String getMyDeviceName() {
          return sharedPreferences.getString(this.myDeviceName, "");
    }

    public void setMyDeviceName(String myDeviceName) {
        sharedPreferences.edit().putString(this.myDeviceName, myDeviceName).apply();
    }

    public String getMyCountryName() {
        return sharedPreferences.getString(this.myCountryName, "");
    }

    public void setMyCountryName(String myCountryName) {
        sharedPreferences.edit().putString(this.myCountryName, myCountryName).apply();
    }
}