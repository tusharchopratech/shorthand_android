package com.shorthand.edata.ads;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import java.util.Random;

/**
 * Created by tushar on 02/08/17.
 */

public class AdsProcessor {

    Context context;

    public AdsProcessor(Context context) {
        this.context = context;
    }

    public void showBanner(final AdView adView, final String className) {
        int flag = 0;
        if (!new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            if (Constants.IS_IN_PRODUCTION) {
                if (className.equals("HomeScreenNavigationDrawerActivity")) {
                    if (Variables.ADS_PERCENTAGE_BANNER_HOMESCREEN_ACTIVITY != 0 &&
                            getRandomNumber() < Variables.ADS_PERCENTAGE_BANNER_HOMESCREEN_ACTIVITY) {
                        flag = 1;
                    }
                } else if (className.equals("SlidingUpPanelLayout")) {
                    if (Variables.ADS_PERCENTAGE_BANNER_SLIDING_UP_PANEL != 0 &&
                            getRandomNumber() < Variables.ADS_PERCENTAGE_BANNER_SLIDING_UP_PANEL) {
                        flag = 1;
                    }
                }

                if (flag == 1) {
                    adView.loadAd(new AdRequest.Builder().build());
                    adView.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            adView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                            ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.BANNER_AD_CLICK, className);
                        }
                    });
                }
            }
        }
    }


    public void showInterstitial(final String className) {
        int flag = 0;

        if (!new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            if (Constants.IS_IN_PRODUCTION) {
                if (className.equals("RecyclerViewDictationListAdapter")) {
                    if (Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_LIST != 0
                            && getRandomNumber() < Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_LIST) {
                        flag = 1;
                    }
                } else if (className.equals("RecyclerViewDictationTypeAdapter")) {
                    if (Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_TYPE != 0
                            && getRandomNumber() < Variables.ADS_PERCENTAGE_INTERSTITIAL_DICTATION_TYPE) {
                        flag = 1;
                    }
                }
                if (flag == 1) {
                    final InterstitialAd mInterstitialAd = new InterstitialAd(context);
                    mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.abmob_interstitial_id));
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            mInterstitialAd.show();
                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                            ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.INTERSTITIAL_AD_CLICK, className);
                        }
                    });
                }
            }
        }
    }

    public void showRewardedVideo(final RewardedVideoAd rewardedVideoAd, final String className) {

        if (!new CacheBoundary().getUserPaymentStatus(context).equals(Constants.USER_PAYMENT_STATUS.PAID)) {
            if (Constants.IS_IN_PRODUCTION) {
                if (Variables.ADS_PERCENTAGE_REWARDED_VIDEO != 0 && getRandomNumber() < Variables.ADS_PERCENTAGE_REWARDED_VIDEO) {
                    rewardedVideoAd.loadAd(context.getResources().getString(R.string.abmob_rewarded_video_id),
                            new AdRequest.Builder().build());
                    rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                        @Override
                        public void onRewardedVideoAdLoaded() {
                            if (rewardedVideoAd.isLoaded()) {
                                rewardedVideoAd.show();
                            }
                        }

                        @Override
                        public void onRewardedVideoAdOpened() {

                        }

                        @Override
                        public void onRewardedVideoStarted() {

                        }

                        @Override
                        public void onRewardedVideoAdClosed() {

                        }

                        @Override
                        public void onRewarded(RewardItem rewardItem) {

                        }

                        @Override
                        public void onRewardedVideoAdLeftApplication() {
                            ShorthandAnalytics.trackEvent(context, Constants.ANALYTICS.EVENT_TYPE.REWARDED_VIDEO_AD_CLICK, className);
                        }

                        @Override
                        public void onRewardedVideoAdFailedToLoad(int i) {

                        }
                    });
                }
            }
        }
    }

    public int getRandomNumber() {
        Random r = new Random();
        int Low = 1;
        int High = 101;
        return r.nextInt(High - Low) + Low;
    }

}
