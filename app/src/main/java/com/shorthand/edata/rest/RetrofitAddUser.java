package com.shorthand.edata.rest;

import android.content.Context;
import android.os.Bundle;

import com.shorthand.edata.pojo.AddUserData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by tushar on 10/07/17.
 */

class RetrofitAddUser {

    public Bundle addUserAndGetUserId(Context context, String baseUrl, AddUserData addUserData) {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(baseUrl + "v1/users/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);

        Call<ApiResponse> c = api.sendData(addUserData);
        try {
            Response<ApiResponse> resp = c.execute();
            if (resp != null && resp.code() == 200) {
                Bundle bundle = new Bundle();
                bundle.putString("token", resp.body().getToken());
                bundle.putString("refresh_token", resp.body().getRefresh_token());
                bundle.putString("uuid", resp.body().getUuid());
                bundle.putString("type", resp.body().getType());
                new CacheBoundary().setIsTokenExpired(context,false);
                return bundle;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    interface ApiInterface {
        @POST("v1/add_user")
        Call<ApiResponse> sendData(@Body AddUserData addUserData);
    }

    class ApiResponse {
        private String token, uuid, refresh_token, type;

        public ApiResponse(String token, String uuid, String refresh_token, String type) {
            this.token = token;
            this.uuid = uuid;
            this.refresh_token = refresh_token;
            this.type = type;
        }

        public String getToken() {
            return token;
        }

        public String getUuid() {
            return uuid;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public String getType() {
            return type;
        }
    }


}
