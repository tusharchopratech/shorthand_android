package com.shorthand.pofflinedictations.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.offline.DownloadFileService;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.pofflinedictations.activity.OfflineDictationsActivity;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class OfflineDictationDownloadFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Context context;
    NumberProgressBar numberProgressBar;
    AVLoadingIndicatorView avLoadingIndicatorView;
    TextView textViewFileSize;

    public OfflineDictationDownloadFragment() {
        // Required empty public constructor
    }

    public static OfflineDictationDownloadFragment newInstance(String param1, String param2) {
        OfflineDictationDownloadFragment fragment = new OfflineDictationDownloadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private void initializeVariables(View view) {
        EventBus.getDefault().register(this);
        this.context = view.getContext();
        numberProgressBar = (NumberProgressBar) view.findViewById(R.id.numberProgressBarId);
        avLoadingIndicatorView = (AVLoadingIndicatorView) view.findViewById(R.id.aVLoadingIndicatorViewOfflineDictationDownloadId);
        textViewFileSize = (TextView) view.findViewById(R.id.textViewOfflineFileSizeId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offline_dictation_download, container, false);
        initializeVariables(view);
        if (!new OfflineDictationsBoundary().isOfflineFileExist() && !General.isThisServiceRunning(context, DownloadFileService.class)) {
            context.startService(new Intent(context, DownloadFileService.class));
        }
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventData event) {
        if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.DOWNLOAD_OFFLINE_FILE.DOWNLOAD_START)) {
            if (Integer.parseInt(event.getBundleData().getString("total_size")) > (General.getAvailbleStorage())) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Please free up your space. Required memory space "
                        +event.getBundleData().getString("total_size")+"Mb. Free memory space "
                        +String.valueOf(General.getAvailbleStorage())+"Mb.");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            } else {
                avLoadingIndicatorView.setVisibility(View.VISIBLE);
                textViewFileSize.setText("Downloading file.\nPlease Wait\n(" + event.getBundleData().getString("total_size") + " mb)");
            }
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.DOWNLOAD_OFFLINE_FILE.DOWNLOAD_PROGRESS)) {
            avLoadingIndicatorView.setVisibility(View.VISIBLE);
            numberProgressBar.setProgress(event.getBundleData().getInt("progress"));
            textViewFileSize.setText("Downloading file.\nPlease Wait\n(" + event.getBundleData().getString("total_size") + " mb)");
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.DOWNLOAD_OFFLINE_FILE.DOWNLOAD_FAILED)) {
            General.changeFragment(getFragmentManager(), new OfflineDictationsNetworkProblemFragment()
                    , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
            OfflineDictationsActivity.currentFragmentName = "OfflineDictationsNetworkProblemFragment";
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.DOWNLOAD_OFFLINE_FILE.GET_DOWNLOAD_FILE_DATA_FAILED)) {
            General.changeFragment(getFragmentManager(), new OfflineDictationsNetworkProblemFragment()
                    , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
            OfflineDictationsActivity.currentFragmentName = "OfflineDictationsNetworkProblemFragment";
        } else if (event.getEventName().equals(Constants.EVENT_BUS_EVENT.DOWNLOAD_OFFLINE_FILE.DOWNLOAD_SUCCESS)) {
            General.removeAllFragment(getFragmentManager(), R.id.main_container_offline_dictations);
            General.changeFragment(getFragmentManager(), new OfflineDictationsFragment()
                    , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
            OfflineDictationsActivity.currentFragmentName = "OfflineDictationsFragment";
        }
    }


}
