package com.shorthand.phomescreen.taptagetview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.shorthand.R;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;

/**
 * Created by tushar on 30/08/17.
 */

public class TapTargetViewProcessor {

    Context context;

    public TapTargetViewProcessor(Context context) {
        this.context = context;
    }

    public void showDictationTypeFragmentTapTargetView(RecyclerView recyclerView, final View toolbarView) {

        if (new CacheBoundary().getShowTapTargetViewDictationType(context)) {
            try {
                TapTargetView.showFor((AppCompatActivity) context,                 // `this` is an Activity
                        TapTarget.forView(recyclerView.getLayoutManager().findViewByPosition(2)
                                , "These are dictation types", "You can select dictation as per language, speed and duration.\nPlease tap to continue.")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                .targetCircleColor(R.color.white)   // Specify a color for the target circle
                                .titleTextSize(20)                  // Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(10)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                                //.icon(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher, null))                     // Specify a custom drawable to draw as the target
                                .targetRadius(60),                  // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
                                //doSomething();
                                TapTargetView.showFor((AppCompatActivity) context,                 // `this` is an Activity
                                        TapTarget.forView(toolbarView
                                                , "Use menu icon", "Use it to switch to offline mode, send us feedback, update profile, share and rate us.\nTap to continue.")
                                                // All options below are optional
                                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                                .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                                .targetCircleColor(R.color.white)   // Specify a color for the target circle
                                                .titleTextSize(20)                  // Specify the size (in sp) of the title text
                                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                                .descriptionTextSize(10)            // Specify the size (in sp) of the description text
                                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                                .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                                .tintTarget(true)                   // Whether to tint the target view's color
                                                .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                                                //.icon(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher, null))                     // Specify a custom drawable to draw as the target
                                                .targetRadius(60),                  // Specify the target radius (in dp)
                                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                                            @Override
                                            public void onTargetClick(TapTargetView view) {
                                                super.onTargetClick(view);      // This call is optional
                                                new CacheBoundary().setShowTapTargetViewDictationType(context, false);
                                            }
                                        });

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showDictationListFragmentTapTargetView(RecyclerView recyclerView) {

        if (new CacheBoundary().getShowTapTargetViewDictationList(context)) {
            try {
                TapTargetView.showFor((AppCompatActivity) context,                 // `this` is an Activity
                        TapTarget.forView(recyclerView.getLayoutManager().findViewByPosition(2).findViewById(R.id.textViewTopicNameId)
                                , "These are dictations", "Topic, speaker and your practiced status are mentioned below.\nTap to continue.")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                .targetCircleColor(R.color.white)   // Specify a color for the target circle
                                .titleTextSize(20)                  // Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(10)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                               // .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                                //.icon(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher, null))                     // Specify a custom drawable to draw as the target
                                .targetRadius(60),                  // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
                                new CacheBoundary().setShowTapTargetViewDictationList(context, false);
                            }

                            @Override
                            public void onTargetCancel(TapTargetView view) {
                                super.onTargetCancel(view);
                                new CacheBoundary().setShowTapTargetViewDictationList(context, false);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
