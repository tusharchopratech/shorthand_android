package com.shorthand.pofflinedictations.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.shorthand.BuildConfig;
import com.shorthand.R;
import com.shorthand.edata.general.Constants;
import com.shorthand.edata.general.General;
import com.shorthand.edata.general.Variables;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.rest.RestBoundary;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.shorthand.phomescreen.activity.AudioPlayer;
import com.shorthand.phomescreen.service.MusicPlayerService;
import com.shorthand.pofflinedictations.fragments.OfflineDictationDownloadFragment;
import com.shorthand.pofflinedictations.fragments.OfflineDictationsFragment;
import com.shorthand.pofflinedictations.fragments.OfflineDictationsNetworkProblemFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.vistrav.ask.Ask;
import com.vistrav.ask.annotations.AskDenied;
import com.vistrav.ask.annotations.AskGranted;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

public class OfflineDictationsActivity extends AppCompatActivity implements OfflineDictationsFragment.OnFragmentInteractionListener
        , OfflineDictationDownloadFragment.OnFragmentInteractionListener, OfflineDictationsNetworkProblemFragment.OnFragmentInteractionListener {

    public String TAG = "#" + getClass().getSimpleName();
    public static String currentFragmentName = "";

    LinearLayout linearLayoutPermissionDenied, linearLayoutBottomPanel;
    Toolbar toolbar;
    SlidingUpPanelLayout slidingUpPanelLayout;
    public static AudioPlayer audioPlayer;

    int flag = 1;

    private void initializeVariables() {
        Variables.IS_ONLINE_MODE = false;
        EventBus.getDefault().register(this);
        String storagePermissionText = "In order to save offline data you will need to grant storage permission.";
        if(!isStoragePermissionGranted()) {
            Ask.on(OfflineDictationsActivity.this)
                    .id(123)
                    .forPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withRationales(storagePermissionText, storagePermissionText).go();
        }
        linearLayoutPermissionDenied = (LinearLayout) findViewById(R.id.linearLayoutPermissionDeniedId);
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.slidingLayoutOfflineActivityId);
        findViewById(R.id.buttonGoOfflineSlidingPaneLayoutId).setVisibility(View.GONE);
        findViewById(R.id.linearLayoutViewPagerContainerId).setVisibility(View.GONE);

        setToolbar();
        setSupportActionBar(toolbar);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBarDictationListId);
        ImageView imageViewPlayPauseBottom = (ImageView) findViewById(R.id.imageViewPlayPauseBottomId);
        ImageView imageViewPlayPauseMain = (ImageView) findViewById(R.id.imageViewPlayPauseMainId);
        TextView textViewBottomTopic = (TextView) findViewById(R.id.textViewBottomTopicId);
        TextView textViewBottomSpeaker = (TextView) findViewById(R.id.textViewBottomSpeakerId);
        ProgressBar progressBarBottom = (ProgressBar) findViewById(R.id.progressBarBottomId);
        ProgressBar progressBarMain = (ProgressBar) findViewById(R.id.progressBarMainId);
        DiscreteScrollView discreteScrollView = (DiscreteScrollView) findViewById(R.id.discreteScrollViewId);
        TextView textViewPlayedDuration = (TextView) findViewById(R.id.textViewPlayedDurationId);
        TextView textViewTotalDuration = (TextView) findViewById(R.id.textViewTotalDurationId);
        TextView textViewBottomIcon = (TextView) findViewById(R.id.textViewBottomIconId);

        linearLayoutBottomPanel = (LinearLayout) findViewById(R.id.linearLayoutDictationPlayBottomId);


        audioPlayer = new AudioPlayer(OfflineDictationsActivity.this);
        audioPlayer.setElements(seekBar, imageViewPlayPauseMain, imageViewPlayPauseBottom, textViewBottomTopic,
                textViewBottomIcon, textViewPlayedDuration, progressBarBottom, textViewTotalDuration,
                progressBarMain, discreteScrollView, textViewBottomSpeaker, slidingUpPanelLayout, null);

        findViewById(R.id.buttonExitAppId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myAppSettings);
                finish();
            }
        });

    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbarOfflineActivityId);
        toolbar.setNavigationIcon(R.drawable.vd_cross);
        LinearLayout linearLayout = (LinearLayout) toolbar.findViewById(R.id.linearLayoutToolbarActivityOfflineId);
        linearLayout.removeAllViews();
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(OfflineDictationsActivity.this);
        textView.setText("Shorthand");
        textView.setGravity(Gravity.CENTER);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/big_noodle_titling.ttf");
        textView.setTypeface(typeFace);
        textView.setPadding(0, 0, General.dpToPx(80, OfflineDictationsActivity.this), 0);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(25);

        TextView textViewSubtitle = new TextView(OfflineDictationsActivity.this);
        textViewSubtitle.setText("Offline Mode");
        textViewSubtitle.setGravity(Gravity.CENTER);
        textViewSubtitle.setTextSize(10);
        textViewSubtitle.setTextColor(Color.WHITE);
        textViewSubtitle.setPadding(0, 0, General.dpToPx(80, OfflineDictationsActivity.this), 0);

        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.addView(textView, layoutParams);
        linearLayout.addView(textViewSubtitle, layoutParams);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_dictations);
        initializeVariables();

        linearLayoutBottomPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        if (General.isThisServiceRunning(OfflineDictationsActivity.this, MusicPlayerService.class)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            audioPlayer.syncWithService();
        } else {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }

        if (isStoragePermissionGranted() && flag == 1) {
            flag = 0;
            executedDownloadingOrLoading();
        } else {
            linearLayoutPermissionDenied.setVisibility(View.VISIBLE);
        }

        if (getIntent().hasExtra("showNoInternetDialog")) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(OfflineDictationsActivity.this);
            builder1.setMessage("You are in Offline Mode.");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @AskGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void fileAccessGranted(int id) {
        General.showLog(TAG, "FILE  GRANTED");
        linearLayoutPermissionDenied.setVisibility(View.GONE);
        if (flag == 1) {
            flag = 0;
            executedDownloadingOrLoading();
        }
    }

    private void executedDownloadingOrLoading() {
        linearLayoutPermissionDenied.setVisibility(View.GONE);
        new BackgroundGetInitialInfo().execute();
        if (!new OfflineDictationsBoundary().isOfflineFileExist()) {
            if (General.isNetworkAvailable(OfflineDictationsActivity.this)) {
                General.changeFragment(getSupportFragmentManager(), new OfflineDictationDownloadFragment()
                        , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                currentFragmentName = "OfflineDictationDownloadFragment";
            } else {
                General.changeFragment(getSupportFragmentManager(), new OfflineDictationsNetworkProblemFragment()
                        , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                currentFragmentName = "OfflineDictationsNetworkProblemFragment";
            }
        } else {
            if (new CacheBoundary().getOfflineFilePassword(OfflineDictationsActivity.this).equals("")) {
                File mainFile = new File(Constants.OFFLINE_FILE_PATH);
                if (mainFile.exists()) {
                    mainFile.delete();
                }
                File tempFile = new File(Constants.OFFLINE_FILE_PATH + ".tmp");
                if (tempFile.exists()) {
                    tempFile.delete();
                }
                if (General.isNetworkAvailable(OfflineDictationsActivity.this)) {
                    General.changeFragment(getSupportFragmentManager(), new OfflineDictationDownloadFragment()
                            , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                    currentFragmentName = "OfflineDictationDownloadFragment";
                } else {
                    General.changeFragment(getSupportFragmentManager(), new OfflineDictationsNetworkProblemFragment()
                            , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                    currentFragmentName = "OfflineDictationsNetworkProblemFragment";
                }
            } else {
                General.changeFragment(getSupportFragmentManager(), new OfflineDictationsFragment()
                        , R.id.main_container_offline_dictations, Constants.FRAGMENT_ANIMATION.SLIDE_RIGHT);
                currentFragmentName = "OfflineDictationsFragment";
            }
        }

    }

    @AskDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void fileAccessDenied(int id) {
        General.showLog(TAG, "FILE  DENiED");
        linearLayoutPermissionDenied.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventData event) {
        audioPlayer.onMessageEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                General.showLog(TAG, "Permission is granted");
                return true;
            } else {
                General.showLog(TAG, "Permission is revoked");
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            General.showLog(TAG, "Permission is granted");
            return true;
        }
    }


    class BackgroundGetInitialInfo extends AsyncTask<Void, Void, Void> {
        InitialAppInfo initialAppInfo;

        @Override
        protected Void doInBackground(Void... params) {
            int versionCode = BuildConfig.VERSION_CODE;
            initialAppInfo = new RestBoundary().getInitialAppInfo(OfflineDictationsActivity.this, Constants.BASE_URL,
                    new CacheBoundary().getMyToken(OfflineDictationsActivity.this)
                    , General.getShaKey(new CacheBoundary().getMyUuid(OfflineDictationsActivity.this),
                            new CacheBoundary().getMyEmail(OfflineDictationsActivity.this)), String.valueOf(versionCode), "android");

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (initialAppInfo != null) {

                new CacheBoundary().setUserPaymentStatus(OfflineDictationsActivity.this, initialAppInfo.getUser_type());

                if (initialAppInfo.getUpdate_app().toLowerCase().equals("yes")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(OfflineDictationsActivity.this);
                    builder1.setMessage("This version of your application not supported now. Please update your app.");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            General.goToPlayStore(OfflineDictationsActivity.this);
                            finish();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else if (initialAppInfo.getUpdate_app().toLowerCase().equals("optional")) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(OfflineDictationsActivity.this);
                    builder1.setMessage("The new version of this application is available, click Ok to upgrade now?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            General.goToPlayStore(OfflineDictationsActivity.this);
                            finish();
                        }
                    });
                    builder1.setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        }
    }

}
