package com.shorthand.popenlink;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.shorthand.R;
import com.shorthand.edata.analytics.ShorthandAnalytics;
import com.shorthand.edata.general.Constants;


public class OpenLinkActivity extends AppCompatActivity {
    Toolbar toolbarFullStory;
    WebView webViewFullStory;
    TextView textViewUrl;
    ProgressBar progressBarFullStory;
    String url;

    /**
     * Method to initialize variables, views, arrays, sqlite, cache etc.
     */
    private void initializeVariables() {
        toolbarFullStory = (Toolbar) findViewById(R.id.toolBarFullStoryId);
        textViewUrl = (TextView) toolbarFullStory.findViewById(R.id.textViewUrlId);
        progressBarFullStory = (ProgressBar) findViewById(R.id.progressBarFullStoryId);
        progressBarFullStory.setMax(100);
        webViewFullStory = (WebView) findViewById(R.id.webViewFullStoryId);
        webViewFullStory.getSettings().setJavaScriptEnabled(true);
        webViewFullStory.setWebChromeClient(new MyWebViewChromeClient());
        webViewFullStory.setWebViewClient(new MyWebViewClient());
        setSupportActionBar(toolbarFullStory);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarFullStory.setNavigationIcon(R.drawable.vd_back_white);
        ShorthandAnalytics.trackScreen(OpenLinkActivity.this, Constants.ANALYTICS.SCREEN_TYPE.SCREEN_ACTIVITY, this.getClass().getSimpleName());
    }

    /**
     * Activity LifeCycle Method
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);
        url = getIntent().getStringExtra("url");
        initializeVariables();
        webViewFullStory.loadUrl(url);
        progressBarFullStory.setProgress(0);
        progressBarFullStory.setVisibility(View.VISIBLE);
        textViewUrl.setText(url);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setValue(int progress) {
        this.progressBarFullStory.setProgress(progress);
        if (progress == 100) {
            progressBarFullStory.setVisibility(View.GONE);
        }
    }

    /**
     * Set WebViewClient for WebView.
     */
    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(url);
            return true;
        }
    }

    /**
     * Set WebChromeClient for WebView.
     */
    private class MyWebViewChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            OpenLinkActivity.this.setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }

    }

    @Override
    public void onBackPressed() {
        ShorthandAnalytics.trackEvent(OpenLinkActivity.this, Constants.ANALYTICS.EVENT_TYPE.HARDWARE_BACK_PRESSED + "/" + this.getClass().getSimpleName(), "activity_closed");
        super.onBackPressed();
    }
}

