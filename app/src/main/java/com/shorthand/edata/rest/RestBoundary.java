package com.shorthand.edata.rest;

import android.content.Context;
import android.os.Bundle;

import com.shorthand.edata.pojo.AddUserData;
import com.shorthand.edata.pojo.AnalyticsPojo;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojo.DictationType;
import com.shorthand.edata.pojo.InitialAppInfo;
import com.shorthand.edata.pojo.OfflineFileData;
import com.shorthand.edata.pojo.AnalyticsPojoRealm;
import com.shorthand.edata.pojo.UserDetails;

/**
 * Created by tushar on 10/07/17.
 */

public class RestBoundary {

    RestInteractor restInteractor;

    public RestBoundary() {
        restInteractor = new RestInteractor();
    }

    public Bundle addUserAndGetUserId(Context context, String baseUrl, AddUserData addUserData) {
        return restInteractor.addUserAndGetUserIdInteractor(context, baseUrl, addUserData);
    }

    public Bundle getMyLocation(Context context) {
        return restInteractor.getMyLocation(context);
    }

    public DictationInfo[] getDictationList(Context context, String baseUrl, String token, String key, String language, int duration, int speed) {
        return restInteractor.getDictationListInteractor(context, baseUrl, token, key, language, duration, speed);
    }

    public UserDetails getUserDetails(Context context, String baseUrl, String token, String key) {
        return restInteractor.getUserDetailsInteractor(context, baseUrl, token, key);
    }

    public String setBookmark(Context context, String baseUrl, String token, String key, String dictationId, boolean isBookmarked) {
        return restInteractor.setBookmarkInteractor(context, baseUrl, token, key, dictationId, isBookmarked);
    }

    public String setFeedback(Context context, String baseUrl, String token, String key, String feedBack, String type) {
        return restInteractor.setFeedbackInteractor(context, baseUrl, token, key, feedBack, type);
    }

    public String setPracticed(Context context, String baseUrl, String token, String key, String dictationId) {
        return restInteractor.setPracticedInteractor(context, baseUrl, token, key, dictationId);
    }

    public String setViewed(Context context, String baseUrl, String token, String key, String dictationId) {
        return restInteractor.setViewedInteractor(context, baseUrl, token, key, dictationId);
    }

    public String setProgressReset(Context context, String baseUrl, String token, String key) {
        return restInteractor.setProgressReset(context, baseUrl, token, key);
    }

    public String[] setShorthandAnalytics(Context context, String baseUrl, String token, String key, AnalyticsPojo[] analyticsPojoArray) {
        return restInteractor.setShorthandAnalytics(context, baseUrl, token, key,analyticsPojoArray);
    }

    public String setUserDetails(Context context, String baseUrl, String token, String key, String dob, String gender, String phoneNo, String name) {
        return restInteractor.setUserDetailsInteractor(context, baseUrl, token, key, dob, gender, phoneNo, name);
    }

    public String setUserDictationRating(Context context, String baseUrl, String token, String key, String dictationId, int passageDifficulty, int modulation, int voiceClarity, String message) {
        return restInteractor.setUserDictationRatingInteractor(context, baseUrl, token, key, dictationId, passageDifficulty, modulation, voiceClarity, message);
    }

    public InitialAppInfo getInitialAppInfo(Context context, String baseUrl, String token, String key, String appVersion, String platform) {
        return restInteractor.getInitialAppInfoInteractor(context, baseUrl, token, key, appVersion, platform);
    }

    public OfflineFileData getOfflineFileData(Context context, String baseUrl, String token, String key) {
        return restInteractor.getOfflineFileData(context, baseUrl, token, key);
    }

    public String getNewAccessToken(Context context, String baseUrl, String refreshToken, String key) {
        return restInteractor.getNewAccessTokenInteractor(context, baseUrl, refreshToken, key);
    }

    public String setUserPaymentInfo(Context context, String baseUrl, String token, String key, String jsonResponseString) {
        return restInteractor.setUserPaymentInfo(context, baseUrl, token, key, jsonResponseString);
    }

}
