package com.shorthand.edata.pojo;

/**
 * Created by tushar on 12/09/17.
 */

public class AnalyticsPojo {

    private String id;
    private String category, action, label;
    private long epoch_time;

    public AnalyticsPojo(String id, String category, String action, String label, long epochTime) {
        this.id = id;
        this.category = category;
        this.action = action;
        this.label = label;
        this.epoch_time = epochTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getEpoch_time() {
        return epoch_time;
    }

    public void setEpoch_time(long epoch_time) {
        this.epoch_time = epoch_time;
    }

}
