package com.shorthand.pofflinedictations.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.shorthand.R;
import com.shorthand.edata.general.General;
import com.shorthand.edata.offline.OfflineDictationsBoundary;
import com.shorthand.edata.pojo.DictationInfo;
import com.shorthand.edata.pojo.OfflineDictationType;
import com.shorthand.edata.pojoeventbus.MessageEventData;
import com.shorthand.edata.storage.sharedpreference.CacheBoundary;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class OfflineDictationsFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ArrayList<DictationInfo> dictationInfoArrayList;

    public OfflineDictationsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Context context;
    MaterialBetterSpinner materialBetterSpinnerSelectDictation;
    RecyclerView recyclerView;
    ArrayList<OfflineDictationType> arrayListOfflineDictationTypes;
    RecyclerViewAdapterOfflineDictationList recyclerViewAdapterOfflineDictationList;

    private void initializeVariables(View view) {
        context = view.getContext();
        EventBus.getDefault().register(this);
        arrayListOfflineDictationTypes = new ArrayList<>();
        materialBetterSpinnerSelectDictation = (MaterialBetterSpinner) view.findViewById(R.id.materialBetterSpinnerSelectDictationId);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewOfflineDictationListId);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offline_dictation_select, container, false);
        initializeVariables(view);

        new BackgroundWork().execute();


        materialBetterSpinnerSelectDictation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                for (int i = 0; i < arrayListOfflineDictationTypes.size(); i++) {
                    if (arrayListOfflineDictationTypes.get(i).getText().equals(materialBetterSpinnerSelectDictation.getText().toString())) {
                        renderRecyclerView(arrayListOfflineDictationTypes.get(i).getLanguage(),
                                arrayListOfflineDictationTypes.get(i).getSpeed(),
                                arrayListOfflineDictationTypes.get(i).getDuration());
                    }
                }
            }
        });
        return view;
    }

    private void renderRecyclerView(String language, String speed, String duration) {
        ArrayList<DictationInfo> tempList = new ArrayList<>();
        for (int i = 0; i < dictationInfoArrayList.size(); i++) {
            if (dictationInfoArrayList.get(i).getLanguage().equals(language) &&
                    dictationInfoArrayList.get(i).getSpeed().equals(speed) &&
                    dictationInfoArrayList.get(i).getDuration().equals(duration)) {
                tempList.add(dictationInfoArrayList.get(i));
                recyclerViewAdapterOfflineDictationList = new RecyclerViewAdapterOfflineDictationList(tempList, context, recyclerView);
                recyclerView.setAdapter(recyclerViewAdapterOfflineDictationList);
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventData event) {
        if (recyclerViewAdapterOfflineDictationList != null) {
            recyclerViewAdapterOfflineDictationList.onMessageEvent(event);
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    class BackgroundWork extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dictationInfoArrayList = new OfflineDictationsBoundary().getDictationList(
                    new CacheBoundary().getOfflineFilePassword(context),
                    new CacheBoundary().getOfflineFileIv(context)
            );
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (dictationInfoArrayList != null) {
                ArrayList<String> arrayListLanguage = new ArrayList<>();
                for (int i = 0; i < dictationInfoArrayList.size(); i++) {
                    String language = dictationInfoArrayList.get(i).getLanguage();
                    String speed = dictationInfoArrayList.get(i).getSpeed();
                    String duration = dictationInfoArrayList.get(i).getDuration();
                    String text = General.capsFirst(language) + "/" + speed + "wpm/" + duration + "minutes";
                    if (!arrayListLanguage.contains(text)) {
                        arrayListLanguage.add(text);
                        arrayListOfflineDictationTypes.add(new OfflineDictationType(text, language, speed, duration));
                    }
                }

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.inflate_spinner_textview_user_details,
                        arrayListLanguage);
                materialBetterSpinnerSelectDictation.setAdapter(arrayAdapter);
            } else {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

}
